import type { Publisher } from '../models/Publisher';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class PublisherService {
    /**
     * Get Publishers
     * Get an array of Publishers according to defined filter criteria
     * @param filter
     * @returns Publisher array of Publishers
     * @throws ApiError
     */
    static getPublishers(filter?: string): CancelablePromise<Array<Publisher>>;
    /**
     * Post Publishers
     * Post an array of Publisher objects
     * @param requestBody
     * @returns Publisher created Publisher
     * @throws ApiError
     */
    static postPublishers(requestBody: Publisher): CancelablePromise<Publisher>;
    /**
     * Refresh All Properties
     * Retrieve an array of urls for use to refresh all Publisher linked properties
     * @returns string array of urls
     * @throws ApiError
     */
    static refreshAllProperties(): CancelablePromise<Array<string>>;
    /**
     * Update Notify
     * Update publisher notification endpoint to keep up to date on property feed
     * @param requestBody
     * @returns Publisher Successful
     * @throws ApiError
     */
    static updateNotify(requestBody: {
        notify?: string;
    }): CancelablePromise<Publisher>;
}
