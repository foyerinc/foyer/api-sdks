import type { Landmark } from '../models/Landmark';
import type { PatchBody } from '../models/PatchBody';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class LandmarkService {
    /**
     * Get Landmark By Id
     * Get specific Landmark with unique Id
     * @param id
     * @param filter
     * @returns Landmark Successful
     * @throws ApiError
     */
    static getLandmarkById(id: number, filter?: string): CancelablePromise<Landmark>;
    /**
     * Patch Landmark By Id
     * Patch specific Landmark with unique Id
     * @param id
     * @param requestBody
     * @returns Landmark patched Landmark
     * @throws ApiError
     */
    static patchLandmarkById(id: number, requestBody?: PatchBody): CancelablePromise<Landmark>;
    /**
     * Delete Landmark By Id
     * Delete specific Landmark with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteLandmarkById(id: number): CancelablePromise<void>;
    /**
     * Insert landmark by edit key
     * @param requestBody
     * @returns Landmark The inserted landmark
     * @throws ApiError
     */
    static insertByEditKey(requestBody: {
        landmark?: Landmark;
        editKey?: string;
        envisionAssetId?: number;
    }): CancelablePromise<Landmark>;
    /**
     * Delete landmark by edit key
     * @param id
     * @param editKey
     * @returns void
     * @throws ApiError
     */
    static deleteByEditKey(id: number, editKey?: string): CancelablePromise<void>;
    /**
     * Edit landmark by edit key
     * @param id
     * @param requestBody
     * @returns Landmark The edited landmark
     * @throws ApiError
     */
    static editByEditKey(id: number, requestBody: {
        landmark?: Landmark;
        editKey?: string;
    }): CancelablePromise<Landmark>;
}
