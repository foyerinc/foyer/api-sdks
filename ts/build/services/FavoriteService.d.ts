import type { Favorite } from '../models/Favorite';
import type { PatchBody } from '../models/PatchBody';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class FavoriteService {
    /**
     * Get Favorites
     * Find Favorites according to defined filter criteria
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static getFavorites(filter?: string): CancelablePromise<Array<Favorite>>;
    /**
     * Post Favorites
     * Post an array of Favorite objects
     * @param requestBody
     * @returns Favorite Successful
     * @throws ApiError
     */
    static postFavorites(requestBody: Favorite): CancelablePromise<Favorite>;
    /**
     * Find One Favorite
     * Find a single Favorite according to defined filter criteria
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static findOneFavorite(filter: string): CancelablePromise<Favorite>;
    /**
     * Get Favorite By Id
     * Get a specific Favorite with unique id
     * @param id
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static getFavoriteById(id: number, filter?: string): CancelablePromise<Favorite>;
    /**
     * Patch Favorite By Id
     * Patch specific Favorite with unique id
     * @param id
     * @param requestBody
     * @returns Favorite patched Favorite
     * @throws ApiError
     */
    static patchFavoriteById(id: number, requestBody?: PatchBody): CancelablePromise<Favorite>;
    /**
     * Delete Favorite
     * Delete specific Favorite with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteFavoriteById(id: number): CancelablePromise<void>;
}
