import type { BoundingBox } from '../models/BoundingBox';
import type { Media } from '../models/Media';
import type { PatchBody } from '../models/PatchBody';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class MediaService {
    /**
     * Get Media
     * Get an array of Media according to defined filter criteria
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMedia(filter?: string): CancelablePromise<Array<Media>>;
    /**
     * Post Media
     * Post an array of Media objects
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static postMedia(requestBody: Media): CancelablePromise<Media>;
    /**
     * Find One Media
     * Fine one Media according to defined filter criteria
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static findOneMedia(filter: string): CancelablePromise<Media>;
    /**
     * Get Media by Id
     * Get a specific Media with unique id
     * @param id
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMediaById(id: number, filter?: string): CancelablePromise<Media>;
    /**
     * Patch Media by Id
     * Patch specific Media with unique id
     * @param id
     * @param requestBody
     * @returns Media patched Media
     * @throws ApiError
     */
    static patchMediaById(id: number, requestBody?: PatchBody): CancelablePromise<Media>;
    /**
     * Delete Media by Id
     * Delete specific Media with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMediaById(id: number): CancelablePromise<void>;
    /**
     * Run Insight on an array of images as files or urls
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    static classify(requestBody: {
        file?: string;
        url?: string;
        force?: boolean;
        objects?: boolean;
    }): CancelablePromise<{
        classifications?: Array<{
            name?: string;
            confidence?: number;
            rank?: number;
        }>;
        detections?: Array<{
            mask?: string;
            area?: number;
            boundingBox?: BoundingBox;
            confidence?: number;
            attributes?: {
                'is-stainless'?: boolean;
            };
        }>;
    }>;
    /**
     * Run Insight given a properly formatted RESO Property with nested Media
     * @returns any Successful
     * @throws ApiError
     */
    static classifyReso(): CancelablePromise<{
        value?: Array<{
            Media?: Array<{
                MediaCategory?: string;
                MediaURL?: string;
                MediaKey?: string;
                MediaModificationTimestamp?: string;
                MediaOf?: string;
            }>;
        }>;
    }>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static reportClassificationError(requestBody?: {
        labels?: Array<string>;
        hash?: string;
    }): CancelablePromise<void>;
    /**
     * Get a presigned url for uploading media through the use of the property edit key
     * @param editKey
     * @param mimeType
     * @param fileExtension
     * @returns string The presigned url that can be used to upload media
     * @throws ApiError
     */
    static getMediaUploadUrlByEditKey(editKey?: string, mimeType?: string, fileExtension?: string): CancelablePromise<string>;
    /**
     * Insert media by the property edit key
     * @param requestBody
     * @returns Media The inserted media
     * @throws ApiError
     */
    static insertMediaByEditKey(requestBody: {
        media?: Array<Media>;
        editKey?: string;
        landmarkId?: number;
    }): CancelablePromise<Array<Media>>;
}
