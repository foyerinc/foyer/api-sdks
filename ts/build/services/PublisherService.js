"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublisherService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class PublisherService {
    /**
     * Get Publishers
     * Get an array of Publishers according to defined filter criteria
     * @param filter
     * @returns Publisher array of Publishers
     * @throws ApiError
     */
    static getPublishers(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Publisher',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Publishers
     * Post an array of Publisher objects
     * @param requestBody
     * @returns Publisher created Publisher
     * @throws ApiError
     */
    static postPublishers(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Publisher',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Refresh All Properties
     * Retrieve an array of urls for use to refresh all Publisher linked properties
     * @returns string array of urls
     * @throws ApiError
     */
    static refreshAllProperties() {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Publisher/refreshAllProperties',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Update Notify
     * Update publisher notification endpoint to keep up to date on property feed
     * @param requestBody
     * @returns Publisher Successful
     * @throws ApiError
     */
    static updateNotify(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Publisher/updateNotify',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.PublisherService = PublisherService;
