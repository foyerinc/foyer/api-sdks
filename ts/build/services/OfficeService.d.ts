import type { Office } from '../models/Office';
import type { PatchBody } from '../models/PatchBody';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class OfficeService {
    /**
     * Find One Office
     * Fine one Office according to defined filter criteria
     * @param filter
     * @returns Office Successful
     * @throws ApiError
     */
    static findOneOffice(filter: string): CancelablePromise<Office>;
    /**
     * Get Office by Id
     * Get a specific Office with unique id
     * @param id
     * @param filter
     * @returns Office single Office
     * @throws ApiError
     */
    static getOfficeById(id: number, filter?: string): CancelablePromise<Office>;
    /**
     * Patch Office by Id
     * Patch a specific Office with unique id
     * @param id
     * @param requestBody
     * @returns Office patched Office
     * @throws ApiError
     */
    static patchOfficeById(id: number, requestBody?: PatchBody): CancelablePromise<Office>;
    /**
     * Delete Office by Id
     * Delete a specific Office with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteOfficeById(id: number): CancelablePromise<void>;
}
