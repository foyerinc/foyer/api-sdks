import type { ErrorsCalculatingFoyerScore } from '../models/ErrorsCalculatingFoyerScore';
import type { GetPropertyByViewerKeyResponse } from '../models/GetPropertyByViewerKeyResponse';
import type { Property } from '../models/Property';
import type { PropertyScore } from '../models/PropertyScore';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class PropertyService {
    /**
     * Get Properties
     * Get an object with an array of properties and a total count according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters
     * @param filter
     * @param verbosity
     * @returns any Successful
     * @throws ApiError
     */
    static getProperties(filter?: string, verbosity?: string): CancelablePromise<{
        count?: number;
        rows?: Array<Property>;
    }>;
    /**
     * Get Recommendations
     * Return an array of Properties according to preferences set on User and Shopper Models
     * @param limit
     * @param bufLen
     * @returns Property Successful
     * @throws ApiError
     */
    static getRecommendations(limit?: number, bufLen?: number): CancelablePromise<Array<Property>>;
    /**
     * Find Nearest Properties
     * Provide an address to find the geographically closest Properties
     * @param streetNumber
     * @param street
     * @param streetType
     * @param cityName
     * @param state
     * @param lat
     * @param lng
     * @returns any Successful
     * @throws ApiError
     */
    static findNearestProperties(streetNumber: string, street: string, streetType: string, cityName: string, state: string, lat: number, lng: number): CancelablePromise<{
        bestChoice?: Property;
        neighbors?: Array<Property>;
        properties?: Array<Property>;
    }>;
    /**
     * Find One Property
     * Find one Property according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters
     * @param filter
     * @param verbosity
     * @returns Property Successful
     * @throws ApiError
     */
    static findOneProperty(filter: string, verbosity?: string): CancelablePromise<Property>;
    /**
     * Get Property by Id
     * Get specific Property with unique id. Also allows for verbosity parameter to shortcut commonly used filters
     * @param id
     * @param filter
     * @param verbosity
     * @returns Property Single Property
     * @throws ApiError
     */
    static getPropertyById(id: number, filter?: string, verbosity?: string): CancelablePromise<Property>;
    /**
     * Property Search
     * Search for properties according to defined criteria and desired image features
     * @param verbosity
     * @returns Property Successful
     * @throws ApiError
     */
    static propertySearch(verbosity?: string): CancelablePromise<Array<Property>>;
    /**
     * Compare property by id to local area
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateLocalGraph(id: number, filter?: string): CancelablePromise<any>;
    /**
     * Calculate Foyer Score for Property
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateScore(id: number, filter?: string): CancelablePromise<(PropertyScore | ErrorsCalculatingFoyerScore)>;
    /**
     * Gets a property by a viewer key
     * @param viewerKey
     * @returns GetPropertyByViewerKeyResponse The property corresponding to the viewer key
     * @throws ApiError
     */
    static getPropertyByViewerKey(viewerKey?: string): CancelablePromise<GetPropertyByViewerKeyResponse>;
}
