"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LandmarkService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class LandmarkService {
    /**
     * Get Landmark By Id
     * Get specific Landmark with unique Id
     * @param id
     * @param filter
     * @returns Landmark Successful
     * @throws ApiError
     */
    static getLandmarkById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Landmark/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Landmark By Id
     * Patch specific Landmark with unique Id
     * @param id
     * @param requestBody
     * @returns Landmark patched Landmark
     * @throws ApiError
     */
    static patchLandmarkById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Landmark/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Landmark By Id
     * Delete specific Landmark with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteLandmarkById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Landmark/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Insert landmark by edit key
     * @param requestBody
     * @returns Landmark The inserted landmark
     * @throws ApiError
     */
    static insertByEditKey(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Landmark/insertByEditKey',
            body: requestBody,
            mediaType: 'application/json',
        });
    }
    /**
     * Delete landmark by edit key
     * @param id
     * @param editKey
     * @returns void
     * @throws ApiError
     */
    static deleteByEditKey(id, editKey) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Landmark/{id}/deleteByEditKey',
            path: {
                'id': id,
            },
            query: {
                'editKey': editKey,
            },
        });
    }
    /**
     * Edit landmark by edit key
     * @param id
     * @param requestBody
     * @returns Landmark The edited landmark
     * @throws ApiError
     */
    static editByEditKey(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Landmark/{id}/editByEditKey',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }
}
exports.LandmarkService = LandmarkService;
