"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MemberService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class MemberService {
    /**
     * Find One Member
     * Fine one Member according to defined filter criteria
     * @param filter
     * @returns Member Successful
     * @throws ApiError
     */
    static findOneMember(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Member/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Member by Id
     * Get specific Member with unique Id
     * @param id
     * @param filter
     * @returns Member single Member
     * @throws ApiError
     */
    static getMemberById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Member/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Member by Id
     * Patch specific Member with unique Id
     * @param id
     * @param requestBody
     * @returns Member patched Member
     * @throws ApiError
     */
    static patchMemberById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Member/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Member by Id
     * Delete specific Member with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMemberById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Member/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.MemberService = MemberService;
