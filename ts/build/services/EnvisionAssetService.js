"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnvisionAssetService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class EnvisionAssetService {
    /**
     * get presigned url of the model
     * Get presigned url of the model corresponding to the envision asset
     * @param id
     * @returns string The presigned url of the model
     * @throws ApiError
     */
    static getModelPresignedUrl(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/EnvisionAsset/{id}/modelPresignedUrl',
            path: {
                'id': id,
            },
        });
    }
    /**
     * Add landmarks for the envision asset
     * @param id
     * @param requestBody
     * @returns Landmark The inserted landmarks
     * @throws ApiError
     */
    static insertLandmarks(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/EnvisionAsset/{id}/insertLandmarks',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }
}
exports.EnvisionAssetService = EnvisionAssetService;
