import type { PatchBody } from '../models/PatchBody';
import type { Share } from '../models/Share';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class ShareService {
    /**
     * Get Shares
     * Get an array of shares according to defined filter criteria
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShares(filter?: string): CancelablePromise<Array<Share>>;
    /**
     * Post Shares
     * Post an array of Share objects
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static postShares(requestBody: Share): CancelablePromise<Share>;
    /**
     * Make Share
     * Make a share with specific contact information
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static makeShare(requestBody: {
        propertyId?: number;
        phoneNumber?: string;
        firstName?: string;
        lastName?: string;
    }): CancelablePromise<Share>;
    /**
     * Find One Share
     * Find a single Share according to defined filter criteria
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static findOneShare(filter: string): CancelablePromise<Share>;
    /**
     * Get Share By Id
     * Get a specific Share with unique id
     * @param id
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShareById(id: number, filter?: string): CancelablePromise<Share>;
    /**
     * Patch Share By Id
     * Patch a specific Share with unique id
     * @param id
     * @param requestBody
     * @returns Share patched Share
     * @throws ApiError
     */
    static patchShareById(id: number, requestBody?: PatchBody): CancelablePromise<Share>;
    /**
     * Delete Share By Id
     * Delete a specific Share with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShareById(id: number): CancelablePromise<void>;
}
