import type { Location } from '../models/Location';
import type { PatchBody } from '../models/PatchBody';
import type { User } from '../models/User';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class UserService {
    /**
     * Login
     * Login with credentials and receive auth token for use in subsequent API calls
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    static login(requestBody: {
        email?: string;
        password?: string;
        source?: string;
    }): CancelablePromise<string>;
    /**
     * Sign Up
     * Sign up a new User for access to Foyer Services
     * @param requestBody
     * @returns any User and related models created
     * @throws ApiError
     */
    static signUp(requestBody: {
        firstName?: string;
        lastName?: string;
        email?: string;
        password?: string;
        confirmedPassword?: string;
        source?: string;
    }): CancelablePromise<any>;
    /**
     * Sign Up as a Publisher
     * Sign up for Foyer Syndication Services
     * @param requestBody
     * @returns any user and publisher created
     * @throws ApiError
     */
    static publisherSignUp(requestBody: {
        email?: string;
        password?: string;
        confirmedPassword?: string;
        name?: string;
        website?: string;
        logo?: string;
    }): CancelablePromise<any>;
    /**
     * Change Password
     * Change current User's log in password
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static changePassword(requestBody: {
        currentPassword?: string;
        newPassword?: string;
        confirmNewPassword?: string;
    }): CancelablePromise<void>;
    /**
     * Request SMS Verification
     * Generate and send code to User's provided phone number for verification
     * @param id
     * @param phone
     * @returns void
     * @throws ApiError
     */
    static requestSmsVerification(id: number, phone: string): CancelablePromise<void>;
    /**
     * Verify SMS
     * Send user provided code to complete verification
     * @param id
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static verifySms(id: number, requestBody: {
        verificationCode?: number;
    }): CancelablePromise<void>;
    /**
     * Update User Location
     * Update a user's location for use with Home Discovery
     * @param id
     * @param requestBody
     * @returns User Successful
     * @throws ApiError
     */
    static updateUserLocation(id: number, requestBody: {
        locationVerified?: boolean;
        location?: Location;
        searchRadius?: number;
        searchCity?: string;
    }): CancelablePromise<User>;
    /**
     * Send Reset Password Request
     * Sends a message to provided email to aid in password reset
     * @param requestBody
     * @returns any send reset password link in email to user
     * @throws ApiError
     */
    static resetPasswordStepOne(requestBody: {
        email?: string;
    }): CancelablePromise<any>;
    /**
     * Confirm Reset Password
     * Send user provided newPassword along with previously requested token to finish resetting password
     * @param requestBody
     * @returns any Reset password
     * @throws ApiError
     */
    static resetPasswordStepTwo(requestBody: {
        resetToken?: string;
        newPassword?: string;
        confirmNewPassword?: string;
    }): CancelablePromise<any>;
    /**
     * Get Users
     * Find users according to defined filter criteria
     * @param filter
     * @returns User Array of Users
     * @throws ApiError
     */
    static getUsers(filter?: string): CancelablePromise<Array<User>>;
    /**
     * Post Users
     * Post an array of User objects
     * @param requestBody
     * @returns User created User
     * @throws ApiError
     */
    static postUsers(requestBody: User): CancelablePromise<User>;
    /**
     * Get User By Id
     * Find specific User with unique id
     * @param id
     * @param filter
     * @returns User single User
     * @throws ApiError
     */
    static getUserById(id: number, filter?: string): CancelablePromise<User>;
    /**
     * Patch User By Id
     * Patch specific User with unique id
     * @param id
     * @param requestBody
     * @returns User patched User
     * @throws ApiError
     */
    static patchUserById(id: number, requestBody?: PatchBody): CancelablePromise<User>;
}
