import type { Landmark } from '../models/Landmark';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class EnvisionAssetService {
    /**
     * get presigned url of the model
     * Get presigned url of the model corresponding to the envision asset
     * @param id
     * @returns string The presigned url of the model
     * @throws ApiError
     */
    static getModelPresignedUrl(id: number): CancelablePromise<string>;
    /**
     * Add landmarks for the envision asset
     * @param id
     * @param requestBody
     * @returns Landmark The inserted landmarks
     * @throws ApiError
     */
    static insertLandmarks(id: number, requestBody?: {
        landmarks?: Array<Landmark>;
        deleteExistingLandmarks?: boolean;
    }): CancelablePromise<Array<Landmark>>;
}
