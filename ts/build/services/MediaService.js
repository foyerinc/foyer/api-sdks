"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class MediaService {
    /**
     * Get Media
     * Get an array of Media according to defined filter criteria
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMedia(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Media',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Media
     * Post an array of Media objects
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static postMedia(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Media',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find One Media
     * Fine one Media according to defined filter criteria
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static findOneMedia(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Media/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Media by Id
     * Get a specific Media with unique id
     * @param id
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMediaById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Media/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Media by Id
     * Patch specific Media with unique id
     * @param id
     * @param requestBody
     * @returns Media patched Media
     * @throws ApiError
     */
    static patchMediaById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Media/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Media by Id
     * Delete specific Media with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMediaById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Media/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Run Insight on an array of images as files or urls
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    static classify(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Media/classify',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                403: `Forbidden`,
            },
        });
    }
    /**
     * Run Insight given a properly formatted RESO Property with nested Media
     * @returns any Successful
     * @throws ApiError
     */
    static classifyReso() {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Media/classifyReso',
            errors: {
                403: `Forbidden`,
            },
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static reportClassificationError(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Media/reportClassificationError',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get a presigned url for uploading media through the use of the property edit key
     * @param editKey
     * @param mimeType
     * @param fileExtension
     * @returns string The presigned url that can be used to upload media
     * @throws ApiError
     */
    static getMediaUploadUrlByEditKey(editKey, mimeType, fileExtension) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Media/mediaUploadUrlByEditKey',
            query: {
                'editKey': editKey,
                'mimeType': mimeType,
                'fileExtension': fileExtension,
            },
        });
    }
    /**
     * Insert media by the property edit key
     * @param requestBody
     * @returns Media The inserted media
     * @throws ApiError
     */
    static insertMediaByEditKey(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Media/insertMediaByEditKey',
            body: requestBody,
            mediaType: 'application/json',
        });
    }
}
exports.MediaService = MediaService;
