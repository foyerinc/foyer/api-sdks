import type { Member } from '../models/Member';
import type { PatchBody } from '../models/PatchBody';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class MemberService {
    /**
     * Find One Member
     * Fine one Member according to defined filter criteria
     * @param filter
     * @returns Member Successful
     * @throws ApiError
     */
    static findOneMember(filter: string): CancelablePromise<Member>;
    /**
     * Get Member by Id
     * Get specific Member with unique Id
     * @param id
     * @param filter
     * @returns Member single Member
     * @throws ApiError
     */
    static getMemberById(id: number, filter?: string): CancelablePromise<Member>;
    /**
     * Patch Member by Id
     * Patch specific Member with unique Id
     * @param id
     * @param requestBody
     * @returns Member patched Member
     * @throws ApiError
     */
    static patchMemberById(id: number, requestBody?: PatchBody): CancelablePromise<Member>;
    /**
     * Delete Member by Id
     * Delete specific Member with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMemberById(id: number): CancelablePromise<void>;
}
