import type { ErrorFeedback } from '../models/ErrorFeedback';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class ErrorFeedbackService {
    /**
     * Post ErrorFeedback
     * Post an ErrorFeedback object
     * @param requestBody
     * @returns ErrorFeedback Successful
     * @throws ApiError
     */
    static postErrorFeedback(requestBody: ErrorFeedback): CancelablePromise<ErrorFeedback>;
}
