"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShareService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class ShareService {
    /**
     * Get Shares
     * Get an array of shares according to defined filter criteria
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShares(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Share',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Shares
     * Post an array of Share objects
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static postShares(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Share',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Make Share
     * Make a share with specific contact information
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static makeShare(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Share/makeShare',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find One Share
     * Find a single Share according to defined filter criteria
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static findOneShare(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Share/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Share By Id
     * Get a specific Share with unique id
     * @param id
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShareById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Share/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Share By Id
     * Patch a specific Share with unique id
     * @param id
     * @param requestBody
     * @returns Share patched Share
     * @throws ApiError
     */
    static patchShareById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Share/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Share By Id
     * Delete a specific Share with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShareById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Share/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.ShareService = ShareService;
