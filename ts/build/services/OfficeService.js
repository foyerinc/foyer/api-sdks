"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OfficeService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class OfficeService {
    /**
     * Find One Office
     * Fine one Office according to defined filter criteria
     * @param filter
     * @returns Office Successful
     * @throws ApiError
     */
    static findOneOffice(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Office/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Office by Id
     * Get a specific Office with unique id
     * @param id
     * @param filter
     * @returns Office single Office
     * @throws ApiError
     */
    static getOfficeById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Office/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Office by Id
     * Patch a specific Office with unique id
     * @param id
     * @param requestBody
     * @returns Office patched Office
     * @throws ApiError
     */
    static patchOfficeById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Office/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Office by Id
     * Delete a specific Office with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteOfficeById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Office/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.OfficeService = OfficeService;
