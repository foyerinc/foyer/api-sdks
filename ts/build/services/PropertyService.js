"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PropertyService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class PropertyService {
    /**
     * Get Properties
     * Get an object with an array of properties and a total count according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters
     * @param filter
     * @param verbosity
     * @returns any Successful
     * @throws ApiError
     */
    static getProperties(filter, verbosity) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property',
            headers: {
                'filter': filter,
            },
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Recommendations
     * Return an array of Properties according to preferences set on User and Shopper Models
     * @param limit
     * @param bufLen
     * @returns Property Successful
     * @throws ApiError
     */
    static getRecommendations(limit, bufLen) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/getRecommendations',
            query: {
                'limit': limit,
                'bufLen': bufLen,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find Nearest Properties
     * Provide an address to find the geographically closest Properties
     * @param streetNumber
     * @param street
     * @param streetType
     * @param cityName
     * @param state
     * @param lat
     * @param lng
     * @returns any Successful
     * @throws ApiError
     */
    static findNearestProperties(streetNumber, street, streetType, cityName, state, lat, lng) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/findNearest',
            query: {
                'streetNumber': streetNumber,
                'street': street,
                'streetType': streetType,
                'cityName': cityName,
                'state': state,
                'lat': lat,
                'lng': lng,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find One Property
     * Find one Property according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters
     * @param filter
     * @param verbosity
     * @returns Property Successful
     * @throws ApiError
     */
    static findOneProperty(filter, verbosity) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/findOne',
            headers: {
                'filter': filter,
            },
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Property by Id
     * Get specific Property with unique id. Also allows for verbosity parameter to shortcut commonly used filters
     * @param id
     * @param filter
     * @param verbosity
     * @returns Property Single Property
     * @throws ApiError
     */
    static getPropertyById(id, filter, verbosity) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Property Search
     * Search for properties according to defined criteria and desired image features
     * @param verbosity
     * @returns Property Successful
     * @throws ApiError
     */
    static propertySearch(verbosity) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/search',
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Compare property by id to local area
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateLocalGraph(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/{id}/calculateLocalGraph',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Calculate Foyer Score for Property
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateScore(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/{id}/calculateScore',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Gets a property by a viewer key
     * @param viewerKey
     * @returns GetPropertyByViewerKeyResponse The property corresponding to the viewer key
     * @throws ApiError
     */
    static getPropertyByViewerKey(viewerKey) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Property/propertyByViewerKey',
            query: {
                'viewerKey': viewerKey,
            },
        });
    }
}
exports.PropertyService = PropertyService;
