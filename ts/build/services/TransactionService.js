"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class TransactionService {
    /**
     * Get Transactions
     * Get Transactions according to defined filter criteria
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static getTransactions(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Transaction',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Transactions
     * Post an array of Transaction objects
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static postTransactions(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Transaction',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find One Transaction
     * Find a specific Transaction according to defined filter criteria
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static findOneTransaction(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Transaction/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Transaction By Id
     * Get a specific Transaction with unique id
     * @param id
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static getTransactionById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Transaction/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Transaction By Id
     * Patch a specific Transaction with unique id
     * @param id
     * @param requestBody
     * @returns Transaction patched Transaction
     * @throws ApiError
     */
    static patchTransactionById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Transaction/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Transaction By Id
     * Get a specific Transaction with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteTransactionById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Transaction/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Create or Update Transaction
     * Create or Update a single Transaction and report User activity
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static createOrUpdate(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Transaction/createOrUpdate',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.TransactionService = TransactionService;
