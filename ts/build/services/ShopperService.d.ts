import type { PatchBody } from '../models/PatchBody';
import type { Shopper } from '../models/Shopper';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class ShopperService {
    /**
     * Get Shoppers
     * Get an array of Shoppers according to defined filter criteria
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShoppers(filter?: string): CancelablePromise<Array<Shopper>>;
    /**
     * Post Shoppers
     * Post an array of Shopper objects
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    static postShoppers(requestBody: Shopper): CancelablePromise<Shopper>;
    /**
     * Find One Shopper
     * Find one specific Shopper according to defined filter criteria
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static findOneShopper(filter: string): CancelablePromise<Shopper>;
    /**
     * Get Shopper By Id
     * Get specific Shopper with unique Id
     * @param id
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShopperById(id: number, filter?: string): CancelablePromise<Shopper>;
    /**
     * Patch Shopper By Id
     * Patch specific Shopper with unique Id
     * @param id
     * @param requestBody
     * @returns Shopper patched Shopper
     * @throws ApiError
     */
    static patchShopperById(id: number, requestBody?: PatchBody): CancelablePromise<Shopper>;
    /**
     * Delete Shopper By Id
     * Delete specific Shopper with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShopperById(id: number): CancelablePromise<void>;
}
