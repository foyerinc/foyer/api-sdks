"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FavoriteService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class FavoriteService {
    /**
     * Get Favorites
     * Find Favorites according to defined filter criteria
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static getFavorites(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Favorite',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Favorites
     * Post an array of Favorite objects
     * @param requestBody
     * @returns Favorite Successful
     * @throws ApiError
     */
    static postFavorites(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Favorite',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find One Favorite
     * Find a single Favorite according to defined filter criteria
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static findOneFavorite(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Favorite/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Favorite By Id
     * Get a specific Favorite with unique id
     * @param id
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static getFavoriteById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Favorite/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Favorite By Id
     * Patch specific Favorite with unique id
     * @param id
     * @param requestBody
     * @returns Favorite patched Favorite
     * @throws ApiError
     */
    static patchFavoriteById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Favorite/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Favorite
     * Delete specific Favorite with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteFavoriteById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Favorite/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.FavoriteService = FavoriteService;
