"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class UserService {
    /**
     * Login
     * Login with credentials and receive auth token for use in subsequent API calls
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    static login(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/login',
            body: requestBody,
            mediaType: 'application/json',
            responseHeader: 'authorization',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Sign Up
     * Sign up a new User for access to Foyer Services
     * @param requestBody
     * @returns any User and related models created
     * @throws ApiError
     */
    static signUp(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/signUp',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Sign Up as a Publisher
     * Sign up for Foyer Syndication Services
     * @param requestBody
     * @returns any user and publisher created
     * @throws ApiError
     */
    static publisherSignUp(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/publisherSignup',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Change Password
     * Change current User's log in password
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static changePassword(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/changePassword',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Request SMS Verification
     * Generate and send code to User's provided phone number for verification
     * @param id
     * @param phone
     * @returns void
     * @throws ApiError
     */
    static requestSmsVerification(id, phone) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/User/{id}/requestSMSVerification',
            path: {
                'id': id,
            },
            query: {
                'phone': phone,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Verify SMS
     * Send user provided code to complete verification
     * @param id
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static verifySms(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/{id}/verifySMS',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Update User Location
     * Update a user's location for use with Home Discovery
     * @param id
     * @param requestBody
     * @returns User Successful
     * @throws ApiError
     */
    static updateUserLocation(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/{id}/updateUserLocation',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Send Reset Password Request
     * Sends a message to provided email to aid in password reset
     * @param requestBody
     * @returns any send reset password link in email to user
     * @throws ApiError
     */
    static resetPasswordStepOne(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/resetPasswordStepOne',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Confirm Reset Password
     * Send user provided newPassword along with previously requested token to finish resetting password
     * @param requestBody
     * @returns any Reset password
     * @throws ApiError
     */
    static resetPasswordStepTwo(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User/resetPasswordStepTwo',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Users
     * Find users according to defined filter criteria
     * @param filter
     * @returns User Array of Users
     * @throws ApiError
     */
    static getUsers(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/User',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Users
     * Post an array of User objects
     * @param requestBody
     * @returns User created User
     * @throws ApiError
     */
    static postUsers(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/User',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get User By Id
     * Find specific User with unique id
     * @param id
     * @param filter
     * @returns User single User
     * @throws ApiError
     */
    static getUserById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/User/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch User By Id
     * Patch specific User with unique id
     * @param id
     * @param requestBody
     * @returns User patched User
     * @throws ApiError
     */
    static patchUserById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/User/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.UserService = UserService;
