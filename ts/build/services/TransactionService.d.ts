import type { PatchBody } from '../models/PatchBody';
import type { Transaction } from '../models/Transaction';
import type { CancelablePromise } from '../core/CancelablePromise';
export declare class TransactionService {
    /**
     * Get Transactions
     * Get Transactions according to defined filter criteria
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static getTransactions(filter?: string): CancelablePromise<Array<Transaction>>;
    /**
     * Post Transactions
     * Post an array of Transaction objects
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static postTransactions(requestBody: Transaction): CancelablePromise<Transaction>;
    /**
     * Find One Transaction
     * Find a specific Transaction according to defined filter criteria
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static findOneTransaction(filter: string): CancelablePromise<Transaction>;
    /**
     * Get Transaction By Id
     * Get a specific Transaction with unique id
     * @param id
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static getTransactionById(id: number, filter?: string): CancelablePromise<Transaction>;
    /**
     * Patch Transaction By Id
     * Patch a specific Transaction with unique id
     * @param id
     * @param requestBody
     * @returns Transaction patched Transaction
     * @throws ApiError
     */
    static patchTransactionById(id: number, requestBody?: PatchBody): CancelablePromise<Transaction>;
    /**
     * Delete Transaction By Id
     * Get a specific Transaction with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteTransactionById(id: number): CancelablePromise<void>;
    /**
     * Create or Update Transaction
     * Create or Update a single Transaction and report User activity
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static createOrUpdate(requestBody: {
        transaction?: Transaction;
    }): CancelablePromise<Transaction>;
}
