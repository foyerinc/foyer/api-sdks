"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorFeedbackService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class ErrorFeedbackService {
    /**
     * Post ErrorFeedback
     * Post an ErrorFeedback object
     * @param requestBody
     * @returns ErrorFeedback Successful
     * @throws ApiError
     */
    static postErrorFeedback(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/ErrorFeedback',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.ErrorFeedbackService = ErrorFeedbackService;
