"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopperService = void 0;
const OpenAPI_1 = require("../core/OpenAPI");
const request_1 = require("../core/request");
class ShopperService {
    /**
     * Get Shoppers
     * Get an array of Shoppers according to defined filter criteria
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShoppers(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Shopper',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Post Shoppers
     * Post an array of Shopper objects
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    static postShoppers(requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'POST',
            url: '/Shopper',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Find One Shopper
     * Find one specific Shopper according to defined filter criteria
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static findOneShopper(filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Shopper/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Get Shopper By Id
     * Get specific Shopper with unique Id
     * @param id
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShopperById(id, filter) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'GET',
            url: '/Shopper/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Patch Shopper By Id
     * Patch specific Shopper with unique Id
     * @param id
     * @param requestBody
     * @returns Shopper patched Shopper
     * @throws ApiError
     */
    static patchShopperById(id, requestBody) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'PATCH',
            url: '/Shopper/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }
    /**
     * Delete Shopper By Id
     * Delete specific Shopper with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShopperById(id) {
        return (0, request_1.request)(OpenAPI_1.OpenAPI, {
            method: 'DELETE',
            url: '/Shopper/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }
}
exports.ShopperService = ShopperService;
