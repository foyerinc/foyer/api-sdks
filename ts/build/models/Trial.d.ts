import type { Media } from './Media';
import type { User } from './User';
export declare type Trial = {
    id?: number;
    userId?: number;
    mediaId?: number;
    result?: string;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
    Media?: Media;
};
