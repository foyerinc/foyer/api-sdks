import type { User } from './User';
export declare type ApiUsage = {
    id?: number;
    userId?: number;
    year?: number;
    month?: number;
    credits?: number;
    requests?: number;
    trial?: boolean;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
};
