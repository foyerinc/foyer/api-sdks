import type { Syndication } from './Syndication';
import type { User } from './User';
export declare type Publisher = {
    id?: number;
    userId?: number;
    name?: string;
    website?: string;
    logo?: string;
    active?: boolean;
    createdAt?: string;
    updatedAt?: string;
    User?: User;
    Syndications?: Array<Syndication>;
};
