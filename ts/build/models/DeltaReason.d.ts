export declare type DeltaReason = {
    tag?: string;
    problem?: string;
    recommendation?: string;
    modifier?: number;
    passed?: boolean;
    url?: string;
    mediaKey?: string;
};
