import type { Member } from './Member';
import type { OUID } from './OUID';
export declare type TeamMembers = {
    id?: number;
    MemberKeyNumeric?: number;
    TeamKeyNumeric?: number;
    TeamMemberKeyNumeric?: number;
    MemberKey?: string;
    MemberLoginId?: string;
    MemberMlsId?: string;
    OriginatingSystemID?: number;
    OriginatingSystemKey?: string;
    OriginatingSystemName?: string;
    SourceSystemID?: number;
    SourceSystemKey?: string;
    SourceSystemName?: string;
    TeamKey?: string;
    TeamMemberKey?: string;
    TeamMemberNationalAssociationId?: string;
    TeamMemberStateLicense?: string;
    TeamImpersonationLevel?: string;
    TeamMemberType?: string;
    ModificationTimestamp?: string;
    OriginalEntryTimestamp?: string;
    Member?: Member;
    OriginatingSystem?: OUID;
    SourceSystem?: OUID;
};
