"use strict";
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchDocument = void 0;
var PatchDocument;
(function (PatchDocument) {
    /**
     * The operation to be performed
     */
    let op;
    (function (op) {
        op["ADD"] = "add";
        op["REMOVE"] = "remove";
        op["REPLACE"] = "replace";
        op["MOVE"] = "move";
        op["COPY"] = "copy";
        op["TEST"] = "test";
    })(op = PatchDocument.op || (PatchDocument.op = {}));
})(PatchDocument = exports.PatchDocument || (exports.PatchDocument = {}));
