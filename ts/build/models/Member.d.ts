import type { Contacts } from './Contacts';
import type { HistoryTransactional } from './HistoryTransactional';
import type { Office } from './Office';
import type { OpenHouse } from './OpenHouse';
import type { OUID } from './OUID';
import type { Property } from './Property';
import type { Prospecting } from './Prospecting';
import type { SavedSearch } from './SavedSearch';
import type { Showing } from './Showing';
import type { TeamMembers } from './TeamMembers';
import type { Teams } from './Teams';
export declare type Member = {
    id?: number;
    MemberMlsAccessYN?: boolean;
    MemberAORkeyNumeric?: number;
    MemberKeyNumeric?: number;
    OfficeKeyNumeric?: number;
    JobTitle?: string;
    MemberAORMlsId?: string;
    MemberAORkey?: string;
    MemberAddress1?: string;
    MemberAddress2?: string;
    MemberAssociationComments?: string;
    MemberCarrierRoute?: string;
    MemberCity?: string;
    MemberDirectPhone?: string;
    MemberEmail?: string;
    MemberFax?: string;
    MemberFirstName?: string;
    MemberFullName?: string;
    MemberHomePhone?: string;
    MemberIsAssistantTo?: string;
    MemberKey?: string;
    MemberLastName?: string;
    MemberLoginId?: string;
    MemberMiddleName?: string;
    MemberMlsId?: string;
    MemberMobilePhone?: string;
    MemberNamePrefix?: string;
    MemberNameSuffix?: string;
    MemberNationalAssociationId?: string;
    MemberNickname?: string;
    MemberOfficePhone?: string;
    MemberOfficePhoneExt?: string;
    MemberPager?: string;
    MemberPassword?: string;
    MemberPhoneTTYTDD?: string;
    MemberPostalCode?: string;
    MemberPostalCodePlus4?: string;
    MemberPreferredPhone?: string;
    MemberPreferredPhoneExt?: string;
    MemberStateLicense?: string;
    MemberTollFreePhone?: string;
    MemberVoiceMail?: string;
    MemberVoiceMailExt?: string;
    OfficeKey?: string;
    OfficeMlsId?: string;
    OfficeName?: string;
    OriginatingSystemID?: string;
    OriginatingSystemMemberKey?: string;
    OriginatingSystemName?: string;
    SourceSystemID?: string;
    SourceSystemMemberKey?: string;
    SourceSystemName?: string;
    MemberDesignation?: Array<string>;
    MemberLanguages?: Array<string>;
    SyndicateTo?: Array<string>;
    MemberAOR?: string;
    MemberCountry?: string;
    MemberCountyOrParish?: string;
    MemberMlsSecurityClass?: string;
    MemberOtherPhoneType?: string;
    MemberStateLicenseState?: string;
    MemberStateOrProvince?: string;
    MemberStatus?: string;
    MemberType?: string;
    SocialMediaType?: string;
    LastLoginTimestamp?: string;
    ModificationTimestamp?: string;
    OriginalEntryTimestamp?: string;
    Office?: Office;
    OriginatingSystem?: OUID;
    SourceSystem?: OUID;
    Contacts?: Array<Contacts>;
    HistoryTransactionals?: Array<HistoryTransactional>;
    OUIDs?: Array<OUID>;
    BrokerOffices?: Array<Office>;
    ManagerOffices?: Array<Office>;
    OpenHouses?: Array<OpenHouse>;
    BuyerAgentProperties?: Array<Property>;
    CoBuyerAgentProperties?: Array<Property>;
    CoListAgentProperties?: Array<Property>;
    ListAgentProperties?: Array<Property>;
    Prospectings?: Array<Prospecting>;
    SavedSearches?: Array<SavedSearch>;
    Showings?: Array<Showing>;
    TeamMembers?: Array<TeamMembers>;
    Teams?: Array<Teams>;
};
