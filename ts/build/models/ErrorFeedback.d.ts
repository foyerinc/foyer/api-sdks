import type { User } from './User';
export declare type ErrorFeedback = {
    id?: number;
    userId?: number;
    url?: string;
    error?: any;
    message?: string;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
};
