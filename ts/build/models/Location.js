"use strict";
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Location = void 0;
var Location;
(function (Location) {
    let type;
    (function (type) {
        type["POINT"] = "Point";
    })(type = Location.type || (Location.type = {}));
})(Location = exports.Location || (exports.Location = {}));
