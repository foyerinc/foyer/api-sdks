import type { Media } from './Media';
export declare type Label = {
    id?: number;
    mediaId?: number;
    name?: string;
    classId?: number;
    confidence?: number;
    rank?: number;
    source?: string;
    updatedAt?: string;
    createdAt?: string;
    Media?: Media;
};
