import type { Property } from './Property';
export declare type GetPropertyByViewerKeyResponse = {
    initialEnvisionAssetId?: number;
    property?: Property;
};
