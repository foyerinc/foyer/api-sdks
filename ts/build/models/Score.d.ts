import type { DeltaReason } from './DeltaReason';
export declare type Score = {
    score?: number;
    reasons?: Array<DeltaReason>;
};
