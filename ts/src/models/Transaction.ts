/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Property } from './Property';
import type { User } from './User';

export type Transaction = {
    id?: number;
    userId?: number;
    propertyId?: number;
    type?: string;
    detailsViewed?: boolean;
    imagesViewed?: number;
    timeToDecision?: number;
    filter?: string;
    session?: string;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
    Property?: Property;
};

