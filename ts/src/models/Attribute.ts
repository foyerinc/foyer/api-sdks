/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Attribute = {
    name?: string;
    confidence?: number;
    value?: Array<number>;
};

