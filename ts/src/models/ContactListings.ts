/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Contacts } from './Contacts';
import type { Property } from './Property';

export type ContactListings = {
    id?: number;
    AgentNotesUnreadYN?: boolean;
    ContactNotesUnreadYN?: boolean;
    DirectEmailYN?: boolean;
    ListingViewedYN?: boolean;
    ContactKeyNumeric?: number;
    ContactListingsKeyNumeric?: number;
    ListingKeyNumeric?: number;
    ContactKey?: string;
    ContactListingsKey?: string;
    ContactLoginId?: string;
    ListingId?: string;
    ListingKey?: string;
    ClassName?: string;
    ContactListingPreference?: string;
    ResourceName?: string;
    LastAgentNoteTimestamp?: string;
    LastContactNoteTimestamp?: string;
    ListingModificationTimestamp?: string;
    ListingSentTimestamp?: string;
    ModificationTimestamp?: string;
    PortalLastVisitedTimestamp?: string;
    Contact?: Contacts;
    Property?: Property;
};

