/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Property } from './Property';

export type GetPropertyByViewerKeyResponse = {
    initialEnvisionAssetId?: number;
    property?: Property;
};

