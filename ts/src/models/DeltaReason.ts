/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type DeltaReason = {
    tag?: string;
    problem?: string;
    recommendation?: string;
    modifier?: number;
    passed?: boolean;
    url?: string;
    mediaKey?: string;
};

