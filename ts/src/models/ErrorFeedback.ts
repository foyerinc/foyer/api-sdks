/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { User } from './User';

export type ErrorFeedback = {
    id?: number;
    userId?: number;
    url?: string;
    error?: any;
    message?: string;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
};

