/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Syndication } from './Syndication';
import type { User } from './User';

export type Publisher = {
    id?: number;
    userId?: number;
    name?: string;
    website?: string;
    logo?: string;
    active?: boolean;
    createdAt?: string;
    updatedAt?: string;
    User?: User;
    Syndications?: Array<Syndication>;
};

