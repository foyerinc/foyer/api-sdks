/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Property } from './Property';
import type { User } from './User';

export type Share = {
    id?: number;
    seen?: boolean;
    receiverId?: number;
    senderId?: number;
    propertyId?: number;
    updatedAt?: string;
    createdAt?: string;
    Receiver?: User;
    Sender?: User;
    Property?: Property;
};

