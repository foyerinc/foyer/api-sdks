/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Property } from './Property';

export type PropertyRooms = {
    id?: number;
    ListingKeyNumeric?: number;
    RoomArea?: number;
    RoomKeyNumeric?: number;
    RoomLength?: number;
    RoomWidth?: number;
    ListingId?: string;
    ListingKey?: string;
    RoomDescription?: string;
    RoomDimensions?: string;
    RoomKey?: string;
    RoomFeatures?: Array<string>;
    RoomAreaSource?: string;
    RoomAreaUnits?: string;
    RoomLengthWidthUnits?: string;
    RoomType?: string;
    ModificationTimestamp?: string;
    Property?: Property;
};

