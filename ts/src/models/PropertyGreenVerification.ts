/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Property } from './Property';

export type PropertyGreenVerification = {
    id?: number;
    GreenBuildingVerificationKeyNumeric?: number;
    GreenVerificationMetric?: number;
    GreenVerificationYear?: number;
    ListingKeyNumeric?: number;
    GreenBuildingVerificationKey?: string;
    GreenVerificationBody?: string;
    GreenVerificationRating?: string;
    GreenVerificationURL?: string;
    GreenVerificationVersion?: string;
    ListingId?: string;
    ListingKey?: string;
    GreenBuildingVerificationType?: string;
    GreenVerificationSource?: string;
    GreenVerificationStatus?: string;
    ModificationTimestamp?: string;
    Property?: Property;
};

