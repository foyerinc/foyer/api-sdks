/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Member } from './Member';
import type { OUID } from './OUID';

export type HistoryTransactional = {
    id?: number;
    ChangedByMemberKeyNumeric?: number;
    FieldKeyNumeric?: number;
    HistoryTransactionalKeyNumeric?: number;
    ResourceRecordKeyNumeric?: number;
    ChangedByMemberID?: string;
    ChangedByMemberKey?: string;
    ClassName?: string;
    FieldKey?: string;
    FieldName?: string;
    HistoryTransactionalKey?: string;
    NewValue?: string;
    OriginatingSystemHistoryKey?: string;
    OriginatingSystemID?: number;
    OriginatingSystemName?: string;
    PreviousValue?: string;
    ResourceName?: string;
    ResourceRecordID?: string;
    ResourceRecordKey?: string;
    SourceSystemHistoryKey?: string;
    SourceSystemID?: number;
    SourceSystemName?: string;
    ChangeType?: string;
    ModificationTimestamp?: string;
    ChangedByMember?: Member;
    OriginatingSystem?: OUID;
    SourceSystem?: OUID;
};

