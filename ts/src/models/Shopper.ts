/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Member } from './Member';
import type { User } from './User';

export type Shopper = {
    id?: number;
    userId?: number;
    memberId?: number;
    username?: string;
    price?: number;
    priceVerified?: boolean;
    locationVerified?: boolean;
    tutorialStep?: number;
    searchRadius?: number;
    searchCity?: string;
    filterLocation?: boolean;
    searchHistory?: string;
    imageFilter?: string;
    createdAt?: string;
    updatedAt?: string;
    User?: User;
    Member?: Member;
};

