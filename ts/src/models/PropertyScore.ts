/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Score } from './Score';

export type PropertyScore = {
    categoryScores?: {
        bathroom?: Score;
        bedroom?: Score;
        dining_room?: Score;
        kitchen?: Score;
        living_room?: Score;
        outdoor?: Score;
    };
    inferenceScores?: {
        all?: {
            naturalLight?: Score;
            presentation?: Score;
            spaciousness?: Score;
            trendiness?: Score;
        };
        bathroom?: {
            trendiness?: Score;
        };
        bedroom?: {
            spaciousness?: Score;
            trendiness?: Score;
        };
        dining_room?: {
            spaciousness?: Score;
            trendiness?: Score;
        };
        kitchen?: {
            presentation?: Score;
            spaciousness?: Score;
            trendiness?: Score;
        };
        living_room?: {
            presentation?: Score;
            spaciousness?: Score;
            trendiness?: Score;
        };
    };
    mediaCountScores?: {
        bounds?: Score;
    };
    categoryPhotosPresent?: {
        bathroom?: boolean;
        bedroom?: boolean;
        dining_room?: boolean;
        kitchen?: boolean;
        living_room?: boolean;
        outdoor?: boolean;
    };
    finalCategoryScores?: {
        bathroom?: number;
        bedroom?: number;
        dining_room?: number;
        kitchen?: number;
        living_room?: number;
        outdoor?: number;
    };
    bonusScore?: Score;
    finalScore?: number;
};

