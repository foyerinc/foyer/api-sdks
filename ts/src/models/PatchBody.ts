/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PatchDocument } from './PatchDocument';

export type PatchBody = Array<PatchDocument>;
