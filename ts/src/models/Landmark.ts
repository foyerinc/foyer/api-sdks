/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Landmark = {
    id?: number;
    envisionAssetId?: number;
    class?: string;
    confidence?: number;
    position?: Array<number>;
    features?: Array<string>;
    mediaIds?: Array<number>;
    updatedAt?: string;
    createdAt?: string;
};

