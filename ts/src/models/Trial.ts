/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Media } from './Media';
import type { User } from './User';

export type Trial = {
    id?: number;
    userId?: number;
    mediaId?: number;
    result?: string;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
    Media?: Media;
};

