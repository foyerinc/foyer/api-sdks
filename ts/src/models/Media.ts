/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Label } from './Label';
import type { Segmap } from './Segmap';
import type { Trial } from './Trial';

export type Media = {
    id?: number;
    MediaKey?: string;
    PreferredPhotoYN?: boolean;
    Deleted?: boolean;
    ChangedByMemberKeyNumeric?: number;
    ImageHeight?: number;
    ImageWidth?: number;
    MediaKeyNumeric?: number;
    Order?: number;
    ResourceRecordKeyNumeric?: number;
    Hash?: string;
    ChangedByMemberID?: string;
    ChangedByMemberKey?: string;
    LongDescription?: string;
    MediaHTML?: string;
    MediaObjectID?: string;
    MediaURL?: string;
    OriginatingSystemID?: string;
    OriginatingSystemMediaKey?: string;
    OriginatingSystemName?: string;
    ResourceRecordID?: string;
    ResourceRecordKey?: string;
    ShortDescription?: string;
    SourceSystemID?: string;
    SourceSystemMediaKey?: string;
    SourceSystemName?: string;
    Permission?: Array<string>;
    ClassName?: string;
    ImageOf?: string;
    ImageSizeDescription?: string;
    MediaCategory?: string;
    MediaStatus?: string;
    MediaType?: string;
    ResourceName?: string;
    ModificationTimestamp?: string;
    MediaModificationTimestamp?: string;
    updatedAt?: string;
    createdAt?: string;
    Labels?: Array<Label>;
    Segmaps?: Array<Segmap>;
    Trials?: Array<Trial>;
};

