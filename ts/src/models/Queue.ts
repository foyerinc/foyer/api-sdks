/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { OUID } from './OUID';

export type Queue = {
    id?: number;
    QueueTransactionKeyNumeric?: number;
    ResourceRecordKeyNumeric?: number;
    OriginatingSystemID?: number;
    OriginatingSystemName?: string;
    OriginatingSystemQueueKey?: string;
    QueueTransactionKey?: string;
    ResourceRecordID?: string;
    ResourceRecordKey?: string;
    SourceSystemID?: string;
    SourceSystemName?: string;
    SourceSystemQueueKey?: string;
    ClassName?: string;
    QueueTransactionType?: string;
    ResourceName?: string;
    ModificationTimestamp?: string;
    OriginatingSystem?: OUID;
    SourceSystem?: OUID;
};

