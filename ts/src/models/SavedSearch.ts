/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Member } from './Member';
import type { OUID } from './OUID';
import type { Prospecting } from './Prospecting';

export type SavedSearch = {
    id?: number;
    MemberKeyNumeric?: number;
    SavedSearchKeyNumeric?: number;
    MemberKey?: string;
    MemberMlsId?: string;
    OriginatingSystemID?: number;
    OriginatingSystemKey?: string;
    OriginatingSystemMemberKey?: string;
    OriginatingSystemMemberName?: string;
    OriginatingSystemName?: string;
    SavedSearchDescription?: string;
    SavedSearchKey?: string;
    SavedSearchName?: string;
    SearchQuery?: string;
    SearchQueryExceptionDetails?: string;
    SearchQueryHumanReadable?: string;
    SourceSystemID?: number;
    SourceSystemKey?: string;
    SourceSystemName?: string;
    ClassName?: string;
    ResourceName?: string;
    SavedSearchType?: string;
    SearchQueryExceptions?: string;
    SearchQueryType?: string;
    ModificationTimestamp?: string;
    OriginalEntryTimestamp?: string;
    Member?: Member;
    OriginatingSystem?: OUID;
    SourceSystem?: OUID;
    Prospectings?: Array<Prospecting>;
};

