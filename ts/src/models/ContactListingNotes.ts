/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Contacts } from './Contacts';

export type ContactListingNotes = {
    id?: number;
    ContactKeyNumeric?: number;
    ContactListingNotesKeyNumeric?: number;
    ListingKeyNumeric?: number;
    ContactKey?: string;
    ContactListingNotesKey?: string;
    ListingId?: string;
    ListingKey?: string;
    NoteContents?: string;
    NotedBy?: string;
    ModificationTimestamp?: string;
    Contact?: Contacts;
};

