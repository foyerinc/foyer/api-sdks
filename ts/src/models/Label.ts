/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Media } from './Media';

export type Label = {
    id?: number;
    mediaId?: number;
    name?: string;
    classId?: number;
    confidence?: number;
    rank?: number;
    source?: string;
    updatedAt?: string;
    createdAt?: string;
    Media?: Media;
};

