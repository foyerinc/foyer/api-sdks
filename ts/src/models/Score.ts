/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DeltaReason } from './DeltaReason';

export type Score = {
    score?: number;
    reasons?: Array<DeltaReason>;
};

