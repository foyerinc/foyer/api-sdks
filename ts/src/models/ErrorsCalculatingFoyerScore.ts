/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ErrorsCalculatingFoyerScore = {
    reportHasRequiredInformation?: boolean;
};

