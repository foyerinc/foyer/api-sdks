/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Landmark } from './Landmark';
import type { Property } from './Property';

export type EnvisionAsset = {
    id?: number;
    ListingKey?: string;
    aboundId?: string;
    thumbnail?: string;
    topDown?: string;
    glb?: string;
    obj?: string;
    dump?: string;
    tags?: any;
    updatedAt?: string;
    createdAt?: string;
    Property?: Property;
    Landmarks?: Array<Landmark>;
};

