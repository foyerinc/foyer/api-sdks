/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Coordinates } from './Coordinates';

export type Location = {
    type: Location.type;
    coordinates: Coordinates;
};

export namespace Location {

    export enum type {
        POINT = 'Point',
    }


}

