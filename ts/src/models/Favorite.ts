/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Property } from './Property';
import type { User } from './User';

export type Favorite = {
    id?: number;
    userId?: number;
    propertyId?: number;
    session?: string;
    filter?: string;
    detailsViewed?: boolean;
    imagesViewed?: number;
    notes?: string;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
    Property?: Property;
};

