/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { User } from './User';

export type ApiUsage = {
    id?: number;
    userId?: number;
    year?: number;
    month?: number;
    credits?: number;
    requests?: number;
    trial?: boolean;
    updatedAt?: string;
    createdAt?: string;
    User?: User;
};

