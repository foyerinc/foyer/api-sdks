/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Favorite } from '../models/Favorite';
import type { PatchBody } from '../models/PatchBody';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class FavoriteService {

    /**
     * Get Favorites
     * Find Favorites according to defined filter criteria
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    public static getFavorites(
        filter?: string,
    ): CancelablePromise<Array<Favorite>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Favorite',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Favorites
     * Post an array of Favorite objects
     * @param requestBody
     * @returns Favorite Successful
     * @throws ApiError
     */
    public static postFavorites(
        requestBody: Favorite,
    ): CancelablePromise<Favorite> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Favorite',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find One Favorite
     * Find a single Favorite according to defined filter criteria
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    public static findOneFavorite(
        filter: string,
    ): CancelablePromise<Favorite> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Favorite/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Favorite By Id
     * Get a specific Favorite with unique id
     * @param id
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    public static getFavoriteById(
        id: number,
        filter?: string,
    ): CancelablePromise<Favorite> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Favorite/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Favorite By Id
     * Patch specific Favorite with unique id
     * @param id
     * @param requestBody
     * @returns Favorite patched Favorite
     * @throws ApiError
     */
    public static patchFavoriteById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Favorite> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Favorite/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Favorite
     * Delete specific Favorite with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteFavoriteById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Favorite/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
