/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PatchBody } from '../models/PatchBody';
import type { Share } from '../models/Share';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ShareService {

    /**
     * Get Shares
     * Get an array of shares according to defined filter criteria
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    public static getShares(
        filter?: string,
    ): CancelablePromise<Array<Share>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Share',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Shares
     * Post an array of Share objects
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    public static postShares(
        requestBody: Share,
    ): CancelablePromise<Share> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Share',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Make Share
     * Make a share with specific contact information
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    public static makeShare(
        requestBody: {
            propertyId?: number;
            phoneNumber?: string;
            firstName?: string;
            lastName?: string;
        },
    ): CancelablePromise<Share> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Share/makeShare',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find One Share
     * Find a single Share according to defined filter criteria
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    public static findOneShare(
        filter: string,
    ): CancelablePromise<Share> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Share/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Share By Id
     * Get a specific Share with unique id
     * @param id
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    public static getShareById(
        id: number,
        filter?: string,
    ): CancelablePromise<Share> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Share/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Share By Id
     * Patch a specific Share with unique id
     * @param id
     * @param requestBody
     * @returns Share patched Share
     * @throws ApiError
     */
    public static patchShareById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Share> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Share/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Share By Id
     * Delete a specific Share with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteShareById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Share/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
