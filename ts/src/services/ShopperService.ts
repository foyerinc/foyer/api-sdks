/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PatchBody } from '../models/PatchBody';
import type { Shopper } from '../models/Shopper';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ShopperService {

    /**
     * Get Shoppers
     * Get an array of Shoppers according to defined filter criteria
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    public static getShoppers(
        filter?: string,
    ): CancelablePromise<Array<Shopper>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Shopper',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Shoppers
     * Post an array of Shopper objects
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    public static postShoppers(
        requestBody: Shopper,
    ): CancelablePromise<Shopper> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Shopper',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find One Shopper
     * Find one specific Shopper according to defined filter criteria
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    public static findOneShopper(
        filter: string,
    ): CancelablePromise<Shopper> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Shopper/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Shopper By Id
     * Get specific Shopper with unique Id
     * @param id
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    public static getShopperById(
        id: number,
        filter?: string,
    ): CancelablePromise<Shopper> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Shopper/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Shopper By Id
     * Patch specific Shopper with unique Id
     * @param id
     * @param requestBody
     * @returns Shopper patched Shopper
     * @throws ApiError
     */
    public static patchShopperById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Shopper> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Shopper/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Shopper By Id
     * Delete specific Shopper with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteShopperById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Shopper/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
