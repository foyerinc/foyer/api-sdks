/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Publisher } from '../models/Publisher';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class PublisherService {

    /**
     * Get Publishers
     * Get an array of Publishers according to defined filter criteria
     * @param filter
     * @returns Publisher array of Publishers
     * @throws ApiError
     */
    public static getPublishers(
        filter?: string,
    ): CancelablePromise<Array<Publisher>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Publisher',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Publishers
     * Post an array of Publisher objects
     * @param requestBody
     * @returns Publisher created Publisher
     * @throws ApiError
     */
    public static postPublishers(
        requestBody: Publisher,
    ): CancelablePromise<Publisher> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Publisher',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Refresh All Properties
     * Retrieve an array of urls for use to refresh all Publisher linked properties
     * @returns string array of urls
     * @throws ApiError
     */
    public static refreshAllProperties(): CancelablePromise<Array<string>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Publisher/refreshAllProperties',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Update Notify
     * Update publisher notification endpoint to keep up to date on property feed
     * @param requestBody
     * @returns Publisher Successful
     * @throws ApiError
     */
    public static updateNotify(
        requestBody: {
            notify?: string;
        },
    ): CancelablePromise<Publisher> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Publisher/updateNotify',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
