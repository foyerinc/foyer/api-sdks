/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PatchBody } from '../models/PatchBody';
import type { Transaction } from '../models/Transaction';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class TransactionService {

    /**
     * Get Transactions
     * Get Transactions according to defined filter criteria
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    public static getTransactions(
        filter?: string,
    ): CancelablePromise<Array<Transaction>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Transaction',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Transactions
     * Post an array of Transaction objects
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    public static postTransactions(
        requestBody: Transaction,
    ): CancelablePromise<Transaction> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Transaction',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find One Transaction
     * Find a specific Transaction according to defined filter criteria
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    public static findOneTransaction(
        filter: string,
    ): CancelablePromise<Transaction> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Transaction/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Transaction By Id
     * Get a specific Transaction with unique id
     * @param id
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    public static getTransactionById(
        id: number,
        filter?: string,
    ): CancelablePromise<Transaction> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Transaction/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Transaction By Id
     * Patch a specific Transaction with unique id
     * @param id
     * @param requestBody
     * @returns Transaction patched Transaction
     * @throws ApiError
     */
    public static patchTransactionById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Transaction> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Transaction/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Transaction By Id
     * Get a specific Transaction with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteTransactionById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Transaction/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Create or Update Transaction
     * Create or Update a single Transaction and report User activity
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    public static createOrUpdate(
        requestBody: {
            transaction?: Transaction;
        },
    ): CancelablePromise<Transaction> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Transaction/createOrUpdate',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
