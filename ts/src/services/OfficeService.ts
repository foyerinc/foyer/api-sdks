/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Office } from '../models/Office';
import type { PatchBody } from '../models/PatchBody';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class OfficeService {

    /**
     * Find One Office
     * Fine one Office according to defined filter criteria
     * @param filter
     * @returns Office Successful
     * @throws ApiError
     */
    public static findOneOffice(
        filter: string,
    ): CancelablePromise<Office> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Office/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Office by Id
     * Get a specific Office with unique id
     * @param id
     * @param filter
     * @returns Office single Office
     * @throws ApiError
     */
    public static getOfficeById(
        id: number,
        filter?: string,
    ): CancelablePromise<Office> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Office/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Office by Id
     * Patch a specific Office with unique id
     * @param id
     * @param requestBody
     * @returns Office patched Office
     * @throws ApiError
     */
    public static patchOfficeById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Office> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Office/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Office by Id
     * Delete a specific Office with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteOfficeById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Office/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
