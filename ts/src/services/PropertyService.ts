/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ErrorsCalculatingFoyerScore } from '../models/ErrorsCalculatingFoyerScore';
import type { GetPropertyByViewerKeyResponse } from '../models/GetPropertyByViewerKeyResponse';
import type { Property } from '../models/Property';
import type { PropertyScore } from '../models/PropertyScore';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class PropertyService {

    /**
     * Get Properties
     * Get an object with an array of properties and a total count according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters
     * @param filter
     * @param verbosity
     * @returns any Successful
     * @throws ApiError
     */
    public static getProperties(
        filter?: string,
        verbosity?: string,
    ): CancelablePromise<{
        count?: number;
        rows?: Array<Property>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property',
            headers: {
                'filter': filter,
            },
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Recommendations
     * Return an array of Properties according to preferences set on User and Shopper Models
     * @param limit
     * @param bufLen
     * @returns Property Successful
     * @throws ApiError
     */
    public static getRecommendations(
        limit?: number,
        bufLen?: number,
    ): CancelablePromise<Array<Property>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/getRecommendations',
            query: {
                'limit': limit,
                'bufLen': bufLen,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find Nearest Properties
     * Provide an address to find the geographically closest Properties
     * @param streetNumber
     * @param street
     * @param streetType
     * @param cityName
     * @param state
     * @param lat
     * @param lng
     * @returns any Successful
     * @throws ApiError
     */
    public static findNearestProperties(
        streetNumber: string,
        street: string,
        streetType: string,
        cityName: string,
        state: string,
        lat: number,
        lng: number,
    ): CancelablePromise<{
        bestChoice?: Property;
        neighbors?: Array<Property>;
        properties?: Array<Property>;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/findNearest',
            query: {
                'streetNumber': streetNumber,
                'street': street,
                'streetType': streetType,
                'cityName': cityName,
                'state': state,
                'lat': lat,
                'lng': lng,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find One Property
     * Find one Property according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters
     * @param filter
     * @param verbosity
     * @returns Property Successful
     * @throws ApiError
     */
    public static findOneProperty(
        filter: string,
        verbosity?: string,
    ): CancelablePromise<Property> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/findOne',
            headers: {
                'filter': filter,
            },
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Property by Id
     * Get specific Property with unique id. Also allows for verbosity parameter to shortcut commonly used filters
     * @param id
     * @param filter
     * @param verbosity
     * @returns Property Single Property
     * @throws ApiError
     */
    public static getPropertyById(
        id: number,
        filter?: string,
        verbosity?: string,
    ): CancelablePromise<Property> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Property Search
     * Search for properties according to defined criteria and desired image features
     * @param verbosity
     * @returns Property Successful
     * @throws ApiError
     */
    public static propertySearch(
        verbosity?: string,
    ): CancelablePromise<Array<Property>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/search',
            query: {
                'verbosity': verbosity,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Compare property by id to local area
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    public static calculateLocalGraph(
        id: number,
        filter?: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/{id}/calculateLocalGraph',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Calculate Foyer Score for Property
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    public static calculateScore(
        id: number,
        filter?: string,
    ): CancelablePromise<(PropertyScore | ErrorsCalculatingFoyerScore)> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/{id}/calculateScore',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Gets a property by a viewer key
     * @param viewerKey
     * @returns GetPropertyByViewerKeyResponse The property corresponding to the viewer key
     * @throws ApiError
     */
    public static getPropertyByViewerKey(
        viewerKey?: string,
    ): CancelablePromise<GetPropertyByViewerKeyResponse> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Property/propertyByViewerKey',
            query: {
                'viewerKey': viewerKey,
            },
        });
    }

}
