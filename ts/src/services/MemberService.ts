/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Member } from '../models/Member';
import type { PatchBody } from '../models/PatchBody';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class MemberService {

    /**
     * Find One Member
     * Fine one Member according to defined filter criteria
     * @param filter
     * @returns Member Successful
     * @throws ApiError
     */
    public static findOneMember(
        filter: string,
    ): CancelablePromise<Member> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Member/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Member by Id
     * Get specific Member with unique Id
     * @param id
     * @param filter
     * @returns Member single Member
     * @throws ApiError
     */
    public static getMemberById(
        id: number,
        filter?: string,
    ): CancelablePromise<Member> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Member/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Member by Id
     * Patch specific Member with unique Id
     * @param id
     * @param requestBody
     * @returns Member patched Member
     * @throws ApiError
     */
    public static patchMemberById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Member> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Member/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Member by Id
     * Delete specific Member with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteMemberById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Member/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
