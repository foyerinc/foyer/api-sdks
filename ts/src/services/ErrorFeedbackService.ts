/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ErrorFeedback } from '../models/ErrorFeedback';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ErrorFeedbackService {

    /**
     * Post ErrorFeedback
     * Post an ErrorFeedback object
     * @param requestBody
     * @returns ErrorFeedback Successful
     * @throws ApiError
     */
    public static postErrorFeedback(
        requestBody: ErrorFeedback,
    ): CancelablePromise<ErrorFeedback> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/ErrorFeedback',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
