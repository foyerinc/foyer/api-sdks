/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Landmark } from '../models/Landmark';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class EnvisionAssetService {

    /**
     * get presigned url of the model
     * Get presigned url of the model corresponding to the envision asset
     * @param id
     * @returns string The presigned url of the model
     * @throws ApiError
     */
    public static getModelPresignedUrl(
        id: number,
    ): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/EnvisionAsset/{id}/modelPresignedUrl',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Add landmarks for the envision asset
     * @param id
     * @param requestBody
     * @returns Landmark The inserted landmarks
     * @throws ApiError
     */
    public static insertLandmarks(
        id: number,
        requestBody?: {
            landmarks?: Array<Landmark>;
            deleteExistingLandmarks?: boolean;
        },
    ): CancelablePromise<Array<Landmark>> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/EnvisionAsset/{id}/insertLandmarks',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
