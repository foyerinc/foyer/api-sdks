/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Location } from '../models/Location';
import type { PatchBody } from '../models/PatchBody';
import type { User } from '../models/User';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class UserService {

    /**
     * Login
     * Login with credentials and receive auth token for use in subsequent API calls
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    public static login(
        requestBody: {
            email?: string;
            password?: string;
            source?: string;
        },
    ): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/login',
            body: requestBody,
            mediaType: 'application/json',
            responseHeader: 'authorization',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Sign Up
     * Sign up a new User for access to Foyer Services
     * @param requestBody
     * @returns any User and related models created
     * @throws ApiError
     */
    public static signUp(
        requestBody: {
            firstName?: string;
            lastName?: string;
            email?: string;
            password?: string;
            confirmedPassword?: string;
            source?: string;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/signUp',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Sign Up as a Publisher
     * Sign up for Foyer Syndication Services
     * @param requestBody
     * @returns any user and publisher created
     * @throws ApiError
     */
    public static publisherSignUp(
        requestBody: {
            email?: string;
            password?: string;
            confirmedPassword?: string;
            name?: string;
            website?: string;
            logo?: string;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/publisherSignup',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Change Password
     * Change current User's log in password
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public static changePassword(
        requestBody: {
            currentPassword?: string;
            newPassword?: string;
            confirmNewPassword?: string;
        },
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/changePassword',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Request SMS Verification
     * Generate and send code to User's provided phone number for verification
     * @param id
     * @param phone
     * @returns void
     * @throws ApiError
     */
    public static requestSmsVerification(
        id: number,
        phone: string,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/User/{id}/requestSMSVerification',
            path: {
                'id': id,
            },
            query: {
                'phone': phone,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Verify SMS
     * Send user provided code to complete verification
     * @param id
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public static verifySms(
        id: number,
        requestBody: {
            verificationCode?: number;
        },
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/{id}/verifySMS',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Update User Location
     * Update a user's location for use with Home Discovery
     * @param id
     * @param requestBody
     * @returns User Successful
     * @throws ApiError
     */
    public static updateUserLocation(
        id: number,
        requestBody: {
            locationVerified?: boolean;
            location?: Location;
            searchRadius?: number;
            searchCity?: string;
        },
    ): CancelablePromise<User> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/{id}/updateUserLocation',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Send Reset Password Request
     * Sends a message to provided email to aid in password reset
     * @param requestBody
     * @returns any send reset password link in email to user
     * @throws ApiError
     */
    public static resetPasswordStepOne(
        requestBody: {
            email?: string;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/resetPasswordStepOne',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Confirm Reset Password
     * Send user provided newPassword along with previously requested token to finish resetting password
     * @param requestBody
     * @returns any Reset password
     * @throws ApiError
     */
    public static resetPasswordStepTwo(
        requestBody: {
            resetToken?: string;
            newPassword?: string;
            confirmNewPassword?: string;
        },
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User/resetPasswordStepTwo',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Users
     * Find users according to defined filter criteria
     * @param filter
     * @returns User Array of Users
     * @throws ApiError
     */
    public static getUsers(
        filter?: string,
    ): CancelablePromise<Array<User>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/User',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Users
     * Post an array of User objects
     * @param requestBody
     * @returns User created User
     * @throws ApiError
     */
    public static postUsers(
        requestBody: User,
    ): CancelablePromise<User> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/User',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get User By Id
     * Find specific User with unique id
     * @param id
     * @param filter
     * @returns User single User
     * @throws ApiError
     */
    public static getUserById(
        id: number,
        filter?: string,
    ): CancelablePromise<User> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/User/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch User By Id
     * Patch specific User with unique id
     * @param id
     * @param requestBody
     * @returns User patched User
     * @throws ApiError
     */
    public static patchUserById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<User> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/User/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

}
