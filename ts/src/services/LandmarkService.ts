/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Landmark } from '../models/Landmark';
import type { PatchBody } from '../models/PatchBody';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class LandmarkService {

    /**
     * Get Landmark By Id
     * Get specific Landmark with unique Id
     * @param id
     * @param filter
     * @returns Landmark Successful
     * @throws ApiError
     */
    public static getLandmarkById(
        id: number,
        filter?: string,
    ): CancelablePromise<Landmark> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Landmark/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Landmark By Id
     * Patch specific Landmark with unique Id
     * @param id
     * @param requestBody
     * @returns Landmark patched Landmark
     * @throws ApiError
     */
    public static patchLandmarkById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Landmark> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Landmark/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Landmark By Id
     * Delete specific Landmark with unique Id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteLandmarkById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Landmark/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Insert landmark by edit key
     * @param requestBody
     * @returns Landmark The inserted landmark
     * @throws ApiError
     */
    public static insertByEditKey(
        requestBody: {
            landmark?: Landmark;
            editKey?: string;
            envisionAssetId?: number;
        },
    ): CancelablePromise<Landmark> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Landmark/insertByEditKey',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Delete landmark by edit key
     * @param id
     * @param editKey
     * @returns void
     * @throws ApiError
     */
    public static deleteByEditKey(
        id: number,
        editKey?: string,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Landmark/{id}/deleteByEditKey',
            path: {
                'id': id,
            },
            query: {
                'editKey': editKey,
            },
        });
    }

    /**
     * Edit landmark by edit key
     * @param id
     * @param requestBody
     * @returns Landmark The edited landmark
     * @throws ApiError
     */
    public static editByEditKey(
        id: number,
        requestBody: {
            landmark?: Landmark;
            editKey?: string;
        },
    ): CancelablePromise<Landmark> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Landmark/{id}/editByEditKey',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
