/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BoundingBox } from '../models/BoundingBox';
import type { Media } from '../models/Media';
import type { PatchBody } from '../models/PatchBody';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class MediaService {

    /**
     * Get Media
     * Get an array of Media according to defined filter criteria
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    public static getMedia(
        filter?: string,
    ): CancelablePromise<Array<Media>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Media',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Post Media
     * Post an array of Media objects
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    public static postMedia(
        requestBody: Media,
    ): CancelablePromise<Media> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Media',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Find One Media
     * Fine one Media according to defined filter criteria
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    public static findOneMedia(
        filter: string,
    ): CancelablePromise<Media> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Media/findOne',
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get Media by Id
     * Get a specific Media with unique id
     * @param id
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    public static getMediaById(
        id: number,
        filter?: string,
    ): CancelablePromise<Media> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Media/{id}',
            path: {
                'id': id,
            },
            headers: {
                'filter': filter,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Patch Media by Id
     * Patch specific Media with unique id
     * @param id
     * @param requestBody
     * @returns Media patched Media
     * @throws ApiError
     */
    public static patchMediaById(
        id: number,
        requestBody?: PatchBody,
    ): CancelablePromise<Media> {
        return __request(OpenAPI, {
            method: 'PATCH',
            url: '/Media/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Delete Media by Id
     * Delete specific Media with unique id
     * @param id
     * @returns void
     * @throws ApiError
     */
    public static deleteMediaById(
        id: number,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/Media/{id}',
            path: {
                'id': id,
            },
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Run Insight on an array of images as files or urls
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    public static classify(
        requestBody: {
            file?: string;
            url?: string;
            force?: boolean;
            objects?: boolean;
        },
    ): CancelablePromise<{
        classifications?: Array<{
            name?: string;
            confidence?: number;
            rank?: number;
        }>;
        detections?: Array<{
            mask?: string;
            area?: number;
            boundingBox?: BoundingBox;
            confidence?: number;
            attributes?: {
                'is-stainless'?: boolean;
            };
        }>;
    }> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Media/classify',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                403: `Forbidden`,
            },
        });
    }

    /**
     * Run Insight given a properly formatted RESO Property with nested Media
     * @returns any Successful
     * @throws ApiError
     */
    public static classifyReso(): CancelablePromise<{
        value?: Array<{
            Media?: Array<{
                MediaCategory?: string;
                MediaURL?: string;
                MediaKey?: string;
                MediaModificationTimestamp?: string;
                MediaOf?: string;
            }>;
        }>;
    }> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Media/classifyReso',
            errors: {
                403: `Forbidden`,
            },
        });
    }

    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    public static reportClassificationError(
        requestBody?: {
            labels?: Array<string>;
            hash?: string;
        },
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Media/reportClassificationError',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                401: `Unauthorized`,
            },
        });
    }

    /**
     * Get a presigned url for uploading media through the use of the property edit key
     * @param editKey
     * @param mimeType
     * @param fileExtension
     * @returns string The presigned url that can be used to upload media
     * @throws ApiError
     */
    public static getMediaUploadUrlByEditKey(
        editKey?: string,
        mimeType?: string,
        fileExtension?: string,
    ): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/Media/mediaUploadUrlByEditKey',
            query: {
                'editKey': editKey,
                'mimeType': mimeType,
                'fileExtension': fileExtension,
            },
        });
    }

    /**
     * Insert media by the property edit key
     * @param requestBody
     * @returns Media The inserted media
     * @throws ApiError
     */
    public static insertMediaByEditKey(
        requestBody: {
            media?: Array<Media>;
            editKey?: string;
            landmarkId?: number;
        },
    ): CancelablePromise<Array<Media>> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/Media/insertMediaByEditKey',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
