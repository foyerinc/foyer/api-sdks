# Foyer API

[![npm](https://img.shields.io/npm/v/@foyer-inc/api-sdk.svg)](https://www.npmjs.com/package/@foyer-inc/api-sdk) [![MIT License](https://img.shields.io/npm/l/@foyer-inc/api-sdk.svg)](https://gitlab.com/foyerinc/foyer/api-sdks)

Additional documentation at [Docs](https://foyerinc.gitlab.io/foyer/api-sdks)

Additional details on filter structure available at [Sequelize](https://sequelize.org/docs/v6/core-concepts/model-querying-basics)

### Listed below is example usage of the Foyer api-sdk

```js
import { OpenAPI, PropertyService } from '@foyer-inc/api-sdk';

//usually will not need to change this but it is available for redirecting requests away from the main production api
OpenAPI.BASE = 'remote api endpoint';

//use auth token from Okta or from UserService.login
//needed to access protected endpoints
//set statically and globally
OpenAPI.TOKEN = 'token';

//use await/async  or Promise callback
const properties = await PropertyService.getProperties();

PropertyService.getProperties().then((value) => console.log(value));

//Typically used with a filter to specify which Properties to return
//The filter is also used to indicate which related models should also be returned

const filter = {
  // For more granularity, all fields on Property are able to be included as search criteria.
  where: {
    BedroomsTotal: 2
    BathroomsTotalInteger: {
      gt: 2
    }
  },
  //an array of column names to return. helpful to limit returning unnecessary information
  attributes: ["ListingKey", "StreetNumber"],
  //an array of associated models to return,
  //elements in this array can also be narrowed by including a similarly structured filter
  include: [
    { association: "Media" }
  ],
  // an integer for how many entries to return
  limit: 10,
  // an integer for how many entries to skip, useful for pagination
  offset: 10,
};

const properties = await PropertyService.getProperties(JSON.stringify(filter));
//example usage with verbosity param instead of filter object
PropertyService.getProperties(undefined, 'card').then((value) =>
  console.log(value)
);
```

RESO
Available Foyer models comply with RESO Spec

See [RESO Data Dictionary](https://ddwiki.reso.org/display/DDW17/RESO+Data+Dictionary+Wiki+1.7) for a comprehensive look at RESO

See [Reference Worksheet](https://docs.google.com/spreadsheets/d/1SZ0b6T4_lz6ti6qB2Je7NSz_9iNOaV_v9dbfhPwWgXA/edit?usp=sharing) for Standard Relationships
