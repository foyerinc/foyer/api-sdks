# api-sdks

## TS

Uses: https://github.com/ferdikoomen/openapi-typescript-codegen

`npx openapi-typescript-codegen --input openapi.yaml --output ./ts`

## JS

Run the TS generator, then run `tsc --build`.

## C#

Uses: https://github.com/Mermade/openapi-codegen

`npx -p openapi-codegen cg og:client:csharp -o cs openapi.yaml`

## Python

`npx -p openapi-codegen cg og:client:python -o python openapi.yaml`
