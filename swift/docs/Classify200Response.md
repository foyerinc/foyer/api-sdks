# Classify200Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**classifications** | [Classify200ResponseClassificationsInner] |  | [optional] 
**detections** | [Classify200ResponseDetectionsInner] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


