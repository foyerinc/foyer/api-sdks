# ErrorFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**userId** | **Int** |  | [optional] 
**url** | **String** |  | [optional] 
**error** | [String: AnyCodable] |  | [optional] 
**message** | **String** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


