# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**realm** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**username** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**phoneVerified** | **Bool** |  | [optional] 
**smsVerificationCode** | **Int** |  | [optional] 
**profileImage** | **String** |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**newUser** | **Bool** |  | [optional] 
**emailVerified** | **Bool** |  | [optional] 
**verificationToken** | **String** |  | [optional] 
**parentId** | **Int** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**member** | [**Member**](Member.md) |  | [optional] 
**publisher** | [**Publisher**](Publisher.md) |  | [optional] 
**shopper** | [**Shopper**](Shopper.md) |  | [optional] 
**usages** | [ApiUsage] |  | [optional] 
**favorites** | [Favorite] |  | [optional] 
**shareReceivers** | [Share] |  | [optional] 
**shareSenders** | [Share] |  | [optional] 
**transactions** | [Transaction] |  | [optional] 
**trials** | [Trial] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


