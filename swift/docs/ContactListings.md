# ContactListings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**agentNotesUnreadYN** | **Bool** |  | [optional] 
**contactNotesUnreadYN** | **Bool** |  | [optional] 
**directEmailYN** | **Bool** |  | [optional] 
**listingViewedYN** | **Bool** |  | [optional] 
**contactKeyNumeric** | **Double** |  | [optional] 
**contactListingsKeyNumeric** | **Double** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**contactKey** | **String** |  | [optional] 
**contactListingsKey** | **String** |  | [optional] 
**contactLoginId** | **String** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**className** | **String** |  | [optional] 
**contactListingPreference** | **String** |  | [optional] 
**resourceName** | **String** |  | [optional] 
**lastAgentNoteTimestamp** | **Date** |  | [optional] 
**lastContactNoteTimestamp** | **Date** |  | [optional] 
**listingModificationTimestamp** | **Date** |  | [optional] 
**listingSentTimestamp** | **Date** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**portalLastVisitedTimestamp** | **Date** |  | [optional] 
**contact** | [**Contacts**](Contacts.md) |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


