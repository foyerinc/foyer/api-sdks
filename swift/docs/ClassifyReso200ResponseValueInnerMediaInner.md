# ClassifyReso200ResponseValueInnerMediaInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediaCategory** | **String** |  | [optional] 
**mediaURL** | **String** |  | [optional] 
**mediaKey** | **String** |  | [optional] 
**mediaModificationTimestamp** | **Date** |  | [optional] 
**mediaOf** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


