# ErrorsCalculatingFoyerScore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reportHasRequiredInformation** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


