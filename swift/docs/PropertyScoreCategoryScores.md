# PropertyScoreCategoryScores

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bathroom** | [**Score**](Score.md) |  | [optional] 
**bedroom** | [**Score**](Score.md) |  | [optional] 
**diningRoom** | [**Score**](Score.md) |  | [optional] 
**kitchen** | [**Score**](Score.md) |  | [optional] 
**livingRoom** | [**Score**](Score.md) |  | [optional] 
**outdoor** | [**Score**](Score.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


