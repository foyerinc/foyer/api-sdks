# Rules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**ruleEnabledYN** | **Bool** |  | [optional] 
**fieldKeyNumeric** | **Double** |  | [optional] 
**ruleKeyNumeric** | **Double** |  | [optional] 
**ruleOrder** | **Double** |  | [optional] 
**fieldKey** | **String** |  | [optional] 
**fieldName** | **String** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**originatingSystemRuleKey** | **String** |  | [optional] 
**ruleAction** | **String** |  | [optional] 
**ruleDescription** | **String** |  | [optional] 
**ruleErrorText** | **String** |  | [optional] 
**ruleExpression** | **String** |  | [optional] 
**ruleHelpText** | **String** |  | [optional] 
**ruleKey** | **String** |  | [optional] 
**ruleName** | **String** |  | [optional] 
**ruleVersion** | **String** |  | [optional] 
**ruleWarningText** | **String** |  | [optional] 
**sourceSystemHistoryKey** | **String** |  | [optional] 
**sourceSystemID** | **String** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**className** | **String** |  | [optional] 
**resourceName** | **String** |  | [optional] 
**ruleFormat** | **String** |  | [optional] 
**ruleType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**originalEntryTimestamp** | **Date** |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


