# Shopper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**userId** | **Int** |  | [optional] 
**memberId** | **Int** |  | [optional] 
**username** | **String** |  | [optional] 
**price** | **Double** |  | [optional] 
**priceVerified** | **Bool** |  | [optional] 
**locationVerified** | **Bool** |  | [optional] 
**tutorialStep** | **Int** |  | [optional] 
**searchRadius** | **Int** |  | [optional] 
**searchCity** | **String** |  | [optional] 
**filterLocation** | **Bool** |  | [optional] 
**searchHistory** | **String** |  | [optional] 
**imageFilter** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**member** | [**Member**](Member.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


