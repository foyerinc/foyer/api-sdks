# Contacts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**anniversary** | **Date** |  | [optional] 
**birthdate** | **Date** |  | [optional] 
**contactKeyNumeric** | **Double** |  | [optional] 
**ownerMemberKeyNumeric** | **Double** |  | [optional] 
**assistantEmail** | **String** |  | [optional] 
**assistantName** | **String** |  | [optional] 
**assistantPhone** | **String** |  | [optional] 
**assistantPhoneExt** | **String** |  | [optional] 
**businessFax** | **String** |  | [optional] 
**children** | **String** |  | [optional] 
**company** | **String** |  | [optional] 
**contactKey** | **String** |  | [optional] 
**contactLoginId** | **String** |  | [optional] 
**contactPassword** | **String** |  | [optional] 
**department** | **String** |  | [optional] 
**directPhone** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**email2** | **String** |  | [optional] 
**email3** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**fullName** | **String** |  | [optional] 
**homeAddress1** | **String** |  | [optional] 
**homeAddress2** | **String** |  | [optional] 
**homeCarrierRoute** | **String** |  | [optional] 
**homeCity** | **String** |  | [optional] 
**homeFax** | **String** |  | [optional] 
**homePhone** | **String** |  | [optional] 
**homePostalCode** | **String** |  | [optional] 
**homePostalCodePlus4** | **String** |  | [optional] 
**jobTitle** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**leadSource** | **String** |  | [optional] 
**middleName** | **String** |  | [optional] 
**mobilePhone** | **String** |  | [optional] 
**namePrefix** | **String** |  | [optional] 
**nameSuffix** | **String** |  | [optional] 
**nickname** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**officePhone** | **String** |  | [optional] 
**officePhoneExt** | **String** |  | [optional] 
**originatingSystemContactKey** | **String** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**otherAddress1** | **String** |  | [optional] 
**otherAddress2** | **String** |  | [optional] 
**otherCarrierRoute** | **String** |  | [optional] 
**otherCity** | **String** |  | [optional] 
**otherPostalCode** | **String** |  | [optional] 
**otherPostalCodePlus4** | **String** |  | [optional] 
**ownerMemberID** | **String** |  | [optional] 
**ownerMemberKey** | **String** |  | [optional] 
**pager** | **String** |  | [optional] 
**phoneTTYTDD** | **String** |  | [optional] 
**referredBy** | **String** |  | [optional] 
**sourceSystemContactKey** | **String** |  | [optional] 
**sourceSystemID** | **Double** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**spousePartnerName** | **String** |  | [optional] 
**tollFreePhone** | **String** |  | [optional] 
**voiceMail** | **String** |  | [optional] 
**voiceMailExt** | **String** |  | [optional] 
**workAddress1** | **String** |  | [optional] 
**workAddress2** | **String** |  | [optional] 
**workCarrierRoute** | **String** |  | [optional] 
**workCity** | **String** |  | [optional] 
**workPostalCode** | **String** |  | [optional] 
**workPostalCodePlus4** | **String** |  | [optional] 
**contactType** | **[String]** |  | [optional] 
**language** | **[String]** |  | [optional] 
**contactStatus** | **String** |  | [optional] 
**homeCountry** | **String** |  | [optional] 
**homeCountyOrParish** | **String** |  | [optional] 
**homeStateOrProvince** | **String** |  | [optional] 
**otherCountry** | **String** |  | [optional] 
**otherCountyOrParish** | **String** |  | [optional] 
**otherStateOrProvince** | **String** |  | [optional] 
**otherPhoneType** | **String** |  | [optional] 
**preferredAddress** | **String** |  | [optional] 
**preferredPhone** | **String** |  | [optional] 
**workCountry** | **String** |  | [optional] 
**workCountyOrParish** | **String** |  | [optional] 
**workStateOrProvince** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**originalEntryTimestamp** | **Date** |  | [optional] 
**ownerMember** | [**Member**](Member.md) |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 
**contactListingNotes** | [ContactListingNotes] |  | [optional] 
**contactListings** | [ContactListings] |  | [optional] 
**prospectings** | [Prospecting] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


