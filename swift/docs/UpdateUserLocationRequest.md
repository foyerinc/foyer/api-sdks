# UpdateUserLocationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locationVerified** | **Bool** |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**searchRadius** | **Int** |  | [optional] 
**searchCity** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


