# PropertyScoreInferenceScores

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all** | [**PropertyScoreInferenceScoresAll**](PropertyScoreInferenceScoresAll.md) |  | [optional] 
**bathroom** | [**PropertyScoreInferenceScoresBathroom**](PropertyScoreInferenceScoresBathroom.md) |  | [optional] 
**bedroom** | [**PropertyScoreInferenceScoresBedroom**](PropertyScoreInferenceScoresBedroom.md) |  | [optional] 
**diningRoom** | [**PropertyScoreInferenceScoresBedroom**](PropertyScoreInferenceScoresBedroom.md) |  | [optional] 
**kitchen** | [**PropertyScoreInferenceScoresKitchen**](PropertyScoreInferenceScoresKitchen.md) |  | [optional] 
**livingRoom** | [**PropertyScoreInferenceScoresKitchen**](PropertyScoreInferenceScoresKitchen.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


