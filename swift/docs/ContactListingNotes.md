# ContactListingNotes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**contactKeyNumeric** | **Double** |  | [optional] 
**contactListingNotesKeyNumeric** | **Double** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**contactKey** | **String** |  | [optional] 
**contactListingNotesKey** | **String** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**noteContents** | **String** |  | [optional] 
**notedBy** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**contact** | [**Contacts**](Contacts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


