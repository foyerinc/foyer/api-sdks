# Landmark

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**envisionAssetId** | **Double** |  | [optional] 
**_class** | **String** |  | [optional] 
**confidence** | **Double** |  | [optional] 
**position** | **[Double]** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


