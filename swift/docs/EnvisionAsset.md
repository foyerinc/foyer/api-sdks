# EnvisionAsset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**aboundId** | **String** |  | [optional] 
**thumbnail** | **String** |  | [optional] 
**topDown** | **String** |  | [optional] 
**glb** | **String** |  | [optional] 
**obj** | **String** |  | [optional] 
**dump** | **String** |  | [optional] 
**tags** | [**AnyCodable**](.md) |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 
**landmarks** | [Landmark] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


