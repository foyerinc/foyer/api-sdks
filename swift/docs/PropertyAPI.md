# PropertyAPI

All URIs are relative to *https://api.foyer.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calculateLocalGraph**](PropertyAPI.md#calculatelocalgraph) | **GET** /Property/{id}/calculateLocalGraph | Compare property by id to local area
[**calculateScore**](PropertyAPI.md#calculatescore) | **GET** /Property/{id}/calculateScore | Calculate Foyer Score for Property
[**findNearestProperties**](PropertyAPI.md#findnearestproperties) | **GET** /Property/findNearest | Find Nearest Properties
[**findOneProperty**](PropertyAPI.md#findoneproperty) | **GET** /Property/findOne | Find One Property
[**getProperties**](PropertyAPI.md#getproperties) | **GET** /Property | Get Properties
[**getPropertyById**](PropertyAPI.md#getpropertybyid) | **GET** /Property/{id} | Get Property by Id
[**getPropertyByViewerKey**](PropertyAPI.md#getpropertybyviewerkey) | **GET** /Property/propertyByViewerKey | Gets a property by a viewer key
[**getRecommendations**](PropertyAPI.md#getrecommendations) | **GET** /Property/getRecommendations | Get Recommendations
[**propertySearch**](PropertyAPI.md#propertysearch) | **GET** /Property/search | Property Search


# **calculateLocalGraph**
```swift
    open class func calculateLocalGraph(id: Int, filter: String? = nil, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Compare property by id to local area

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let filter = "filter_example" // String |  (optional)

// Compare property by id to local area
PropertyAPI.calculateLocalGraph(id: id, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **filter** | **String** |  | [optional] 

### Return type

Void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **calculateScore**
```swift
    open class func calculateScore(id: Int, filter: String? = nil, completion: @escaping (_ data: CalculateScore200Response?, _ error: Error?) -> Void)
```

Calculate Foyer Score for Property

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let filter = "filter_example" // String |  (optional)

// Calculate Foyer Score for Property
PropertyAPI.calculateScore(id: id, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **filter** | **String** |  | [optional] 

### Return type

[**CalculateScore200Response**](CalculateScore200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findNearestProperties**
```swift
    open class func findNearestProperties(streetNumber: String, street: String, streetType: String, cityName: String, state: String, lat: Double, lng: Double, completion: @escaping (_ data: FindNearestProperties200Response?, _ error: Error?) -> Void)
```

Find Nearest Properties

Provide an address to find the geographically closest Properties

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let streetNumber = "streetNumber_example" // String | 
let street = "street_example" // String | 
let streetType = "streetType_example" // String | 
let cityName = "cityName_example" // String | 
let state = "state_example" // String | 
let lat = 987 // Double | 
let lng = 987 // Double | 

// Find Nearest Properties
PropertyAPI.findNearestProperties(streetNumber: streetNumber, street: street, streetType: streetType, cityName: cityName, state: state, lat: lat, lng: lng) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **streetNumber** | **String** |  | 
 **street** | **String** |  | 
 **streetType** | **String** |  | 
 **cityName** | **String** |  | 
 **state** | **String** |  | 
 **lat** | **Double** |  | 
 **lng** | **Double** |  | 

### Return type

[**FindNearestProperties200Response**](FindNearestProperties200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findOneProperty**
```swift
    open class func findOneProperty(filter: String, verbosity: String? = nil, completion: @escaping (_ data: Property?, _ error: Error?) -> Void)
```

Find One Property

Find one Property according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String | 
let verbosity = "verbosity_example" // String |  (optional)

// Find One Property
PropertyAPI.findOneProperty(filter: filter, verbosity: verbosity) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** |  | 
 **verbosity** | **String** |  | [optional] 

### Return type

[**Property**](Property.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getProperties**
```swift
    open class func getProperties(filter: String? = nil, verbosity: String? = nil, completion: @escaping (_ data: GetProperties200Response?, _ error: Error?) -> Void)
```

Get Properties

Get an object with an array of properties and a total count according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String |  (optional)
let verbosity = "verbosity_example" // String |  (optional)

// Get Properties
PropertyAPI.getProperties(filter: filter, verbosity: verbosity) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** |  | [optional] 
 **verbosity** | **String** |  | [optional] 

### Return type

[**GetProperties200Response**](GetProperties200Response.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPropertyById**
```swift
    open class func getPropertyById(id: Int, filter: String? = nil, verbosity: String? = nil, completion: @escaping (_ data: Property?, _ error: Error?) -> Void)
```

Get Property by Id

Get specific Property with unique id. Also allows for verbosity parameter to shortcut commonly used filters

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let filter = "filter_example" // String |  (optional)
let verbosity = "verbosity_example" // String |  (optional)

// Get Property by Id
PropertyAPI.getPropertyById(id: id, filter: filter, verbosity: verbosity) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **filter** | **String** |  | [optional] 
 **verbosity** | **String** |  | [optional] 

### Return type

[**Property**](Property.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPropertyByViewerKey**
```swift
    open class func getPropertyByViewerKey(viewerKey: String? = nil, completion: @escaping (_ data: Property?, _ error: Error?) -> Void)
```

Gets a property by a viewer key

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let viewerKey = "viewerKey_example" // String |  (optional)

// Gets a property by a viewer key
PropertyAPI.getPropertyByViewerKey(viewerKey: viewerKey) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **viewerKey** | **String** |  | [optional] 

### Return type

[**Property**](Property.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getRecommendations**
```swift
    open class func getRecommendations(limit: Int? = nil, bufLen: Int? = nil, completion: @escaping (_ data: [Property]?, _ error: Error?) -> Void)
```

Get Recommendations

Return an array of Properties according to preferences set on User and Shopper Models

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let limit = 987 // Int |  (optional)
let bufLen = 987 // Int |  (optional)

// Get Recommendations
PropertyAPI.getRecommendations(limit: limit, bufLen: bufLen) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Int** |  | [optional] 
 **bufLen** | **Int** |  | [optional] 

### Return type

[**[Property]**](Property.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **propertySearch**
```swift
    open class func propertySearch(verbosity: String? = nil, completion: @escaping (_ data: [Property]?, _ error: Error?) -> Void)
```

Property Search

Search for properties according to defined criteria and desired image features

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let verbosity = "verbosity_example" // String |  (optional)

// Property Search
PropertyAPI.propertySearch(verbosity: verbosity) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verbosity** | **String** |  | [optional] 

### Return type

[**[Property]**](Property.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

