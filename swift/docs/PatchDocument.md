# PatchDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**op** | **String** | The operation to be performed | 
**path** | **String** | A JSON-Pointer | 
**value** | **String** | The value to be used within the operations. | [optional] 
**from** | **String** | A string containing a JSON Pointer value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


