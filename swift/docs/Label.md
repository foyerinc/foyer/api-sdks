# Label

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**mediaId** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**classId** | **Int** |  | [optional] 
**confidence** | **Float** |  | [optional] 
**rank** | **Int** |  | [optional] 
**source** | **String** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**media** | [**Media**](Media.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


