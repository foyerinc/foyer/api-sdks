# Classify200ResponseDetectionsInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mask** | **String** |  | [optional] 
**area** | **Int** |  | [optional] 
**boundingBox** | **[Int]** |  | [optional] 
**confidence** | **Float** |  | [optional] 
**attributes** | [**Classify200ResponseDetectionsInnerAttributes**](Classify200ResponseDetectionsInnerAttributes.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


