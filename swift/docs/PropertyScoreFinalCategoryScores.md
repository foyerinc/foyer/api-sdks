# PropertyScoreFinalCategoryScores

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bathroom** | **Double** |  | [optional] 
**bedroom** | **Double** |  | [optional] 
**diningRoom** | **Double** |  | [optional] 
**kitchen** | **Double** |  | [optional] 
**livingRoom** | **Double** |  | [optional] 
**outdoor** | **Double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


