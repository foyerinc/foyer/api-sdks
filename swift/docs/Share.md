# Share

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**seen** | **Bool** |  | [optional] 
**receiverId** | **Int** |  | [optional] 
**senderId** | **Int** |  | [optional] 
**propertyId** | **Int** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**receiver** | [**User**](User.md) |  | [optional] 
**sender** | [**User**](User.md) |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


