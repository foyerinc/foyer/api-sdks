# ClassifyRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**force** | **Bool** |  | [optional] 
**objects** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


