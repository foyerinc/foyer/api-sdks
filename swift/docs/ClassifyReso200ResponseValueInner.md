# ClassifyReso200ResponseValueInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media** | [ClassifyReso200ResponseValueInnerMediaInner] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


