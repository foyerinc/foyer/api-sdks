# PropertyUnitTypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**unitTypeGarageAttachedYN** | **Bool** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**unitTypeActualRent** | **Double** |  | [optional] 
**unitTypeBathsTotal** | **Double** |  | [optional] 
**unitTypeBedsTotal** | **Double** |  | [optional] 
**unitTypeGarageSpaces** | **Double** |  | [optional] 
**unitTypeKeyNumeric** | **Double** |  | [optional] 
**unitTypeProForma** | **Double** |  | [optional] 
**unitTypeTotalRent** | **Double** |  | [optional] 
**unitTypeUnitsTotal** | **Double** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**unitTypeDescription** | **String** |  | [optional] 
**unitTypeKey** | **String** |  | [optional] 
**unitTypeType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


