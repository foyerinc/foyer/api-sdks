# MemberAPI

All URIs are relative to *https://api.foyer.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteMemberById**](MemberAPI.md#deletememberbyid) | **DELETE** /Member/{id} | Delete Member by Id
[**findOneMember**](MemberAPI.md#findonemember) | **GET** /Member/findOne | Find One Member
[**getMemberById**](MemberAPI.md#getmemberbyid) | **GET** /Member/{id} | Get Member by Id
[**patchMemberById**](MemberAPI.md#patchmemberbyid) | **PATCH** /Member/{id} | Patch Member by Id


# **deleteMemberById**
```swift
    open class func deleteMemberById(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Member by Id

Delete specific Member with unique Id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 

// Delete Member by Id
MemberAPI.deleteMemberById(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 

### Return type

Void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findOneMember**
```swift
    open class func findOneMember(filter: String, completion: @escaping (_ data: Member?, _ error: Error?) -> Void)
```

Find One Member

Fine one Member according to defined filter criteria

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String | 

// Find One Member
MemberAPI.findOneMember(filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** |  | 

### Return type

[**Member**](Member.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getMemberById**
```swift
    open class func getMemberById(id: Int, filter: String? = nil, completion: @escaping (_ data: Member?, _ error: Error?) -> Void)
```

Get Member by Id

Get specific Member with unique Id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let filter = "filter_example" // String |  (optional)

// Get Member by Id
MemberAPI.getMemberById(id: id, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **filter** | **String** |  | [optional] 

### Return type

[**Member**](Member.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patchMemberById**
```swift
    open class func patchMemberById(id: Int, patchDocument: [PatchDocument]? = nil, completion: @escaping (_ data: Member?, _ error: Error?) -> Void)
```

Patch Member by Id

Patch specific Member with unique Id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let patchDocument = [PatchDocument(op: "op_example", path: "path_example", value: "value_example", from: "from_example")] // [PatchDocument] |  (optional)

// Patch Member by Id
MemberAPI.patchMemberById(id: id, patchDocument: patchDocument) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **patchDocument** | [**[PatchDocument]**](PatchDocument.md) |  | [optional] 

### Return type

[**Member**](Member.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

