# Queue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**queueTransactionKeyNumeric** | **Double** |  | [optional] 
**resourceRecordKeyNumeric** | **Double** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**originatingSystemQueueKey** | **String** |  | [optional] 
**queueTransactionKey** | **String** |  | [optional] 
**resourceRecordID** | **String** |  | [optional] 
**resourceRecordKey** | **String** |  | [optional] 
**sourceSystemID** | **String** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**sourceSystemQueueKey** | **String** |  | [optional] 
**className** | **String** |  | [optional] 
**queueTransactionType** | **String** |  | [optional] 
**resourceName** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


