# SavedSearch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**memberKeyNumeric** | **Double** |  | [optional] 
**savedSearchKeyNumeric** | **Double** |  | [optional] 
**memberKey** | **String** |  | [optional] 
**memberMlsId** | **String** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemKey** | **String** |  | [optional] 
**originatingSystemMemberKey** | **String** |  | [optional] 
**originatingSystemMemberName** | **String** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**savedSearchDescription** | **String** |  | [optional] 
**savedSearchKey** | **String** |  | [optional] 
**savedSearchName** | **String** |  | [optional] 
**searchQuery** | **String** |  | [optional] 
**searchQueryExceptionDetails** | **String** |  | [optional] 
**searchQueryHumanReadable** | **String** |  | [optional] 
**sourceSystemID** | **Double** |  | [optional] 
**sourceSystemKey** | **String** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**className** | **String** |  | [optional] 
**resourceName** | **String** |  | [optional] 
**savedSearchType** | **String** |  | [optional] 
**searchQueryExceptions** | **String** |  | [optional] 
**searchQueryType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**originalEntryTimestamp** | **Date** |  | [optional] 
**member** | [**Member**](Member.md) |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 
**prospectings** | [Prospecting] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


