# Publisher

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**userId** | **Int** |  | [optional] 
**name** | **String** |  | [optional] 
**website** | **String** |  | [optional] 
**logo** | **String** |  | [optional] 
**active** | **Bool** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**syndications** | [Syndication] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


