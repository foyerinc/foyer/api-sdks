# PropertyScoreCategoryPhotosPresent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bathroom** | **Bool** |  | [optional] 
**bedroom** | **Bool** |  | [optional] 
**diningRoom** | **Bool** |  | [optional] 
**kitchen** | **Bool** |  | [optional] 
**livingRoom** | **Bool** |  | [optional] 
**outdoor** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


