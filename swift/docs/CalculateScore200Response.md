# CalculateScore200Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryScores** | [**PropertyScoreCategoryScores**](PropertyScoreCategoryScores.md) |  | [optional] 
**inferenceScores** | [**PropertyScoreInferenceScores**](PropertyScoreInferenceScores.md) |  | [optional] 
**mediaCountScores** | [**PropertyScoreMediaCountScores**](PropertyScoreMediaCountScores.md) |  | [optional] 
**categoryPhotosPresent** | [**PropertyScoreCategoryPhotosPresent**](PropertyScoreCategoryPhotosPresent.md) |  | [optional] 
**finalCategoryScores** | [**PropertyScoreFinalCategoryScores**](PropertyScoreFinalCategoryScores.md) |  | [optional] 
**bonusScore** | [**Score**](Score.md) |  | [optional] 
**finalScore** | **Double** |  | [optional] 
**reportHasRequiredInformation** | **Bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


