# PropertyRooms

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**roomArea** | **Double** |  | [optional] 
**roomKeyNumeric** | **Double** |  | [optional] 
**roomLength** | **Double** |  | [optional] 
**roomWidth** | **Double** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**roomDescription** | **String** |  | [optional] 
**roomDimensions** | **String** |  | [optional] 
**roomKey** | **String** |  | [optional] 
**roomFeatures** | **[String]** |  | [optional] 
**roomAreaSource** | **String** |  | [optional] 
**roomAreaUnits** | **String** |  | [optional] 
**roomLengthWidthUnits** | **String** |  | [optional] 
**roomType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


