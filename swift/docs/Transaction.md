# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**userId** | **Int** |  | [optional] 
**propertyId** | **Int** |  | [optional] 
**type** | **String** |  | [optional] 
**detailsViewed** | **Bool** |  | [optional] 
**imagesViewed** | **Int** |  | [optional] 
**timeToDecision** | **Int** |  | [optional] 
**filter** | **String** |  | [optional] 
**session** | **String** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


