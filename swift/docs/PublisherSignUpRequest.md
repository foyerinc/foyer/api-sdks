# PublisherSignUpRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**confirmedPassword** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**website** | **String** |  | [optional] 
**logo** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


