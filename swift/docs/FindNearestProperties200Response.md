# FindNearestProperties200Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bestChoice** | [**Property**](Property.md) |  | [optional] 
**neighbors** | [Property] |  | [optional] 
**properties** | [Property] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


