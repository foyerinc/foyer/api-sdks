# PropertyPowerProduction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**powerProductionAnnual** | **Double** |  | [optional] 
**powerProductionKeyNumeric** | **Double** |  | [optional] 
**powerProductionSize** | **Double** |  | [optional] 
**powerProductionYearInstall** | **Double** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**powerProductionKey** | **String** |  | [optional] 
**powerProductionAnnualStatus** | **String** |  | [optional] 
**powerProductionType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


