# Prospecting

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**activeYN** | **Bool** |  | [optional] 
**bccMeYN** | **Bool** |  | [optional] 
**clientActivatedYN** | **Bool** |  | [optional] 
**conciergeNotificationsYN** | **Bool** |  | [optional] 
**conciergeYN** | **Bool** |  | [optional] 
**contactKeyNumeric** | **Double** |  | [optional] 
**ownerMemberKeyNumeric** | **Double** |  | [optional] 
**prospectingKeyNumeric** | **Double** |  | [optional] 
**savedSearchKeyNumeric** | **Double** |  | [optional] 
**bccEmailList** | **String** |  | [optional] 
**ccEmailList** | **String** |  | [optional] 
**contactKey** | **String** |  | [optional] 
**displayTemplateID** | **String** |  | [optional] 
**messageNew** | **String** |  | [optional] 
**messageRevise** | **String** |  | [optional] 
**messageUpdate** | **String** |  | [optional] 
**ownerMemberID** | **String** |  | [optional] 
**ownerMemberKey** | **String** |  | [optional] 
**prospectingKey** | **String** |  | [optional] 
**savedSearchKey** | **String** |  | [optional] 
**subject** | **String** |  | [optional] 
**toEmailList** | **String** |  | [optional] 
**dailySchedule** | **[String]** |  | [optional] 
**languages** | **String** |  | [optional] 
**reasonActiveOrDisabled** | **String** |  | [optional] 
**scheduleType** | **String** |  | [optional] 
**lastNewChangedTimestamp** | **Date** |  | [optional] 
**lastViewedTimestamp** | **Date** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**nextSendTimestamp** | **Date** |  | [optional] 
**contact** | [**Contacts**](Contacts.md) |  | [optional] 
**ownerMember** | [**Member**](Member.md) |  | [optional] 
**savedSearch** | [**SavedSearch**](SavedSearch.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


