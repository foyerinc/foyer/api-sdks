# PropertyGreenVerification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**greenBuildingVerificationKeyNumeric** | **Double** |  | [optional] 
**greenVerificationMetric** | **Double** |  | [optional] 
**greenVerificationYear** | **Double** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**greenBuildingVerificationKey** | **String** |  | [optional] 
**greenVerificationBody** | **String** |  | [optional] 
**greenVerificationRating** | **String** |  | [optional] 
**greenVerificationURL** | **String** |  | [optional] 
**greenVerificationVersion** | **String** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**greenBuildingVerificationType** | **String** |  | [optional] 
**greenVerificationSource** | **String** |  | [optional] 
**greenVerificationStatus** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


