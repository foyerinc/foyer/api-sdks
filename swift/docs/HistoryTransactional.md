# HistoryTransactional

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**changedByMemberKeyNumeric** | **Double** |  | [optional] 
**fieldKeyNumeric** | **Double** |  | [optional] 
**historyTransactionalKeyNumeric** | **Double** |  | [optional] 
**resourceRecordKeyNumeric** | **Double** |  | [optional] 
**changedByMemberID** | **String** |  | [optional] 
**changedByMemberKey** | **String** |  | [optional] 
**className** | **String** |  | [optional] 
**fieldKey** | **String** |  | [optional] 
**fieldName** | **String** |  | [optional] 
**historyTransactionalKey** | **String** |  | [optional] 
**newValue** | **String** |  | [optional] 
**originatingSystemHistoryKey** | **String** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**previousValue** | **String** |  | [optional] 
**resourceName** | **String** |  | [optional] 
**resourceRecordID** | **String** |  | [optional] 
**resourceRecordKey** | **String** |  | [optional] 
**sourceSystemHistoryKey** | **String** |  | [optional] 
**sourceSystemID** | **Double** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**changeType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**changedByMember** | [**Member**](Member.md) |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


