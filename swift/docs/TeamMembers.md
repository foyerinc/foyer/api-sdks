# TeamMembers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**memberKeyNumeric** | **Double** |  | [optional] 
**teamKeyNumeric** | **Double** |  | [optional] 
**teamMemberKeyNumeric** | **Double** |  | [optional] 
**memberKey** | **String** |  | [optional] 
**memberLoginId** | **String** |  | [optional] 
**memberMlsId** | **String** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemKey** | **String** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**sourceSystemID** | **Double** |  | [optional] 
**sourceSystemKey** | **String** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**teamKey** | **String** |  | [optional] 
**teamMemberKey** | **String** |  | [optional] 
**teamMemberNationalAssociationId** | **String** |  | [optional] 
**teamMemberStateLicense** | **String** |  | [optional] 
**teamImpersonationLevel** | **String** |  | [optional] 
**teamMemberType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**originalEntryTimestamp** | **Date** |  | [optional] 
**member** | [**Member**](Member.md) |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


