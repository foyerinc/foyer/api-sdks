# Syndication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**memberId** | **Int** |  | [optional] 
**officeId** | **Int** |  | [optional] 
**publisherId** | **Int** |  | [optional] 
**permission** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**member** | [**Member**](Member.md) |  | [optional] 
**office** | [**Office**](Office.md) |  | [optional] 
**publisher** | [**Publisher**](Publisher.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


