# ShareAPI

All URIs are relative to *https://api.foyer.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteShareById**](ShareAPI.md#deletesharebyid) | **DELETE** /Share/{id} | Delete Share By Id
[**findOneShare**](ShareAPI.md#findoneshare) | **GET** /Share/findOne | Find One Share
[**getShareById**](ShareAPI.md#getsharebyid) | **GET** /Share/{id} | Get Share By Id
[**getShares**](ShareAPI.md#getshares) | **GET** /Share | Get Shares
[**makeShare**](ShareAPI.md#makeshare) | **POST** /Share/makeShare | Make Share
[**patchShareById**](ShareAPI.md#patchsharebyid) | **PATCH** /Share/{id} | Patch Share By Id
[**postShares**](ShareAPI.md#postshares) | **POST** /Share | Post Shares


# **deleteShareById**
```swift
    open class func deleteShareById(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Share By Id

Delete a specific Share with unique id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 

// Delete Share By Id
ShareAPI.deleteShareById(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 

### Return type

Void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findOneShare**
```swift
    open class func findOneShare(filter: String, completion: @escaping (_ data: Share?, _ error: Error?) -> Void)
```

Find One Share

Find a single Share according to defined filter criteria

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String | 

// Find One Share
ShareAPI.findOneShare(filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** |  | 

### Return type

[**Share**](Share.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getShareById**
```swift
    open class func getShareById(id: Int, filter: String? = nil, completion: @escaping (_ data: Share?, _ error: Error?) -> Void)
```

Get Share By Id

Get a specific Share with unique id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let filter = "filter_example" // String |  (optional)

// Get Share By Id
ShareAPI.getShareById(id: id, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **filter** | **String** |  | [optional] 

### Return type

[**Share**](Share.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getShares**
```swift
    open class func getShares(filter: String? = nil, completion: @escaping (_ data: [Share]?, _ error: Error?) -> Void)
```

Get Shares

Get an array of shares according to defined filter criteria

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String |  (optional)

// Get Shares
ShareAPI.getShares(filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** |  | [optional] 

### Return type

[**[Share]**](Share.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **makeShare**
```swift
    open class func makeShare(makeShareRequest: MakeShareRequest, completion: @escaping (_ data: Share?, _ error: Error?) -> Void)
```

Make Share

Make a share with specific contact information

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let makeShareRequest = makeShare_request(propertyId: 123, phoneNumber: "phoneNumber_example", firstName: "firstName_example", lastName: "lastName_example") // MakeShareRequest | 

// Make Share
ShareAPI.makeShare(makeShareRequest: makeShareRequest) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **makeShareRequest** | [**MakeShareRequest**](MakeShareRequest.md) |  | 

### Return type

[**Share**](Share.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patchShareById**
```swift
    open class func patchShareById(id: Int, patchDocument: [PatchDocument]? = nil, completion: @escaping (_ data: Share?, _ error: Error?) -> Void)
```

Patch Share By Id

Patch a specific Share with unique id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let patchDocument = [PatchDocument(op: "op_example", path: "path_example", value: "value_example", from: "from_example")] // [PatchDocument] |  (optional)

// Patch Share By Id
ShareAPI.patchShareById(id: id, patchDocument: patchDocument) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **patchDocument** | [**[PatchDocument]**](PatchDocument.md) |  | [optional] 

### Return type

[**Share**](Share.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postShares**
```swift
    open class func postShares(share: Share, completion: @escaping (_ data: Share?, _ error: Error?) -> Void)
```

Post Shares

Post an array of Share objects

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let share = Share(id: 123, seen: false, receiverId: 123, senderId: 123, propertyId: 123, updatedAt: Date(), createdAt: Date(), receiver: User(id: 123, realm: "realm_example", email: "email_example", username: "username_example", firstName: "firstName_example", lastName: "lastName_example", phone: "phone_example", phoneVerified: false, smsVerificationCode: 123, profileImage: "profileImage_example", location: Location(type: "type_example", coordinates: [123]), newUser: false, emailVerified: false, verificationToken: "verificationToken_example", parentId: 123, updatedAt: Date(), createdAt: Date(), member: Member(id: 123, memberMlsAccessYN: false, memberAORkeyNumeric: 123, memberKeyNumeric: 123, officeKeyNumeric: 123, jobTitle: "jobTitle_example", memberAORMlsId: "memberAORMlsId_example", memberAORkey: "memberAORkey_example", memberAddress1: "memberAddress1_example", memberAddress2: "memberAddress2_example", memberAssociationComments: "memberAssociationComments_example", memberCarrierRoute: "memberCarrierRoute_example", memberCity: "memberCity_example", memberDirectPhone: "memberDirectPhone_example", memberEmail: "memberEmail_example", memberFax: "memberFax_example", memberFirstName: "memberFirstName_example", memberFullName: "memberFullName_example", memberHomePhone: "memberHomePhone_example", memberIsAssistantTo: "memberIsAssistantTo_example", memberKey: "memberKey_example", memberLastName: "memberLastName_example", memberLoginId: "memberLoginId_example", memberMiddleName: "memberMiddleName_example", memberMlsId: "memberMlsId_example", memberMobilePhone: "memberMobilePhone_example", memberNamePrefix: "memberNamePrefix_example", memberNameSuffix: "memberNameSuffix_example", memberNationalAssociationId: "memberNationalAssociationId_example", memberNickname: "memberNickname_example", memberOfficePhone: "memberOfficePhone_example", memberOfficePhoneExt: "memberOfficePhoneExt_example", memberPager: "memberPager_example", memberPassword: "memberPassword_example", memberPhoneTTYTDD: "memberPhoneTTYTDD_example", memberPostalCode: "memberPostalCode_example", memberPostalCodePlus4: "memberPostalCodePlus4_example", memberPreferredPhone: "memberPreferredPhone_example", memberPreferredPhoneExt: "memberPreferredPhoneExt_example", memberStateLicense: "memberStateLicense_example", memberTollFreePhone: "memberTollFreePhone_example", memberVoiceMail: "memberVoiceMail_example", memberVoiceMailExt: "memberVoiceMailExt_example", officeKey: "officeKey_example", officeMlsId: "officeMlsId_example", officeName: "officeName_example", originatingSystemID: "originatingSystemID_example", originatingSystemMemberKey: "originatingSystemMemberKey_example", originatingSystemName: "originatingSystemName_example", sourceSystemID: "sourceSystemID_example", sourceSystemMemberKey: "sourceSystemMemberKey_example", sourceSystemName: "sourceSystemName_example", memberDesignation: ["memberDesignation_example"], memberLanguages: ["memberLanguages_example"], syndicateTo: ["syndicateTo_example"], memberAOR: "memberAOR_example", memberCountry: "memberCountry_example", memberCountyOrParish: "memberCountyOrParish_example", memberMlsSecurityClass: "memberMlsSecurityClass_example", memberOtherPhoneType: "memberOtherPhoneType_example", memberStateLicenseState: "memberStateLicenseState_example", memberStateOrProvince: "memberStateOrProvince_example", memberStatus: "memberStatus_example", memberType: "memberType_example", socialMediaType: "socialMediaType_example", lastLoginTimestamp: Date(), modificationTimestamp: Date(), originalEntryTimestamp: Date(), office: Office(id: 123, iDXOfficeParticipationYN: false, mainOfficeKeyNumeric: 123, officeAORkeyNumeric: 123, officeBrokerKeyNumeric: 123, officeKeyNumeric: 123, officeManagerKeyNumeric: 123, franchiseAffiliation: "franchiseAffiliation_example", mainOfficeKey: "mainOfficeKey_example", mainOfficeMlsId: "mainOfficeMlsId_example", officeAORMlsId: "officeAORMlsId_example", officeAORkey: "officeAORkey_example", officeAddress1: "officeAddress1_example", officeAddress2: "officeAddress2_example", officeAssociationComments: "officeAssociationComments_example", officeBrokerKey: "officeBrokerKey_example", officeBrokerMlsId: "officeBrokerMlsId_example", officeCity: "officeCity_example", officeCorporateLicense: "officeCorporateLicense_example", officeEmail: "officeEmail_example", officeFax: "officeFax_example", officeKey: "officeKey_example", officeManagerKey: "officeManagerKey_example", officeManagerMlsId: "officeManagerMlsId_example", officeMlsId: "officeMlsId_example", officeName: "officeName_example", officeNationalAssociationId: "officeNationalAssociationId_example", officePhone: "officePhone_example", officePhoneExt: "officePhoneExt_example", officePostalCode: "officePostalCode_example", officePostalCodePlus4: "officePostalCodePlus4_example", originatingSystemID: 123, originatingSystemName: "originatingSystemName_example", originatingSystemOfficeKey: "originatingSystemOfficeKey_example", sourceSystemID: 123, sourceSystemName: "sourceSystemName_example", sourceSystemOfficeKey: "sourceSystemOfficeKey_example", syndicateTo: ["syndicateTo_example"], officeAOR: "officeAOR_example", officeBranchType: "officeBranchType_example", officeCountyOrParish: "officeCountyOrParish_example", officeStateOrProvince: "officeStateOrProvince_example", officeStatus: "officeStatus_example", officeType: "officeType_example", socialMediaType: "socialMediaType_example", syndicateAgentOption: "syndicateAgentOption_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), officeBroker: nil, officeManager: nil, mainOffice: nil, originatingSystem: OUID(id: 123, organizationStatus: false, changedByMemberKeyNumeric: 123, organizationAorOuidKeyNumeric: 123, organizationMemberCount: 123, organizationUniqueIdKeyNumeric: 123, changedByMemberID: "changedByMemberID_example", changedByMemberKey: "changedByMemberKey_example", organizationAddress1: "organizationAddress1_example", organizationAddress2: "organizationAddress2_example", organizationAorOuid: "organizationAorOuid_example", organizationAorOuidKey: "organizationAorOuidKey_example", organizationCarrierRoute: "organizationCarrierRoute_example", organizationCity: "organizationCity_example", organizationComments: "organizationComments_example", organizationContactEmail: "organizationContactEmail_example", organizationContactFax: "organizationContactFax_example", organizationContactFirstName: "organizationContactFirstName_example", organizationContactFullName: "organizationContactFullName_example", organizationContactJobTitle: "organizationContactJobTitle_example", organizationContactLastName: "organizationContactLastName_example", organizationContactMiddleName: "organizationContactMiddleName_example", organizationContactNamePrefix: "organizationContactNamePrefix_example", organizationContactNameSuffix: "organizationContactNameSuffix_example", organizationContactPhone: "organizationContactPhone_example", organizationContactPhoneExt: "organizationContactPhoneExt_example", organizationMlsCode: "organizationMlsCode_example", organizationMlsVendorName: "organizationMlsVendorName_example", organizationMlsVendorOuid: "organizationMlsVendorOuid_example", organizationName: "organizationName_example", organizationNationalAssociationId: "organizationNationalAssociationId_example", organizationPostalCode: "organizationPostalCode_example", organizationPostalCodePlus4: "organizationPostalCodePlus4_example", organizationStateLicense: "organizationStateLicense_example", organizationUniqueId: "organizationUniqueId_example", organizationUniqueIdKey: "organizationUniqueIdKey_example", organizationAOR: "organizationAOR_example", organizationCountry: "organizationCountry_example", organizationCountyOrParish: "organizationCountyOrParish_example", organizationSocialMediaType: "organizationSocialMediaType_example", organizationStateLicenseState: "organizationStateLicenseState_example", organizationStateOrProvince: "organizationStateOrProvince_example", organizationType: "organizationType_example", modificationTimestamp: Date(), organizationStatusChangeTimestamp: Date(), originalEntryTimestamp: Date(), changedByMember: nil, originatingSystemContacts: [Contacts(id: 123, anniversary: Date(), birthdate: Date(), contactKeyNumeric: 123, ownerMemberKeyNumeric: 123, assistantEmail: "assistantEmail_example", assistantName: "assistantName_example", assistantPhone: "assistantPhone_example", assistantPhoneExt: "assistantPhoneExt_example", businessFax: "businessFax_example", children: "children_example", company: "company_example", contactKey: "contactKey_example", contactLoginId: "contactLoginId_example", contactPassword: "contactPassword_example", department: "department_example", directPhone: "directPhone_example", email: "email_example", email2: "email2_example", email3: "email3_example", firstName: "firstName_example", fullName: "fullName_example", homeAddress1: "homeAddress1_example", homeAddress2: "homeAddress2_example", homeCarrierRoute: "homeCarrierRoute_example", homeCity: "homeCity_example", homeFax: "homeFax_example", homePhone: "homePhone_example", homePostalCode: "homePostalCode_example", homePostalCodePlus4: "homePostalCodePlus4_example", jobTitle: "jobTitle_example", lastName: "lastName_example", leadSource: "leadSource_example", middleName: "middleName_example", mobilePhone: "mobilePhone_example", namePrefix: "namePrefix_example", nameSuffix: "nameSuffix_example", nickname: "nickname_example", notes: "notes_example", officePhone: "officePhone_example", officePhoneExt: "officePhoneExt_example", originatingSystemContactKey: "originatingSystemContactKey_example", originatingSystemID: 123, originatingSystemName: "originatingSystemName_example", otherAddress1: "otherAddress1_example", otherAddress2: "otherAddress2_example", otherCarrierRoute: "otherCarrierRoute_example", otherCity: "otherCity_example", otherPostalCode: "otherPostalCode_example", otherPostalCodePlus4: "otherPostalCodePlus4_example", ownerMemberID: "ownerMemberID_example", ownerMemberKey: "ownerMemberKey_example", pager: "pager_example", phoneTTYTDD: "phoneTTYTDD_example", referredBy: "referredBy_example", sourceSystemContactKey: "sourceSystemContactKey_example", sourceSystemID: 123, sourceSystemName: "sourceSystemName_example", spousePartnerName: "spousePartnerName_example", tollFreePhone: "tollFreePhone_example", voiceMail: "voiceMail_example", voiceMailExt: "voiceMailExt_example", workAddress1: "workAddress1_example", workAddress2: "workAddress2_example", workCarrierRoute: "workCarrierRoute_example", workCity: "workCity_example", workPostalCode: "workPostalCode_example", workPostalCodePlus4: "workPostalCodePlus4_example", contactType: ["contactType_example"], language: ["language_example"], contactStatus: "contactStatus_example", homeCountry: "homeCountry_example", homeCountyOrParish: "homeCountyOrParish_example", homeStateOrProvince: "homeStateOrProvince_example", otherCountry: "otherCountry_example", otherCountyOrParish: "otherCountyOrParish_example", otherStateOrProvince: "otherStateOrProvince_example", otherPhoneType: "otherPhoneType_example", preferredAddress: "preferredAddress_example", preferredPhone: "preferredPhone_example", workCountry: "workCountry_example", workCountyOrParish: "workCountyOrParish_example", workStateOrProvince: "workStateOrProvince_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), ownerMember: nil, originatingSystem: nil, sourceSystem: nil, contactListingNotes: [ContactListingNotes(id: 123, contactKeyNumeric: 123, contactListingNotesKeyNumeric: 123, listingKeyNumeric: 123, contactKey: "contactKey_example", contactListingNotesKey: "contactListingNotesKey_example", listingId: "listingId_example", listingKey: "listingKey_example", noteContents: "noteContents_example", notedBy: "notedBy_example", modificationTimestamp: Date(), contact: nil)], contactListings: [ContactListings(id: 123, agentNotesUnreadYN: false, contactNotesUnreadYN: false, directEmailYN: false, listingViewedYN: false, contactKeyNumeric: 123, contactListingsKeyNumeric: 123, listingKeyNumeric: 123, contactKey: "contactKey_example", contactListingsKey: "contactListingsKey_example", contactLoginId: "contactLoginId_example", listingId: "listingId_example", listingKey: "listingKey_example", className: "className_example", contactListingPreference: "contactListingPreference_example", resourceName: "resourceName_example", lastAgentNoteTimestamp: Date(), lastContactNoteTimestamp: Date(), listingModificationTimestamp: Date(), listingSentTimestamp: Date(), modificationTimestamp: Date(), portalLastVisitedTimestamp: Date(), contact: nil, property: Property(id: 123, location: nil, listingKey: "listingKey_example", additionalParcelsYN: false, associationYN: false, attachedGarageYN: false, basementYN: false, carportYN: false, coolingYN: false, cropsIncludedYN: false, dualVariableCompensationYN: false, electricOnPropertyYN: false, farmCreditServiceInclYN: false, fireplaceYN: false, garageYN: false, grazingPermitsBlmYN: false, grazingPermitsForestServiceYN: false, grazingPermitsPrivateYN: false, habitableResidenceYN: false, heatingYN: false, homeWarrantyYN: false, horseYN: false, internetAddressDisplayYN: false, internetAutomatedValuationDisplayYN: false, internetConsumerCommentYN: false, internetEntireListingDisplayYN: false, irrigationWaterRightsYN: false, landLeaseYN: false, leaseAssignableYN: false, leaseConsideredYN: false, leaseRenewalOptionYN: false, mobileHomeRemainsYN: false, newConstructionYN: false, openParkingYN: false, poolPrivateYN: false, propertyAttachedYN: false, rentControlYN: false, seniorCommunityYN: false, showingAttendedYN: false, signOnPropertyYN: false, spaYN: false, viewYN: false, waterfrontYN: false, availabilityDate: Date(), cancellationDate: Date(), closeDate: Date(), contingentDate: Date(), contractStatusChangeDate: Date(), expirationDate: Date(), landLeaseExpirationDate: Date(), leaseExpiration: Date(), listingContractDate: Date(), offMarketDate: Date(), onMarketDate: Date(), purchaseContractDate: Date(), withdrawnDate: Date(), aboveGradeFinishedArea: 123, associationFee: 123, associationFee2: 123, bathroomsFull: 123, bathroomsHalf: 123, bathroomsOneQuarter: 123, bathroomsPartial: 123, bathroomsThreeQuarter: 123, bathroomsTotalInteger: 123, bedroomsPossible: 123, bedroomsTotal: 123, belowGradeFinishedArea: 123, buildingAreaTotal: 123, buyerAgentKeyNumeric: 123, buyerOfficeKeyNumeric: 123, buyerTeamKeyNumeric: 123, cableTvExpense: 123, capRate: 123, carportSpaces: 123, closePrice: 123, coBuyerAgentKeyNumeric: 123, coBuyerOfficeKeyNumeric: 123, coListAgentKeyNumeric: 123, coListOfficeKeyNumeric: 123, concessionsAmount: 123, coveredSpaces: 123, cultivatedArea: 123, cumulativeDaysOnMarket: 123, daysOnMarket: 123, distanceToBusNumeric: 123, distanceToElectricNumeric: 123, distanceToFreewayNumeric: 123, distanceToGasNumeric: 123, distanceToPhoneServiceNumeric: 123, distanceToPlaceofWorshipNumeric: 123, distanceToSchoolBusNumeric: 123, distanceToSchoolsNumeric: 123, distanceToSewerNumeric: 123, distanceToShoppingNumeric: 123, distanceToStreetNumeric: 123, distanceToWaterNumeric: 123, documentsCount: 123, electricExpense: 123, elevation: 123, entryLevel: 123, fireplacesTotal: 123, foundationArea: 123, fuelExpense: 123, furnitureReplacementExpense: 123, garageSpaces: 123, gardenerExpense: 123, grossIncome: 123, grossScheduledIncome: 123, insuranceExpense: 123, irrigationWaterRightsAcres: 123, landLeaseAmount: 123, latitude: 123, leasableArea: 123, leaseAmount: 123, licensesExpense: 123, listAgentKeyNumeric: 123, listOfficeKeyNumeric: 123, listPrice: 123, listPriceLow: 123, listTeamKeyNumeric: 123, listingKeyNumeric: 123, livingArea: 123, longitude: 123, lotSizeAcres: 123, lotSizeArea: 123, lotSizeSquareFeet: 123, mainLevelBathrooms: 123, mainLevelBedrooms: 123, maintenanceExpense: 123, managerExpense: 123, mobileLength: 123, mobileWidth: 123, netOperatingIncome: 123, newTaxesExpense: 123, numberOfBuildings: 123, numberOfFullTimeEmployees: 123, numberOfLots: 123, numberOfPads: 123, numberOfPartTimeEmployees: 123, numberOfSeparateElectricMeters: 123, numberOfSeparateGasMeters: 123, numberOfSeparateWaterMeters: 123, numberOfUnitsInCommunity: 123, numberOfUnitsLeased: 123, numberOfUnitsMoMo: 123, numberOfUnitsTotal: 123, numberOfUnitsVacant: 123, openParkingSpaces: 123, operatingExpense: 123, originalListPrice: 123, otherExpense: 123, parkingTotal: 123, pastureArea: 123, pestControlExpense: 123, photosCount: 123, poolExpense: 123, previousListPrice: 123, professionalManagementExpense: 123, rangeArea: 123, roomsTotal: 123, seatingCapacity: 123, showingAdvanceNotice: 123, stories: 123, storiesTotal: 123, streetNumberNumeric: 123, suppliesExpense: 123, taxAnnualAmount: 123, taxAssessedValue: 123, taxOtherAnnualAssessmentAmount: 123, taxYear: 123, totalActualRent: 123, trashExpense: 123, vacancyAllowance: 123, vacancyAllowanceRate: 123, videosCount: 123, walkScore: 123, waterSewerExpense: 123, woodedArea: 123, workmansCompensationExpense: 123, yearBuilt: 123, yearBuiltEffective: 123, yearEstablished: 123, yearsCurrentOwner: 123, accessCode: "accessCode_example", additionalParcelsDescription: "additionalParcelsDescription_example", anchorsCoTenants: "anchorsCoTenants_example", associationName: "associationName_example", associationName2: "associationName2_example", associationPhone: "associationPhone_example", associationPhone2: "associationPhone2_example", builderModel: "builderModel_example", builderName: "builderName_example", buildingName: "buildingName_example", businessName: "businessName_example", buyerAgencyCompensation: "buyerAgencyCompensation_example", buyerAgentDirectPhone: "buyerAgentDirectPhone_example", buyerAgentEmail: "buyerAgentEmail_example", buyerAgentFax: "buyerAgentFax_example", buyerAgentFirstName: "buyerAgentFirstName_example", buyerAgentFullName: "buyerAgentFullName_example", buyerAgentHomePhone: "buyerAgentHomePhone_example", buyerAgentKey: "buyerAgentKey_example", buyerAgentLastName: "buyerAgentLastName_example", buyerAgentMiddleName: "buyerAgentMiddleName_example", buyerAgentMlsId: "buyerAgentMlsId_example", buyerAgentMobilePhone: "buyerAgentMobilePhone_example", buyerAgentNamePrefix: "buyerAgentNamePrefix_example", buyerAgentNameSuffix: "buyerAgentNameSuffix_example", buyerAgentOfficePhone: "buyerAgentOfficePhone_example", buyerAgentOfficePhoneExt: "buyerAgentOfficePhoneExt_example", buyerAgentPager: "buyerAgentPager_example", buyerAgentPreferredPhone: "buyerAgentPreferredPhone_example", buyerAgentPreferredPhoneExt: "buyerAgentPreferredPhoneExt_example", buyerAgentStateLicense: "buyerAgentStateLicense_example", buyerAgentTollFreePhone: "buyerAgentTollFreePhone_example", buyerAgentURL: "buyerAgentURL_example", buyerAgentVoiceMail: "buyerAgentVoiceMail_example", buyerAgentVoiceMailExt: "buyerAgentVoiceMailExt_example", buyerOfficeEmail: "buyerOfficeEmail_example", buyerOfficeFax: "buyerOfficeFax_example", buyerOfficeKey: "buyerOfficeKey_example", buyerOfficeMlsId: "buyerOfficeMlsId_example", buyerOfficeName: "buyerOfficeName_example", buyerOfficePhone: "buyerOfficePhone_example", buyerOfficePhoneExt: "buyerOfficePhoneExt_example", buyerOfficeURL: "buyerOfficeURL_example", buyerTeamKey: "buyerTeamKey_example", buyerTeamName: "buyerTeamName_example", carrierRoute: "carrierRoute_example", cityRegion: "cityRegion_example", coBuyerAgentDirectPhone: "coBuyerAgentDirectPhone_example", coBuyerAgentEmail: "coBuyerAgentEmail_example", coBuyerAgentFax: "coBuyerAgentFax_example", coBuyerAgentFirstName: "coBuyerAgentFirstName_example", coBuyerAgentFullName: "coBuyerAgentFullName_example", coBuyerAgentHomePhone: "coBuyerAgentHomePhone_example", coBuyerAgentKey: "coBuyerAgentKey_example", coBuyerAgentLastName: "coBuyerAgentLastName_example", coBuyerAgentMiddleName: "coBuyerAgentMiddleName_example", coBuyerAgentMlsId: "coBuyerAgentMlsId_example", coBuyerAgentMobilePhone: "coBuyerAgentMobilePhone_example", coBuyerAgentNamePrefix: "coBuyerAgentNamePrefix_example", coBuyerAgentNameSuffix: "coBuyerAgentNameSuffix_example", coBuyerAgentOfficePhone: "coBuyerAgentOfficePhone_example", coBuyerAgentOfficePhoneExt: "coBuyerAgentOfficePhoneExt_example", coBuyerAgentPager: "coBuyerAgentPager_example", coBuyerAgentPreferredPhone: "coBuyerAgentPreferredPhone_example", coBuyerAgentPreferredPhoneExt: "coBuyerAgentPreferredPhoneExt_example", coBuyerAgentStateLicense: "coBuyerAgentStateLicense_example", coBuyerAgentTollFreePhone: "coBuyerAgentTollFreePhone_example", coBuyerAgentURL: "coBuyerAgentURL_example", coBuyerAgentVoiceMail: "coBuyerAgentVoiceMail_example", coBuyerAgentVoiceMailExt: "coBuyerAgentVoiceMailExt_example", coBuyerOfficeEmail: "coBuyerOfficeEmail_example", coBuyerOfficeFax: "coBuyerOfficeFax_example", coBuyerOfficeKey: "coBuyerOfficeKey_example", coBuyerOfficeMlsId: "coBuyerOfficeMlsId_example", coBuyerOfficeName: "coBuyerOfficeName_example", coBuyerOfficePhone: "coBuyerOfficePhone_example", coBuyerOfficePhoneExt: "coBuyerOfficePhoneExt_example", coBuyerOfficeURL: "coBuyerOfficeURL_example", coListAgentDirectPhone: "coListAgentDirectPhone_example", coListAgentEmail: "coListAgentEmail_example", coListAgentFax: "coListAgentFax_example", coListAgentFirstName: "coListAgentFirstName_example", coListAgentFullName: "coListAgentFullName_example", coListAgentHomePhone: "coListAgentHomePhone_example", coListAgentKey: "coListAgentKey_example", coListAgentLastName: "coListAgentLastName_example", coListAgentMiddleName: "coListAgentMiddleName_example", coListAgentMlsId: "coListAgentMlsId_example", coListAgentMobilePhone: "coListAgentMobilePhone_example", coListAgentNamePrefix: "coListAgentNamePrefix_example", coListAgentNameSuffix: "coListAgentNameSuffix_example", coListAgentOfficePhone: "coListAgentOfficePhone_example", coListAgentOfficePhoneExt: "coListAgentOfficePhoneExt_example", coListAgentPager: "coListAgentPager_example", coListAgentPreferredPhone: "coListAgentPreferredPhone_example", coListAgentPreferredPhoneExt: "coListAgentPreferredPhoneExt_example", coListAgentStateLicense: "coListAgentStateLicense_example", coListAgentTollFreePhone: "coListAgentTollFreePhone_example", coListAgentURL: "coListAgentURL_example", coListAgentVoiceMail: "coListAgentVoiceMail_example", coListAgentVoiceMailExt: "coListAgentVoiceMailExt_example", coListOfficeEmail: "coListOfficeEmail_example", coListOfficeFax: "coListOfficeFax_example", coListOfficeKey: "coListOfficeKey_example", coListOfficeMlsId: "coListOfficeMlsId_example", coListOfficeName: "coListOfficeName_example", coListOfficePhone: "coListOfficePhone_example", coListOfficePhoneExt: "coListOfficePhoneExt_example", coListOfficeURL: "coListOfficeURL_example", concessionsComments: "concessionsComments_example", continentRegion: "continentRegion_example", contingency: "contingency_example", copyrightNotice: "copyrightNotice_example", countryRegion: "countryRegion_example", crossStreet: "crossStreet_example", dOH1: "dOH1_example", dOH2: "dOH2_example", dOH3: "dOH3_example", directions: "directions_example", disclaimer: "disclaimer_example", distanceToBusComments: "distanceToBusComments_example", distanceToElectricComments: "distanceToElectricComments_example", distanceToFreewayComments: "distanceToFreewayComments_example", distanceToGasComments: "distanceToGasComments_example", distanceToPhoneServiceComments: "distanceToPhoneServiceComments_example", distanceToPlaceofWorshipComments: "distanceToPlaceofWorshipComments_example", distanceToSchoolBusComments: "distanceToSchoolBusComments_example", distanceToSchoolsComments: "distanceToSchoolsComments_example", distanceToSewerComments: "distanceToSewerComments_example", distanceToShoppingComments: "distanceToShoppingComments_example", distanceToStreetComments: "distanceToStreetComments_example", distanceToWaterComments: "distanceToWaterComments_example", entryLocation: "entryLocation_example", exclusions: "exclusions_example", frontageLength: "frontageLength_example", hoursDaysOfOperationDescription: "hoursDaysOfOperationDescription_example", inclusions: "inclusions_example", license1: "license1_example", license2: "license2_example", license3: "license3_example", listAgentDirectPhone: "listAgentDirectPhone_example", listAgentEmail: "listAgentEmail_example", listAgentFax: "listAgentFax_example", listAgentFirstName: "listAgentFirstName_example", listAgentFullName: "listAgentFullName_example", listAgentHomePhone: "listAgentHomePhone_example", listAgentKey: "listAgentKey_example", listAgentLastName: "listAgentLastName_example", listAgentMiddleName: "listAgentMiddleName_example", listAgentMlsId: "listAgentMlsId_example", listAgentMobilePhone: "listAgentMobilePhone_example", listAgentNamePrefix: "listAgentNamePrefix_example", listAgentNameSuffix: "listAgentNameSuffix_example", listAgentOfficePhone: "listAgentOfficePhone_example", listAgentOfficePhoneExt: "listAgentOfficePhoneExt_example", listAgentPager: "listAgentPager_example", listAgentPreferredPhone: "listAgentPreferredPhone_example", listAgentPreferredPhoneExt: "listAgentPreferredPhoneExt_example", listAgentStateLicense: "listAgentStateLicense_example", listAgentTollFreePhone: "listAgentTollFreePhone_example", listAgentURL: "listAgentURL_example", listAgentVoiceMail: "listAgentVoiceMail_example", listAgentVoiceMailExt: "listAgentVoiceMailExt_example", listOfficeEmail: "listOfficeEmail_example", listOfficeFax: "listOfficeFax_example", listOfficeKey: "listOfficeKey_example", listOfficeMlsId: "listOfficeMlsId_example", listOfficeName: "listOfficeName_example", listOfficePhone: "listOfficePhone_example", listOfficePhoneExt: "listOfficePhoneExt_example", listOfficeURL: "listOfficeURL_example", listTeamKey: "listTeamKey_example", listTeamName: "listTeamName_example", listingId: "listingId_example", lockBoxLocation: "lockBoxLocation_example", lockBoxSerialNumber: "lockBoxSerialNumber_example", lotSizeDimensions: "lotSizeDimensions_example", make: "make_example", mapCoordinate: "mapCoordinate_example", mapCoordinateSource: "mapCoordinateSource_example", mapURL: "mapURL_example", model: "model_example", occupantName: "occupantName_example", occupantPhone: "occupantPhone_example", originatingSystemID: "originatingSystemID_example", originatingSystemKey: "originatingSystemKey_example", originatingSystemName: "originatingSystemName_example", otherParking: "otherParking_example", ownerName: "ownerName_example", ownerPhone: "ownerPhone_example", ownership: ["ownership_example"], parcelNumber: "parcelNumber_example", parkManagerName: "parkManagerName_example", parkManagerPhone: "parkManagerPhone_example", parkName: "parkName_example", postalCode: "postalCode_example", postalCodePlus4: "postalCodePlus4_example", privateOfficeRemarks: "privateOfficeRemarks_example", privateRemarks: "privateRemarks_example", publicRemarks: "publicRemarks_example", publicSurveyRange: "publicSurveyRange_example", publicSurveySection: "publicSurveySection_example", publicSurveyTownship: "publicSurveyTownship_example", rVParkingDimensions: "rVParkingDimensions_example", serialU: "serialU_example", serialX: "serialX_example", serialXX: "serialXX_example", showingContactName: "showingContactName_example", showingContactPhone: "showingContactPhone_example", showingContactPhoneExt: "showingContactPhoneExt_example", showingInstructions: "showingInstructions_example", sourceSystemID: "sourceSystemID_example", sourceSystemKey: "sourceSystemKey_example", sourceSystemName: "sourceSystemName_example", stateRegion: "stateRegion_example", streetAdditionalInfo: "streetAdditionalInfo_example", streetName: "streetName_example", streetNumber: "streetNumber_example", streetSuffixModifier: "streetSuffixModifier_example", subAgencyCompensation: "subAgencyCompensation_example", subdivisionName: "subdivisionName_example", syndicationRemarks: "syndicationRemarks_example", taxBlock: "taxBlock_example", taxBookNumber: "taxBookNumber_example", taxLegalDescription: "taxLegalDescription_example", taxLot: "taxLot_example", taxMapNumber: "taxMapNumber_example", taxParcelLetter: "taxParcelLetter_example", taxTract: "taxTract_example", topography: "topography_example", township: "township_example", transactionBrokerCompensation: "transactionBrokerCompensation_example", unitNumber: "unitNumber_example", universalPropertyId: "universalPropertyId_example", universalPropertySubId: "universalPropertySubId_example", unparsedAddress: "unparsedAddress_example", virtualTourURLBranded: "virtualTourURLBranded_example", virtualTourURLUnbranded: "virtualTourURLUnbranded_example", waterBodyName: "waterBodyName_example", yearBuiltDetails: "yearBuiltDetails_example", zoning: "zoning_example", zoningDescription: "zoningDescription_example", appliances: ["appliances_example"], accessibilityFeatures: ["accessibilityFeatures_example"], architecturalStyle: ["architecturalStyle_example"], associationAmenities: "associationAmenities_example", associationFeeIncludes: "associationFeeIncludes_example", basement: "basement_example", bodyType: "bodyType_example", buildingFeatures: ["buildingFeatures_example"], businessType: ["businessType_example"], buyerAgentDesignation: ["buyerAgentDesignation_example"], buyerFinancing: ["buyerFinancing_example"], coBuyerAgentDesignation: ["coBuyerAgentDesignation_example"], coListAgentDesignation: ["coListAgentDesignation_example"], commonWalls: ["commonWalls_example"], communityFeatures: ["communityFeatures_example"], constructionMaterials: ["constructionMaterials_example"], cooling: ["cooling_example"], currentFinancing: ["currentFinancing_example"], currentUse: ["currentUse_example"], developmentStatus: ["developmentStatus_example"], disclosures: ["disclosures_example"], documentsAvailable: ["documentsAvailable_example"], doorFeatures: ["doorFeatures_example"], electric: ["electric_example"], existingLeaseType: ["existingLeaseType_example"], exteriorFeatures: ["exteriorFeatures_example"], fencing: ["fencing_example"], financialDataSource: ["financialDataSource_example"], fireplaceFeatures: ["fireplaceFeatures_example"], flooring: ["flooring_example"], foundationDetails: ["foundationDetails_example"], frontageType: ["frontageType_example"], greenBuildingVerificationType: ["greenBuildingVerificationType_example"], greenEnergyEfficient: ["greenEnergyEfficient_example"], greenEnergyGeneration: ["greenEnergyGeneration_example"], greenIndoorAirQuality: ["greenIndoorAirQuality_example"], greenLocation: ["greenLocation_example"], greenSustainability: ["greenSustainability_example"], greenWaterConservation: ["greenWaterConservation_example"], heating: ["heating_example"], horseAmenities: ["horseAmenities_example"], hoursDaysOfOperation: ["hoursDaysOfOperation_example"], incomeIncludes: ["incomeIncludes_example"], interiorFeatures: ["interiorFeatures_example"], irrigationSource: ["irrigationSource_example"], laborInformation: ["laborInformation_example"], laundryFeatures: ["laundryFeatures_example"], leaseRenewalCompensation: ["leaseRenewalCompensation_example"], levels: ["levels_example"], listAgentDesignation: ["listAgentDesignation_example"], listingTerms: ["listingTerms_example"], lockBoxType: ["lockBoxType_example"], lotFeatures: ["lotFeatures_example"], operatingExpenseIncludes: ["operatingExpenseIncludes_example"], otherEquipment: ["otherEquipment_example"], otherStructures: ["otherStructures_example"], ownerPays: ["ownerPays_example"], parkingFeatures: ["parkingFeatures_example"], patioAndPorchFeatures: ["patioAndPorchFeatures_example"], petsAllowed: ["petsAllowed_example"], poolFeatures: ["poolFeatures_example"], possession: ["possession_example"], possibleUse: ["possibleUse_example"], powerProductionType: ["powerProductionType_example"], propertyCondition: ["propertyCondition_example"], rentIncludes: ["rentIncludes_example"], roadFrontageType: ["roadFrontageType_example"], roadResponsibility: ["roadResponsibility_example"], roadSurfaceType: ["roadSurfaceType_example"], roof: ["roof_example"], roomType: ["roomType_example"], securityFeatures: ["securityFeatures_example"], sewer: ["sewer_example"], showingContactType: ["showingContactType_example"], showingDays: ["showingDays_example"], showingRequirements: ["showingRequirements_example"], skirt: ["skirt_example"], spaFeatures: ["spaFeatures_example"], specialLicenses: ["specialLicenses_example"], specialListingConditions: ["specialListingConditions_example"], structureType: ["structureType_example"], syndicateTo: ["syndicateTo_example"], taxStatusCurrent: ["taxStatusCurrent_example"], tenantPays: ["tenantPays_example"], unitTypeType: ["unitTypeType_example"], utilities: ["utilities_example"], vegetation: ["vegetation_example"], view: ["view_example"], waterSource: ["waterSource_example"], waterfrontFeatures: ["waterfrontFeatures_example"], windowFeatures: ["windowFeatures_example"], aboveGradeFinishedAreaSource: "aboveGradeFinishedAreaSource_example", aboveGradeFinishedAreaUnits: "aboveGradeFinishedAreaUnits_example", associationFee2Frequency: "associationFee2Frequency_example", associationFeeFrequency: "associationFeeFrequency_example", belowGradeFinishedAreaSource: "belowGradeFinishedAreaSource_example", belowGradeFinishedAreaUnits: "belowGradeFinishedAreaUnits_example", buildingAreaSource: "buildingAreaSource_example", buildingAreaUnits: "buildingAreaUnits_example", buyerAgencyCompensationType: "buyerAgencyCompensationType_example", buyerAgentAOR: "buyerAgentAOR_example", buyerOfficeAOR: "buyerOfficeAOR_example", city: "city_example", coBuyerAgentAOR: "coBuyerAgentAOR_example", coBuyerOfficeAOR: "coBuyerOfficeAOR_example", coListAgentAOR: "coListAgentAOR_example", coListOfficeAOR: "coListOfficeAOR_example", commonInterest: "commonInterest_example", concessions: "concessions_example", country: "country_example", countyOrParish: "countyOrParish_example", directionFaces: "directionFaces_example", distanceToBusUnits: "distanceToBusUnits_example", distanceToElectricUnits: "distanceToElectricUnits_example", distanceToFreewayUnits: "distanceToFreewayUnits_example", distanceToGasUnits: "distanceToGasUnits_example", distanceToPhoneServiceUnits: "distanceToPhoneServiceUnits_example", distanceToPlaceofWorshipUnits: "distanceToPlaceofWorshipUnits_example", distanceToSchoolBusUnits: "distanceToSchoolBusUnits_example", distanceToSchoolsUnits: "distanceToSchoolsUnits_example", distanceToSewerUnits: "distanceToSewerUnits_example", distanceToShoppingUnits: "distanceToShoppingUnits_example", distanceToStreetUnits: "distanceToStreetUnits_example", distanceToWaterUnits: "distanceToWaterUnits_example", elementarySchool: "elementarySchool_example", elementarySchoolDistrict: "elementarySchoolDistrict_example", elevationUnits: "elevationUnits_example", farmLandAreaSource: "farmLandAreaSource_example", farmLandAreaUnits: "farmLandAreaUnits_example", furnished: "furnished_example", highSchool: "highSchool_example", highSchoolDistrict: "highSchoolDistrict_example", landLeaseAmountFrequency: "landLeaseAmountFrequency_example", leasableAreaUnits: "leasableAreaUnits_example", leaseAmountFrequency: "leaseAmountFrequency_example", leaseTerm: "leaseTerm_example", listAOR: "listAOR_example", listAgentAOR: "listAgentAOR_example", listOfficeAOR: "listOfficeAOR_example", listingAgreement: "listingAgreement_example", listingService: "listingService_example", livingAreaSource: "livingAreaSource_example", livingAreaUnits: "livingAreaUnits_example", lotDimensionsSource: "lotDimensionsSource_example", lotSizeSource: "lotSizeSource_example", lotSizeUnits: "lotSizeUnits_example", mLSAreaMajor: "mLSAreaMajor_example", mLSAreaMinor: "mLSAreaMinor_example", majorChangeType: "majorChangeType_example", middleOrJuniorSchool: "middleOrJuniorSchool_example", middleOrJuniorSchoolDistrict: "middleOrJuniorSchoolDistrict_example", mlsStatus: "mlsStatus_example", mobileDimUnits: "mobileDimUnits_example", occupantType: "occupantType_example", ownershipType: "ownershipType_example", postalCity: "postalCity_example", propertySubType: "propertySubType_example", propertyType: "propertyType_example", standardStatus: "standardStatus_example", stateOrProvince: "stateOrProvince_example", streetDirPrefix: "streetDirPrefix_example", streetDirSuffix: "streetDirSuffix_example", streetSuffix: "streetSuffix_example", subAgencyCompensationType: "subAgencyCompensationType_example", transactionBrokerCompensationType: "transactionBrokerCompensationType_example", unitsFurnished: "unitsFurnished_example", yearBuiltSource: "yearBuiltSource_example", documentsChangeTimestamp: Date(), majorChangeTimestamp: Date(), modificationTimestamp: Date(), offMarketTimestamp: Date(), onMarketTimestamp: Date(), originalEntryTimestamp: Date(), pendingTimestamp: Date(), photosChangeTimestamp: Date(), priceChangeTimestamp: Date(), showingEndTime: Date(), showingStartTime: Date(), statusChangeTimestamp: Date(), videosChangeTimestamp: Date(), buyerAgent: nil, coBuyerAgent: nil, coListAgent: nil, listAgent: nil, buyerOffice: nil, coBuyerOffice: nil, coListOffice: nil, listOffice: nil, originatingSystem: nil, sourceSystem: nil, listTeam: Teams(id: 123, teamKeyNumeric: 123, teamLeadKeyNumeric: 123, originatingSystemID: 123, originatingSystemKey: "originatingSystemKey_example", originatingSystemName: "originatingSystemName_example", sourceSystemID: 123, sourceSystemKey: "sourceSystemKey_example", sourceSystemName: "sourceSystemName_example", teamAddress1: "teamAddress1_example", teamAddress2: "teamAddress2_example", teamCarrierRoute: "teamCarrierRoute_example", teamCity: "teamCity_example", teamDescription: "teamDescription_example", teamDirectPhone: "teamDirectPhone_example", teamEmail: "teamEmail_example", teamFax: "teamFax_example", teamKey: "teamKey_example", teamLeadKey: "teamLeadKey_example", teamLeadLoginId: "teamLeadLoginId_example", teamLeadMlsId: "teamLeadMlsId_example", teamLeadNationalAssociationId: "teamLeadNationalAssociationId_example", teamLeadStateLicense: "teamLeadStateLicense_example", teamMobilePhone: "teamMobilePhone_example", teamName: "teamName_example", teamOfficePhone: "teamOfficePhone_example", teamOfficePhoneExt: "teamOfficePhoneExt_example", teamPostalCode: "teamPostalCode_example", teamPostalCodePlus4: "teamPostalCodePlus4_example", teamPreferredPhone: "teamPreferredPhone_example", teamPreferredPhoneExt: "teamPreferredPhoneExt_example", teamTollFreePhone: "teamTollFreePhone_example", teamVoiceMail: "teamVoiceMail_example", teamVoiceMailExt: "teamVoiceMailExt_example", socialMediaType: "socialMediaType_example", teamCountry: "teamCountry_example", teamCountyOrParish: "teamCountyOrParish_example", teamLeadStateLicenseState: "teamLeadStateLicenseState_example", teamStateOrProvince: "teamStateOrProvince_example", teamStatus: "teamStatus_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), teamLead: nil, originatingSystem: nil, sourceSystem: nil, buyerTeamProperties: nil, listTeamProperties: nil), buyerTeam: nil, contactListings: [nil], favorites: [Favorite(id: 123, userId: 123, propertyId: 123, session: "session_example", filter: "filter_example", detailsViewed: false, imagesViewed: 123, notes: "notes_example", updatedAt: Date(), createdAt: Date(), user: nil, property: nil)], openHouse: [OpenHouse(id: 123, appointmentRequiredYN: false, openHouseDate: Date(), listingKeyNumeric: 123, openHouseKeyNumeric: 123, showingAgentKeyNumeric: 123, listingId: "listingId_example", listingKey: "listingKey_example", openHouseId: "openHouseId_example", openHouseKey: "openHouseKey_example", openHouseRemarks: "openHouseRemarks_example", originatingSystemID: 123, originatingSystemKey: "originatingSystemKey_example", originatingSystemName: "originatingSystemName_example", refreshments: "refreshments_example", showingAgentFirstName: "showingAgentFirstName_example", showingAgentKey: "showingAgentKey_example", showingAgentLastName: "showingAgentLastName_example", showingAgentMlsID: "showingAgentMlsID_example", sourceSystemID: 123, sourceSystemKey: "sourceSystemKey_example", sourceSystemName: "sourceSystemName_example", openHouseAttendedBy: "openHouseAttendedBy_example", openHouseStatus: "openHouseStatus_example", openHouseType: "openHouseType_example", modificationTimestamp: Date(), openHouseEndTime: Date(), openHouseStartTime: Date(), originalEntryTimestamp: Date(), showingAgent: nil, originatingSystem: nil, sourceSystem: nil, property: nil)], greenBuildingVerifications: [PropertyGreenVerification(id: 123, greenBuildingVerificationKeyNumeric: 123, greenVerificationMetric: 123, greenVerificationYear: 123, listingKeyNumeric: 123, greenBuildingVerificationKey: "greenBuildingVerificationKey_example", greenVerificationBody: "greenVerificationBody_example", greenVerificationRating: "greenVerificationRating_example", greenVerificationURL: "greenVerificationURL_example", greenVerificationVersion: "greenVerificationVersion_example", listingId: "listingId_example", listingKey: "listingKey_example", greenBuildingVerificationType: "greenBuildingVerificationType_example", greenVerificationSource: "greenVerificationSource_example", greenVerificationStatus: "greenVerificationStatus_example", modificationTimestamp: Date(), property: nil)], powerProductions: [PropertyPowerProduction(id: 123, listingKeyNumeric: 123, powerProductionAnnual: 123, powerProductionKeyNumeric: 123, powerProductionSize: 123, powerProductionYearInstall: 123, listingId: "listingId_example", listingKey: "listingKey_example", powerProductionKey: "powerProductionKey_example", powerProductionAnnualStatus: "powerProductionAnnualStatus_example", powerProductionType: "powerProductionType_example", modificationTimestamp: Date(), property: nil)], rooms: [PropertyRooms(id: 123, listingKeyNumeric: 123, roomArea: 123, roomKeyNumeric: 123, roomLength: 123, roomWidth: 123, listingId: "listingId_example", listingKey: "listingKey_example", roomDescription: "roomDescription_example", roomDimensions: "roomDimensions_example", roomKey: "roomKey_example", roomFeatures: ["roomFeatures_example"], roomAreaSource: "roomAreaSource_example", roomAreaUnits: "roomAreaUnits_example", roomLengthWidthUnits: "roomLengthWidthUnits_example", roomType: "roomType_example", modificationTimestamp: Date(), property: nil)], unitTypes: [PropertyUnitTypes(id: 123, unitTypeGarageAttachedYN: false, listingKeyNumeric: 123, unitTypeActualRent: 123, unitTypeBathsTotal: 123, unitTypeBedsTotal: 123, unitTypeGarageSpaces: 123, unitTypeKeyNumeric: 123, unitTypeProForma: 123, unitTypeTotalRent: 123, unitTypeUnitsTotal: 123, listingId: "listingId_example", listingKey: "listingKey_example", unitTypeDescription: "unitTypeDescription_example", unitTypeKey: "unitTypeKey_example", unitTypeType: "unitTypeType_example", modificationTimestamp: Date(), property: nil)], shares: [nil], showings: [Showing(id: 123, listingKeyNumeric: 123, showingAgentKeyNumeric: 123, showingKeyNumeric: 123, agentOriginatingSystemID: "agentOriginatingSystemID_example", agentOriginatingSystemName: "agentOriginatingSystemName_example", agentSourceSystemID: "agentSourceSystemID_example", agentSourceSystemName: "agentSourceSystemName_example", listingId: "listingId_example", listingKey: "listingKey_example", listingOriginatingSystemID: "listingOriginatingSystemID_example", listingOriginatingSystemName: "listingOriginatingSystemName_example", listingSourceSystemID: 123, listingSourceSystemName: "listingSourceSystemName_example", originatingSystemAgentKey: "originatingSystemAgentKey_example", originatingSystemListingKey: "originatingSystemListingKey_example", originatingSystemShowingKey: "originatingSystemShowingKey_example", showingAgentKey: "showingAgentKey_example", showingAgentMlsID: "showingAgentMlsID_example", showingId: "showingId_example", showingKey: "showingKey_example", showingOriginatingSystemID: "showingOriginatingSystemID_example", showingOriginatingSystemName: "showingOriginatingSystemName_example", showingSourceSystemID: 123, showingSourceSystemName: "showingSourceSystemName_example", sourceSystemAgentKey: "sourceSystemAgentKey_example", sourceSystemListingKey: "sourceSystemListingKey_example", sourceSystemShowingKey: "sourceSystemShowingKey_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), showingEndTimestamp: Date(), showingRequestedTimestamp: Date(), showingStartTimestamp: Date(), showingAgent: nil, agentOriginatingSystem: nil, agentSourceSystem: nil, listingOriginatingSystem: nil, listingSourceSystem: nil, showingOriginatingSystem: nil, showingSourceSystem: nil, property: nil)], transactions: [Transaction(id: 123, userId: 123, propertyId: 123, type: "type_example", detailsViewed: false, imagesViewed: 123, timeToDecision: 123, filter: "filter_example", session: "session_example", updatedAt: Date(), createdAt: Date(), user: nil, property: nil)], media: [Media(id: 123, mediaKey: "mediaKey_example", preferredPhotoYN: false, deleted: false, changedByMemberKeyNumeric: 123, imageHeight: 123, imageWidth: 123, mediaKeyNumeric: 123, order: 123, resourceRecordKeyNumeric: 123, hash: "hash_example", changedByMemberID: "changedByMemberID_example", changedByMemberKey: "changedByMemberKey_example", longDescription: "longDescription_example", mediaHTML: "mediaHTML_example", mediaObjectID: "mediaObjectID_example", mediaURL: "mediaURL_example", originatingSystemID: "originatingSystemID_example", originatingSystemMediaKey: "originatingSystemMediaKey_example", originatingSystemName: "originatingSystemName_example", resourceRecordID: "resourceRecordID_example", resourceRecordKey: "resourceRecordKey_example", shortDescription: "shortDescription_example", sourceSystemID: "sourceSystemID_example", sourceSystemMediaKey: "sourceSystemMediaKey_example", sourceSystemName: "sourceSystemName_example", permission: ["permission_example"], className: "className_example", imageOf: "imageOf_example", imageSizeDescription: "imageSizeDescription_example", mediaCategory: "mediaCategory_example", mediaStatus: "mediaStatus_example", mediaType: "mediaType_example", resourceName: "resourceName_example", modificationTimestamp: Date(), mediaModificationTimestamp: Date(), updatedAt: Date(), createdAt: Date(), labels: [Label(id: 123, mediaId: 123, name: "name_example", classId: 123, confidence: 123, rank: 123, source: "source_example", updatedAt: Date(), createdAt: Date(), media: nil)], segmaps: [Segmap(id: 123, mediaId: 123, mask: "mask_example", area: 123, boundingBox: [123], hash: "hash_example", classId: 123, isCrowd: 123, segmentation: "segmentation_example", confidence: 123, source: "source_example", attributes: [Attribute(name: "name_example", confidence: 123, value: [123])], updatedAt: Date(), createdAt: Date(), media: nil)], trials: [Trial(id: 123, userId: 123, mediaId: 123, result: "result_example", updatedAt: Date(), createdAt: Date(), user: nil, media: nil)])], envisionAssets: [EnvisionAsset(id: 123, listingKey: "listingKey_example", aboundId: "aboundId_example", thumbnail: "thumbnail_example", topDown: "topDown_example", glb: "glb_example", obj: "obj_example", dump: "dump_example", tags: "TODO", updatedAt: Date(), createdAt: Date(), property: nil, landmarks: [Landmark(id: 123, envisionAssetId: 123, _class: "_class_example", confidence: 123, position: [123], updatedAt: Date(), createdAt: Date())])]))], prospectings: [Prospecting(id: 123, activeYN: false, bccMeYN: false, clientActivatedYN: false, conciergeNotificationsYN: false, conciergeYN: false, contactKeyNumeric: 123, ownerMemberKeyNumeric: 123, prospectingKeyNumeric: 123, savedSearchKeyNumeric: 123, bccEmailList: "bccEmailList_example", ccEmailList: "ccEmailList_example", contactKey: "contactKey_example", displayTemplateID: "displayTemplateID_example", messageNew: "messageNew_example", messageRevise: "messageRevise_example", messageUpdate: "messageUpdate_example", ownerMemberID: "ownerMemberID_example", ownerMemberKey: "ownerMemberKey_example", prospectingKey: "prospectingKey_example", savedSearchKey: "savedSearchKey_example", subject: "subject_example", toEmailList: "toEmailList_example", dailySchedule: ["dailySchedule_example"], languages: "languages_example", reasonActiveOrDisabled: "reasonActiveOrDisabled_example", scheduleType: "scheduleType_example", lastNewChangedTimestamp: Date(), lastViewedTimestamp: Date(), modificationTimestamp: Date(), nextSendTimestamp: Date(), contact: nil, ownerMember: nil, savedSearch: SavedSearch(id: 123, memberKeyNumeric: 123, savedSearchKeyNumeric: 123, memberKey: "memberKey_example", memberMlsId: "memberMlsId_example", originatingSystemID: 123, originatingSystemKey: "originatingSystemKey_example", originatingSystemMemberKey: "originatingSystemMemberKey_example", originatingSystemMemberName: "originatingSystemMemberName_example", originatingSystemName: "originatingSystemName_example", savedSearchDescription: "savedSearchDescription_example", savedSearchKey: "savedSearchKey_example", savedSearchName: "savedSearchName_example", searchQuery: "searchQuery_example", searchQueryExceptionDetails: "searchQueryExceptionDetails_example", searchQueryHumanReadable: "searchQueryHumanReadable_example", sourceSystemID: 123, sourceSystemKey: "sourceSystemKey_example", sourceSystemName: "sourceSystemName_example", className: "className_example", resourceName: "resourceName_example", savedSearchType: "savedSearchType_example", searchQueryExceptions: "searchQueryExceptions_example", searchQueryType: "searchQueryType_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), member: nil, originatingSystem: nil, sourceSystem: nil, prospectings: [nil]))])], sourceSystemContacts: [nil], originatingSystemHistoryTransactionals: [HistoryTransactional(id: 123, changedByMemberKeyNumeric: 123, fieldKeyNumeric: 123, historyTransactionalKeyNumeric: 123, resourceRecordKeyNumeric: 123, changedByMemberID: "changedByMemberID_example", changedByMemberKey: "changedByMemberKey_example", className: "className_example", fieldKey: "fieldKey_example", fieldName: "fieldName_example", historyTransactionalKey: "historyTransactionalKey_example", newValue: "newValue_example", originatingSystemHistoryKey: "originatingSystemHistoryKey_example", originatingSystemID: 123, originatingSystemName: "originatingSystemName_example", previousValue: "previousValue_example", resourceName: "resourceName_example", resourceRecordID: "resourceRecordID_example", resourceRecordKey: "resourceRecordKey_example", sourceSystemHistoryKey: "sourceSystemHistoryKey_example", sourceSystemID: 123, sourceSystemName: "sourceSystemName_example", changeType: "changeType_example", modificationTimestamp: Date(), changedByMember: nil, originatingSystem: nil, sourceSystem: nil)], sourceSystemHistoryTransactionals: [nil], actorOriginatingSystemInternetTrackings: [InternetTracking(id: 123, actorKeyNumeric: 123, actorLatitude: 123, actorLongitude: 123, colorDepth: 123, eventKeyNumeric: 123, objectKeyNumeric: 123, screenHeight: 123, screenWidth: 123, timeZoneOffset: 123, actorCity: "actorCity_example", actorEmail: "actorEmail_example", actorID: "actorID_example", actorIP: "actorIP_example", actorKey: "actorKey_example", actorOriginatingSystemID: 123, actorOriginatingSystemName: "actorOriginatingSystemName_example", actorPhone: "actorPhone_example", actorPhoneExt: "actorPhoneExt_example", actorPostalCode: "actorPostalCode_example", actorPostalCodePlus4: "actorPostalCodePlus4_example", actorRegion: "actorRegion_example", actorSourceSystemID: 123, actorSourceSystemName: "actorSourceSystemName_example", eventDescription: "eventDescription_example", eventKey: "eventKey_example", eventLabel: "eventLabel_example", eventOriginatingSystemID: 123, eventOriginatingSystemName: "eventOriginatingSystemName_example", eventSourceSystemID: 123, eventSourceSystemName: "eventSourceSystemName_example", objectID: "objectID_example", objectKey: "objectKey_example", objectOriginatingSystemID: 123, objectOriginatingSystemName: "objectOriginatingSystemName_example", objectSourceSystemID: 123, objectSourceSystemName: "objectSourceSystemName_example", objectURL: "objectURL_example", originatingSystemActorKey: "originatingSystemActorKey_example", originatingSystemEventKey: "originatingSystemEventKey_example", originatingSystemObjectKey: "originatingSystemObjectKey_example", referringURL: "referringURL_example", sessionID: "sessionID_example", sourceSystemActorKey: "sourceSystemActorKey_example", sourceSystemEventKey: "sourceSystemEventKey_example", sourceSystemObjectKey: "sourceSystemObjectKey_example", userAgent: "userAgent_example", actorStateOrProvince: "actorStateOrProvince_example", actorType: "actorType_example", deviceType: "deviceType_example", eventTarget: "eventTarget_example", eventType: "eventType_example", objectIdType: "objectIdType_example", objectType: "objectType_example", eventTimestamp: Date(), actorOriginatingSystem: nil, actorSourceSystem: nil, eventOriginatingSystem: nil, eventSourceSystem: nil, objectOriginatingSystem: nil, objectSourceSystem: nil)], actorSourceSystemInternetTrackings: [nil], eventOriginatingSystemInternetTrackings: [nil], eventSourceSystemInternetTrackings: [nil], objectOriginatingSystemInternetTrackings: [nil], objectSourceSystemInternetTrackings: [nil], originatingSystemMembers: [nil], sourceSystemMembers: [nil], originatingSystemOffices: [nil], sourceSystemOffices: [nil], originatingSystemOpenHouses: [nil], sourceSystemOpenHouses: [nil], originatingSystemProperties: [nil], sourceSystemProperties: [nil], originatingSystemQueues: [Queue(id: 123, queueTransactionKeyNumeric: 123, resourceRecordKeyNumeric: 123, originatingSystemID: 123, originatingSystemName: "originatingSystemName_example", originatingSystemQueueKey: "originatingSystemQueueKey_example", queueTransactionKey: "queueTransactionKey_example", resourceRecordID: "resourceRecordID_example", resourceRecordKey: "resourceRecordKey_example", sourceSystemID: "sourceSystemID_example", sourceSystemName: "sourceSystemName_example", sourceSystemQueueKey: "sourceSystemQueueKey_example", className: "className_example", queueTransactionType: "queueTransactionType_example", resourceName: "resourceName_example", modificationTimestamp: Date(), originatingSystem: nil, sourceSystem: nil)], sourceSystemQueues: [nil], originatingSystemRules: [Rules(id: 123, ruleEnabledYN: false, fieldKeyNumeric: 123, ruleKeyNumeric: 123, ruleOrder: 123, fieldKey: "fieldKey_example", fieldName: "fieldName_example", originatingSystemID: 123, originatingSystemName: "originatingSystemName_example", originatingSystemRuleKey: "originatingSystemRuleKey_example", ruleAction: "ruleAction_example", ruleDescription: "ruleDescription_example", ruleErrorText: "ruleErrorText_example", ruleExpression: "ruleExpression_example", ruleHelpText: "ruleHelpText_example", ruleKey: "ruleKey_example", ruleName: "ruleName_example", ruleVersion: "ruleVersion_example", ruleWarningText: "ruleWarningText_example", sourceSystemHistoryKey: "sourceSystemHistoryKey_example", sourceSystemID: "sourceSystemID_example", sourceSystemName: "sourceSystemName_example", className: "className_example", resourceName: "resourceName_example", ruleFormat: "ruleFormat_example", ruleType: "ruleType_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), originatingSystem: nil, sourceSystem: nil)], sourceSystemRules: [nil], originatingSystemSavedSearches: [nil], sourceSystemSavedSearches: [nil], agentOriginatingSystemShowings: [nil], agentSourceSystemShowings: [nil], listingOriginatingSystemShowings: [nil], listingSourceSystemShowings: [nil], showingOriginatingSystemShowings: [nil], showingSourceSystemShowings: [nil], originatingSystemTeamMembers: [TeamMembers(id: 123, memberKeyNumeric: 123, teamKeyNumeric: 123, teamMemberKeyNumeric: 123, memberKey: "memberKey_example", memberLoginId: "memberLoginId_example", memberMlsId: "memberMlsId_example", originatingSystemID: 123, originatingSystemKey: "originatingSystemKey_example", originatingSystemName: "originatingSystemName_example", sourceSystemID: 123, sourceSystemKey: "sourceSystemKey_example", sourceSystemName: "sourceSystemName_example", teamKey: "teamKey_example", teamMemberKey: "teamMemberKey_example", teamMemberNationalAssociationId: "teamMemberNationalAssociationId_example", teamMemberStateLicense: "teamMemberStateLicense_example", teamImpersonationLevel: "teamImpersonationLevel_example", teamMemberType: "teamMemberType_example", modificationTimestamp: Date(), originalEntryTimestamp: Date(), member: nil, originatingSystem: nil, sourceSystem: nil)], sourceSystemTeamMembers: [nil], originatingSystemTeams: [nil], sourceSystemTeams: [nil]), sourceSystem: nil, members: [nil], offices: [nil], buyerOfficeProperties: [nil], coBuyerOfficeProperties: [nil], coListOfficeProperties: [nil], listOfficeProperties: [nil]), originatingSystem: nil, sourceSystem: nil, contacts: [nil], historyTransactionals: [nil], oUIDs: [nil], brokerOffices: [nil], managerOffices: [nil], openHouses: [nil], buyerAgentProperties: [nil], coBuyerAgentProperties: [nil], coListAgentProperties: [nil], listAgentProperties: [nil], prospectings: [nil], savedSearches: [nil], showings: [nil], teamMembers: [nil], teams: [nil]), publisher: Publisher(id: 123, userId: 123, name: "name_example", website: "website_example", logo: "logo_example", active: false, createdAt: Date(), updatedAt: Date(), user: nil, syndications: [Syndication(id: 123, memberId: 123, officeId: 123, publisherId: 123, permission: "permission_example", createdAt: Date(), updatedAt: Date(), member: nil, office: nil, publisher: nil)]), shopper: Shopper(id: 123, userId: 123, memberId: 123, username: "username_example", price: 123, priceVerified: false, locationVerified: false, tutorialStep: 123, searchRadius: 123, searchCity: "searchCity_example", filterLocation: false, searchHistory: "searchHistory_example", imageFilter: "imageFilter_example", createdAt: Date(), updatedAt: Date(), user: nil, member: nil), usages: [ApiUsage(id: 123, userId: 123, year: 123, month: 123, credits: 123, requests: 123, trial: false, updatedAt: Date(), createdAt: Date(), user: nil)], favorites: [nil], shareReceivers: [nil], shareSenders: [nil], transactions: [nil], trials: [nil]), sender: nil, property: nil) // Share | 

// Post Shares
ShareAPI.postShares(share: share) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **share** | [**Share**](Share.md) |  | 

### Return type

[**Share**](Share.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

