# Score

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | **Double** |  | [optional] 
**reasons** | [DeltaReason] |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


