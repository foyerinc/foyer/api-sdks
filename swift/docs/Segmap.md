# Segmap

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**mediaId** | **Int** |  | [optional] 
**mask** | **String** |  | [optional] 
**area** | **Int** |  | [optional] 
**boundingBox** | **[Int]** |  | [optional] 
**hash** | **String** |  | [optional] 
**classId** | **Int** |  | [optional] 
**isCrowd** | **Int** |  | [optional] 
**segmentation** | **String** |  | [optional] 
**confidence** | **Float** |  | [optional] 
**source** | **String** |  | [optional] 
**attributes** | [Attribute] |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**media** | [**Media**](Media.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


