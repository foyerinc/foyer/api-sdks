# OpenHouse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**appointmentRequiredYN** | **Bool** |  | [optional] 
**openHouseDate** | **Date** |  | [optional] 
**listingKeyNumeric** | **Double** |  | [optional] 
**openHouseKeyNumeric** | **Double** |  | [optional] 
**showingAgentKeyNumeric** | **Double** |  | [optional] 
**listingId** | **String** |  | [optional] 
**listingKey** | **String** |  | [optional] 
**openHouseId** | **String** |  | [optional] 
**openHouseKey** | **String** |  | [optional] 
**openHouseRemarks** | **String** |  | [optional] 
**originatingSystemID** | **Double** |  | [optional] 
**originatingSystemKey** | **String** |  | [optional] 
**originatingSystemName** | **String** |  | [optional] 
**refreshments** | **String** |  | [optional] 
**showingAgentFirstName** | **String** |  | [optional] 
**showingAgentKey** | **String** |  | [optional] 
**showingAgentLastName** | **String** |  | [optional] 
**showingAgentMlsID** | **String** |  | [optional] 
**sourceSystemID** | **Double** |  | [optional] 
**sourceSystemKey** | **String** |  | [optional] 
**sourceSystemName** | **String** |  | [optional] 
**openHouseAttendedBy** | **String** |  | [optional] 
**openHouseStatus** | **String** |  | [optional] 
**openHouseType** | **String** |  | [optional] 
**modificationTimestamp** | **Date** |  | [optional] 
**openHouseEndTime** | **Date** |  | [optional] 
**openHouseStartTime** | **Date** |  | [optional] 
**originalEntryTimestamp** | **Date** |  | [optional] 
**showingAgent** | [**Member**](Member.md) |  | [optional] 
**originatingSystem** | [**OUID**](OUID.md) |  | [optional] 
**sourceSystem** | [**OUID**](OUID.md) |  | [optional] 
**property** | [**Property**](Property.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


