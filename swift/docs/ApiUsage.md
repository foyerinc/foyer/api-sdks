# ApiUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Int** |  | [optional] 
**userId** | **Int** |  | [optional] 
**year** | **Int** |  | [optional] 
**month** | **Int** |  | [optional] 
**credits** | **Int** |  | [optional] 
**requests** | **Int** |  | [optional] 
**trial** | **Bool** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


