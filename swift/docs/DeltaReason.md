# DeltaReason

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **String** |  | [optional] 
**problem** | **String** |  | [optional] 
**recommendation** | **String** |  | [optional] 
**modifier** | **Double** |  | [optional] 
**passed** | **Bool** |  | [optional] 
**url** | **String** |  | [optional] 
**mediaKey** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


