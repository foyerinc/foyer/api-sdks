# EnvisionAssetAPI

All URIs are relative to *https://api.foyer.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getModelPresignedUrl**](EnvisionAssetAPI.md#getmodelpresignedurl) | **GET** /EnvisionAsset/{id}/modelPresignedUrl | get presigned url of the model


# **getModelPresignedUrl**
```swift
    open class func getModelPresignedUrl(id: Int, completion: @escaping (_ data: String?, _ error: Error?) -> Void)
```

get presigned url of the model

Get presigned url of the model corresponding to the envision asset

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 

// get presigned url of the model
EnvisionAssetAPI.getModelPresignedUrl(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 

### Return type

**String**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

