# OfficeAPI

All URIs are relative to *https://api.foyer.ai*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteOfficeById**](OfficeAPI.md#deleteofficebyid) | **DELETE** /Office/{id} | Delete Office by Id
[**findOneOffice**](OfficeAPI.md#findoneoffice) | **GET** /Office/findOne | Find One Office
[**getOfficeById**](OfficeAPI.md#getofficebyid) | **GET** /Office/{id} | Get Office by Id
[**patchOfficeById**](OfficeAPI.md#patchofficebyid) | **PATCH** /Office/{id} | Patch Office by Id


# **deleteOfficeById**
```swift
    open class func deleteOfficeById(id: Int, completion: @escaping (_ data: Void?, _ error: Error?) -> Void)
```

Delete Office by Id

Delete a specific Office with unique id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 

// Delete Office by Id
OfficeAPI.deleteOfficeById(id: id) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 

### Return type

Void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findOneOffice**
```swift
    open class func findOneOffice(filter: String, completion: @escaping (_ data: Office?, _ error: Error?) -> Void)
```

Find One Office

Fine one Office according to defined filter criteria

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let filter = "filter_example" // String | 

// Find One Office
OfficeAPI.findOneOffice(filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String** |  | 

### Return type

[**Office**](Office.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOfficeById**
```swift
    open class func getOfficeById(id: Int, filter: String? = nil, completion: @escaping (_ data: Office?, _ error: Error?) -> Void)
```

Get Office by Id

Get a specific Office with unique id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let filter = "filter_example" // String |  (optional)

// Get Office by Id
OfficeAPI.getOfficeById(id: id, filter: filter) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **filter** | **String** |  | [optional] 

### Return type

[**Office**](Office.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **patchOfficeById**
```swift
    open class func patchOfficeById(id: Int, patchDocument: [PatchDocument]? = nil, completion: @escaping (_ data: Office?, _ error: Error?) -> Void)
```

Patch Office by Id

Patch a specific Office with unique id

### Example
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import OpenAPIClient

let id = 987 // Int | 
let patchDocument = [PatchDocument(op: "op_example", path: "path_example", value: "value_example", from: "from_example")] // [PatchDocument] |  (optional)

// Patch Office by Id
OfficeAPI.patchOfficeById(id: id, patchDocument: patchDocument) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Int** |  | 
 **patchDocument** | [**[PatchDocument]**](PatchDocument.md) |  | [optional] 

### Return type

[**Office**](Office.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

