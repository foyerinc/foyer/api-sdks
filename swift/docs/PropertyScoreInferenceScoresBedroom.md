# PropertyScoreInferenceScoresBedroom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spaciousness** | [**Score**](Score.md) |  | [optional] 
**trendiness** | [**Score**](Score.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


