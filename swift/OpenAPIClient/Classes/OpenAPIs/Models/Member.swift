//
// Member.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct Member: Codable, JSONEncodable, Hashable {

    public var id: Int?
    public var memberMlsAccessYN: Bool?
    public var memberAORkeyNumeric: Double?
    public var memberKeyNumeric: Double?
    public var officeKeyNumeric: Double?
    public var jobTitle: String?
    public var memberAORMlsId: String?
    public var memberAORkey: String?
    public var memberAddress1: String?
    public var memberAddress2: String?
    public var memberAssociationComments: String?
    public var memberCarrierRoute: String?
    public var memberCity: String?
    public var memberDirectPhone: String?
    public var memberEmail: String?
    public var memberFax: String?
    public var memberFirstName: String?
    public var memberFullName: String?
    public var memberHomePhone: String?
    public var memberIsAssistantTo: String?
    public var memberKey: String?
    public var memberLastName: String?
    public var memberLoginId: String?
    public var memberMiddleName: String?
    public var memberMlsId: String?
    public var memberMobilePhone: String?
    public var memberNamePrefix: String?
    public var memberNameSuffix: String?
    public var memberNationalAssociationId: String?
    public var memberNickname: String?
    public var memberOfficePhone: String?
    public var memberOfficePhoneExt: String?
    public var memberPager: String?
    public var memberPassword: String?
    public var memberPhoneTTYTDD: String?
    public var memberPostalCode: String?
    public var memberPostalCodePlus4: String?
    public var memberPreferredPhone: String?
    public var memberPreferredPhoneExt: String?
    public var memberStateLicense: String?
    public var memberTollFreePhone: String?
    public var memberVoiceMail: String?
    public var memberVoiceMailExt: String?
    public var officeKey: String?
    public var officeMlsId: String?
    public var officeName: String?
    public var originatingSystemID: String?
    public var originatingSystemMemberKey: String?
    public var originatingSystemName: String?
    public var sourceSystemID: String?
    public var sourceSystemMemberKey: String?
    public var sourceSystemName: String?
    public var memberDesignation: [String]?
    public var memberLanguages: [String]?
    public var syndicateTo: [String]?
    public var memberAOR: String?
    public var memberCountry: String?
    public var memberCountyOrParish: String?
    public var memberMlsSecurityClass: String?
    public var memberOtherPhoneType: String?
    public var memberStateLicenseState: String?
    public var memberStateOrProvince: String?
    public var memberStatus: String?
    public var memberType: String?
    public var socialMediaType: String?
    public var lastLoginTimestamp: Date?
    public var modificationTimestamp: Date?
    public var originalEntryTimestamp: Date?
    public var office: Office?
    public var originatingSystem: OUID?
    public var sourceSystem: OUID?
    public var contacts: [Contacts]?
    public var historyTransactionals: [HistoryTransactional]?
    public var oUIDs: [OUID]?
    public var brokerOffices: [Office]?
    public var managerOffices: [Office]?
    public var openHouses: [OpenHouse]?
    public var buyerAgentProperties: [Property]?
    public var coBuyerAgentProperties: [Property]?
    public var coListAgentProperties: [Property]?
    public var listAgentProperties: [Property]?
    public var prospectings: [Prospecting]?
    public var savedSearches: [SavedSearch]?
    public var showings: [Showing]?
    public var teamMembers: [TeamMembers]?
    public var teams: [Teams]?

    public init(id: Int? = nil, memberMlsAccessYN: Bool? = nil, memberAORkeyNumeric: Double? = nil, memberKeyNumeric: Double? = nil, officeKeyNumeric: Double? = nil, jobTitle: String? = nil, memberAORMlsId: String? = nil, memberAORkey: String? = nil, memberAddress1: String? = nil, memberAddress2: String? = nil, memberAssociationComments: String? = nil, memberCarrierRoute: String? = nil, memberCity: String? = nil, memberDirectPhone: String? = nil, memberEmail: String? = nil, memberFax: String? = nil, memberFirstName: String? = nil, memberFullName: String? = nil, memberHomePhone: String? = nil, memberIsAssistantTo: String? = nil, memberKey: String? = nil, memberLastName: String? = nil, memberLoginId: String? = nil, memberMiddleName: String? = nil, memberMlsId: String? = nil, memberMobilePhone: String? = nil, memberNamePrefix: String? = nil, memberNameSuffix: String? = nil, memberNationalAssociationId: String? = nil, memberNickname: String? = nil, memberOfficePhone: String? = nil, memberOfficePhoneExt: String? = nil, memberPager: String? = nil, memberPassword: String? = nil, memberPhoneTTYTDD: String? = nil, memberPostalCode: String? = nil, memberPostalCodePlus4: String? = nil, memberPreferredPhone: String? = nil, memberPreferredPhoneExt: String? = nil, memberStateLicense: String? = nil, memberTollFreePhone: String? = nil, memberVoiceMail: String? = nil, memberVoiceMailExt: String? = nil, officeKey: String? = nil, officeMlsId: String? = nil, officeName: String? = nil, originatingSystemID: String? = nil, originatingSystemMemberKey: String? = nil, originatingSystemName: String? = nil, sourceSystemID: String? = nil, sourceSystemMemberKey: String? = nil, sourceSystemName: String? = nil, memberDesignation: [String]? = nil, memberLanguages: [String]? = nil, syndicateTo: [String]? = nil, memberAOR: String? = nil, memberCountry: String? = nil, memberCountyOrParish: String? = nil, memberMlsSecurityClass: String? = nil, memberOtherPhoneType: String? = nil, memberStateLicenseState: String? = nil, memberStateOrProvince: String? = nil, memberStatus: String? = nil, memberType: String? = nil, socialMediaType: String? = nil, lastLoginTimestamp: Date? = nil, modificationTimestamp: Date? = nil, originalEntryTimestamp: Date? = nil, office: Office? = nil, originatingSystem: OUID? = nil, sourceSystem: OUID? = nil, contacts: [Contacts]? = nil, historyTransactionals: [HistoryTransactional]? = nil, oUIDs: [OUID]? = nil, brokerOffices: [Office]? = nil, managerOffices: [Office]? = nil, openHouses: [OpenHouse]? = nil, buyerAgentProperties: [Property]? = nil, coBuyerAgentProperties: [Property]? = nil, coListAgentProperties: [Property]? = nil, listAgentProperties: [Property]? = nil, prospectings: [Prospecting]? = nil, savedSearches: [SavedSearch]? = nil, showings: [Showing]? = nil, teamMembers: [TeamMembers]? = nil, teams: [Teams]? = nil) {
        self.id = id
        self.memberMlsAccessYN = memberMlsAccessYN
        self.memberAORkeyNumeric = memberAORkeyNumeric
        self.memberKeyNumeric = memberKeyNumeric
        self.officeKeyNumeric = officeKeyNumeric
        self.jobTitle = jobTitle
        self.memberAORMlsId = memberAORMlsId
        self.memberAORkey = memberAORkey
        self.memberAddress1 = memberAddress1
        self.memberAddress2 = memberAddress2
        self.memberAssociationComments = memberAssociationComments
        self.memberCarrierRoute = memberCarrierRoute
        self.memberCity = memberCity
        self.memberDirectPhone = memberDirectPhone
        self.memberEmail = memberEmail
        self.memberFax = memberFax
        self.memberFirstName = memberFirstName
        self.memberFullName = memberFullName
        self.memberHomePhone = memberHomePhone
        self.memberIsAssistantTo = memberIsAssistantTo
        self.memberKey = memberKey
        self.memberLastName = memberLastName
        self.memberLoginId = memberLoginId
        self.memberMiddleName = memberMiddleName
        self.memberMlsId = memberMlsId
        self.memberMobilePhone = memberMobilePhone
        self.memberNamePrefix = memberNamePrefix
        self.memberNameSuffix = memberNameSuffix
        self.memberNationalAssociationId = memberNationalAssociationId
        self.memberNickname = memberNickname
        self.memberOfficePhone = memberOfficePhone
        self.memberOfficePhoneExt = memberOfficePhoneExt
        self.memberPager = memberPager
        self.memberPassword = memberPassword
        self.memberPhoneTTYTDD = memberPhoneTTYTDD
        self.memberPostalCode = memberPostalCode
        self.memberPostalCodePlus4 = memberPostalCodePlus4
        self.memberPreferredPhone = memberPreferredPhone
        self.memberPreferredPhoneExt = memberPreferredPhoneExt
        self.memberStateLicense = memberStateLicense
        self.memberTollFreePhone = memberTollFreePhone
        self.memberVoiceMail = memberVoiceMail
        self.memberVoiceMailExt = memberVoiceMailExt
        self.officeKey = officeKey
        self.officeMlsId = officeMlsId
        self.officeName = officeName
        self.originatingSystemID = originatingSystemID
        self.originatingSystemMemberKey = originatingSystemMemberKey
        self.originatingSystemName = originatingSystemName
        self.sourceSystemID = sourceSystemID
        self.sourceSystemMemberKey = sourceSystemMemberKey
        self.sourceSystemName = sourceSystemName
        self.memberDesignation = memberDesignation
        self.memberLanguages = memberLanguages
        self.syndicateTo = syndicateTo
        self.memberAOR = memberAOR
        self.memberCountry = memberCountry
        self.memberCountyOrParish = memberCountyOrParish
        self.memberMlsSecurityClass = memberMlsSecurityClass
        self.memberOtherPhoneType = memberOtherPhoneType
        self.memberStateLicenseState = memberStateLicenseState
        self.memberStateOrProvince = memberStateOrProvince
        self.memberStatus = memberStatus
        self.memberType = memberType
        self.socialMediaType = socialMediaType
        self.lastLoginTimestamp = lastLoginTimestamp
        self.modificationTimestamp = modificationTimestamp
        self.originalEntryTimestamp = originalEntryTimestamp
        self.office = office
        self.originatingSystem = originatingSystem
        self.sourceSystem = sourceSystem
        self.contacts = contacts
        self.historyTransactionals = historyTransactionals
        self.oUIDs = oUIDs
        self.brokerOffices = brokerOffices
        self.managerOffices = managerOffices
        self.openHouses = openHouses
        self.buyerAgentProperties = buyerAgentProperties
        self.coBuyerAgentProperties = coBuyerAgentProperties
        self.coListAgentProperties = coListAgentProperties
        self.listAgentProperties = listAgentProperties
        self.prospectings = prospectings
        self.savedSearches = savedSearches
        self.showings = showings
        self.teamMembers = teamMembers
        self.teams = teams
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case memberMlsAccessYN = "MemberMlsAccessYN"
        case memberAORkeyNumeric = "MemberAORkeyNumeric"
        case memberKeyNumeric = "MemberKeyNumeric"
        case officeKeyNumeric = "OfficeKeyNumeric"
        case jobTitle = "JobTitle"
        case memberAORMlsId = "MemberAORMlsId"
        case memberAORkey = "MemberAORkey"
        case memberAddress1 = "MemberAddress1"
        case memberAddress2 = "MemberAddress2"
        case memberAssociationComments = "MemberAssociationComments"
        case memberCarrierRoute = "MemberCarrierRoute"
        case memberCity = "MemberCity"
        case memberDirectPhone = "MemberDirectPhone"
        case memberEmail = "MemberEmail"
        case memberFax = "MemberFax"
        case memberFirstName = "MemberFirstName"
        case memberFullName = "MemberFullName"
        case memberHomePhone = "MemberHomePhone"
        case memberIsAssistantTo = "MemberIsAssistantTo"
        case memberKey = "MemberKey"
        case memberLastName = "MemberLastName"
        case memberLoginId = "MemberLoginId"
        case memberMiddleName = "MemberMiddleName"
        case memberMlsId = "MemberMlsId"
        case memberMobilePhone = "MemberMobilePhone"
        case memberNamePrefix = "MemberNamePrefix"
        case memberNameSuffix = "MemberNameSuffix"
        case memberNationalAssociationId = "MemberNationalAssociationId"
        case memberNickname = "MemberNickname"
        case memberOfficePhone = "MemberOfficePhone"
        case memberOfficePhoneExt = "MemberOfficePhoneExt"
        case memberPager = "MemberPager"
        case memberPassword = "MemberPassword"
        case memberPhoneTTYTDD = "MemberPhoneTTYTDD"
        case memberPostalCode = "MemberPostalCode"
        case memberPostalCodePlus4 = "MemberPostalCodePlus4"
        case memberPreferredPhone = "MemberPreferredPhone"
        case memberPreferredPhoneExt = "MemberPreferredPhoneExt"
        case memberStateLicense = "MemberStateLicense"
        case memberTollFreePhone = "MemberTollFreePhone"
        case memberVoiceMail = "MemberVoiceMail"
        case memberVoiceMailExt = "MemberVoiceMailExt"
        case officeKey = "OfficeKey"
        case officeMlsId = "OfficeMlsId"
        case officeName = "OfficeName"
        case originatingSystemID = "OriginatingSystemID"
        case originatingSystemMemberKey = "OriginatingSystemMemberKey"
        case originatingSystemName = "OriginatingSystemName"
        case sourceSystemID = "SourceSystemID"
        case sourceSystemMemberKey = "SourceSystemMemberKey"
        case sourceSystemName = "SourceSystemName"
        case memberDesignation = "MemberDesignation"
        case memberLanguages = "MemberLanguages"
        case syndicateTo = "SyndicateTo"
        case memberAOR = "MemberAOR"
        case memberCountry = "MemberCountry"
        case memberCountyOrParish = "MemberCountyOrParish"
        case memberMlsSecurityClass = "MemberMlsSecurityClass"
        case memberOtherPhoneType = "MemberOtherPhoneType"
        case memberStateLicenseState = "MemberStateLicenseState"
        case memberStateOrProvince = "MemberStateOrProvince"
        case memberStatus = "MemberStatus"
        case memberType = "MemberType"
        case socialMediaType = "SocialMediaType"
        case lastLoginTimestamp = "LastLoginTimestamp"
        case modificationTimestamp = "ModificationTimestamp"
        case originalEntryTimestamp = "OriginalEntryTimestamp"
        case office = "Office"
        case originatingSystem = "OriginatingSystem"
        case sourceSystem = "SourceSystem"
        case contacts = "Contacts"
        case historyTransactionals = "HistoryTransactionals"
        case oUIDs = "OUIDs"
        case brokerOffices = "BrokerOffices"
        case managerOffices = "ManagerOffices"
        case openHouses = "OpenHouses"
        case buyerAgentProperties = "BuyerAgentProperties"
        case coBuyerAgentProperties = "CoBuyerAgentProperties"
        case coListAgentProperties = "CoListAgentProperties"
        case listAgentProperties = "ListAgentProperties"
        case prospectings = "Prospectings"
        case savedSearches = "SavedSearches"
        case showings = "Showings"
        case teamMembers = "TeamMembers"
        case teams = "Teams"
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(memberMlsAccessYN, forKey: .memberMlsAccessYN)
        try container.encodeIfPresent(memberAORkeyNumeric, forKey: .memberAORkeyNumeric)
        try container.encodeIfPresent(memberKeyNumeric, forKey: .memberKeyNumeric)
        try container.encodeIfPresent(officeKeyNumeric, forKey: .officeKeyNumeric)
        try container.encodeIfPresent(jobTitle, forKey: .jobTitle)
        try container.encodeIfPresent(memberAORMlsId, forKey: .memberAORMlsId)
        try container.encodeIfPresent(memberAORkey, forKey: .memberAORkey)
        try container.encodeIfPresent(memberAddress1, forKey: .memberAddress1)
        try container.encodeIfPresent(memberAddress2, forKey: .memberAddress2)
        try container.encodeIfPresent(memberAssociationComments, forKey: .memberAssociationComments)
        try container.encodeIfPresent(memberCarrierRoute, forKey: .memberCarrierRoute)
        try container.encodeIfPresent(memberCity, forKey: .memberCity)
        try container.encodeIfPresent(memberDirectPhone, forKey: .memberDirectPhone)
        try container.encodeIfPresent(memberEmail, forKey: .memberEmail)
        try container.encodeIfPresent(memberFax, forKey: .memberFax)
        try container.encodeIfPresent(memberFirstName, forKey: .memberFirstName)
        try container.encodeIfPresent(memberFullName, forKey: .memberFullName)
        try container.encodeIfPresent(memberHomePhone, forKey: .memberHomePhone)
        try container.encodeIfPresent(memberIsAssistantTo, forKey: .memberIsAssistantTo)
        try container.encodeIfPresent(memberKey, forKey: .memberKey)
        try container.encodeIfPresent(memberLastName, forKey: .memberLastName)
        try container.encodeIfPresent(memberLoginId, forKey: .memberLoginId)
        try container.encodeIfPresent(memberMiddleName, forKey: .memberMiddleName)
        try container.encodeIfPresent(memberMlsId, forKey: .memberMlsId)
        try container.encodeIfPresent(memberMobilePhone, forKey: .memberMobilePhone)
        try container.encodeIfPresent(memberNamePrefix, forKey: .memberNamePrefix)
        try container.encodeIfPresent(memberNameSuffix, forKey: .memberNameSuffix)
        try container.encodeIfPresent(memberNationalAssociationId, forKey: .memberNationalAssociationId)
        try container.encodeIfPresent(memberNickname, forKey: .memberNickname)
        try container.encodeIfPresent(memberOfficePhone, forKey: .memberOfficePhone)
        try container.encodeIfPresent(memberOfficePhoneExt, forKey: .memberOfficePhoneExt)
        try container.encodeIfPresent(memberPager, forKey: .memberPager)
        try container.encodeIfPresent(memberPassword, forKey: .memberPassword)
        try container.encodeIfPresent(memberPhoneTTYTDD, forKey: .memberPhoneTTYTDD)
        try container.encodeIfPresent(memberPostalCode, forKey: .memberPostalCode)
        try container.encodeIfPresent(memberPostalCodePlus4, forKey: .memberPostalCodePlus4)
        try container.encodeIfPresent(memberPreferredPhone, forKey: .memberPreferredPhone)
        try container.encodeIfPresent(memberPreferredPhoneExt, forKey: .memberPreferredPhoneExt)
        try container.encodeIfPresent(memberStateLicense, forKey: .memberStateLicense)
        try container.encodeIfPresent(memberTollFreePhone, forKey: .memberTollFreePhone)
        try container.encodeIfPresent(memberVoiceMail, forKey: .memberVoiceMail)
        try container.encodeIfPresent(memberVoiceMailExt, forKey: .memberVoiceMailExt)
        try container.encodeIfPresent(officeKey, forKey: .officeKey)
        try container.encodeIfPresent(officeMlsId, forKey: .officeMlsId)
        try container.encodeIfPresent(officeName, forKey: .officeName)
        try container.encodeIfPresent(originatingSystemID, forKey: .originatingSystemID)
        try container.encodeIfPresent(originatingSystemMemberKey, forKey: .originatingSystemMemberKey)
        try container.encodeIfPresent(originatingSystemName, forKey: .originatingSystemName)
        try container.encodeIfPresent(sourceSystemID, forKey: .sourceSystemID)
        try container.encodeIfPresent(sourceSystemMemberKey, forKey: .sourceSystemMemberKey)
        try container.encodeIfPresent(sourceSystemName, forKey: .sourceSystemName)
        try container.encodeIfPresent(memberDesignation, forKey: .memberDesignation)
        try container.encodeIfPresent(memberLanguages, forKey: .memberLanguages)
        try container.encodeIfPresent(syndicateTo, forKey: .syndicateTo)
        try container.encodeIfPresent(memberAOR, forKey: .memberAOR)
        try container.encodeIfPresent(memberCountry, forKey: .memberCountry)
        try container.encodeIfPresent(memberCountyOrParish, forKey: .memberCountyOrParish)
        try container.encodeIfPresent(memberMlsSecurityClass, forKey: .memberMlsSecurityClass)
        try container.encodeIfPresent(memberOtherPhoneType, forKey: .memberOtherPhoneType)
        try container.encodeIfPresent(memberStateLicenseState, forKey: .memberStateLicenseState)
        try container.encodeIfPresent(memberStateOrProvince, forKey: .memberStateOrProvince)
        try container.encodeIfPresent(memberStatus, forKey: .memberStatus)
        try container.encodeIfPresent(memberType, forKey: .memberType)
        try container.encodeIfPresent(socialMediaType, forKey: .socialMediaType)
        try container.encodeIfPresent(lastLoginTimestamp, forKey: .lastLoginTimestamp)
        try container.encodeIfPresent(modificationTimestamp, forKey: .modificationTimestamp)
        try container.encodeIfPresent(originalEntryTimestamp, forKey: .originalEntryTimestamp)
        try container.encodeIfPresent(office, forKey: .office)
        try container.encodeIfPresent(originatingSystem, forKey: .originatingSystem)
        try container.encodeIfPresent(sourceSystem, forKey: .sourceSystem)
        try container.encodeIfPresent(contacts, forKey: .contacts)
        try container.encodeIfPresent(historyTransactionals, forKey: .historyTransactionals)
        try container.encodeIfPresent(oUIDs, forKey: .oUIDs)
        try container.encodeIfPresent(brokerOffices, forKey: .brokerOffices)
        try container.encodeIfPresent(managerOffices, forKey: .managerOffices)
        try container.encodeIfPresent(openHouses, forKey: .openHouses)
        try container.encodeIfPresent(buyerAgentProperties, forKey: .buyerAgentProperties)
        try container.encodeIfPresent(coBuyerAgentProperties, forKey: .coBuyerAgentProperties)
        try container.encodeIfPresent(coListAgentProperties, forKey: .coListAgentProperties)
        try container.encodeIfPresent(listAgentProperties, forKey: .listAgentProperties)
        try container.encodeIfPresent(prospectings, forKey: .prospectings)
        try container.encodeIfPresent(savedSearches, forKey: .savedSearches)
        try container.encodeIfPresent(showings, forKey: .showings)
        try container.encodeIfPresent(teamMembers, forKey: .teamMembers)
        try container.encodeIfPresent(teams, forKey: .teams)
    }
}

