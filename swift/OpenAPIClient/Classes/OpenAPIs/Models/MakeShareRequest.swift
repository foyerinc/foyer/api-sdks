//
// MakeShareRequest.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct MakeShareRequest: Codable, JSONEncodable, Hashable {

    public var propertyId: Int?
    public var phoneNumber: String?
    public var firstName: String?
    public var lastName: String?

    public init(propertyId: Int? = nil, phoneNumber: String? = nil, firstName: String? = nil, lastName: String? = nil) {
        self.propertyId = propertyId
        self.phoneNumber = phoneNumber
        self.firstName = firstName
        self.lastName = lastName
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case propertyId
        case phoneNumber
        case firstName
        case lastName
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(propertyId, forKey: .propertyId)
        try container.encodeIfPresent(phoneNumber, forKey: .phoneNumber)
        try container.encodeIfPresent(firstName, forKey: .firstName)
        try container.encodeIfPresent(lastName, forKey: .lastName)
    }
}

