//
// Showing.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct Showing: Codable, JSONEncodable, Hashable {

    public var id: Int?
    public var listingKeyNumeric: Double?
    public var showingAgentKeyNumeric: Double?
    public var showingKeyNumeric: Double?
    public var agentOriginatingSystemID: String?
    public var agentOriginatingSystemName: String?
    public var agentSourceSystemID: String?
    public var agentSourceSystemName: String?
    public var listingId: String?
    public var listingKey: String?
    public var listingOriginatingSystemID: String?
    public var listingOriginatingSystemName: String?
    public var listingSourceSystemID: Double?
    public var listingSourceSystemName: String?
    public var originatingSystemAgentKey: String?
    public var originatingSystemListingKey: String?
    public var originatingSystemShowingKey: String?
    public var showingAgentKey: String?
    public var showingAgentMlsID: String?
    public var showingId: String?
    public var showingKey: String?
    public var showingOriginatingSystemID: String?
    public var showingOriginatingSystemName: String?
    public var showingSourceSystemID: Double?
    public var showingSourceSystemName: String?
    public var sourceSystemAgentKey: String?
    public var sourceSystemListingKey: String?
    public var sourceSystemShowingKey: String?
    public var modificationTimestamp: Date?
    public var originalEntryTimestamp: Date?
    public var showingEndTimestamp: Date?
    public var showingRequestedTimestamp: Date?
    public var showingStartTimestamp: Date?
    public var showingAgent: Member?
    public var agentOriginatingSystem: OUID?
    public var agentSourceSystem: OUID?
    public var listingOriginatingSystem: OUID?
    public var listingSourceSystem: OUID?
    public var showingOriginatingSystem: OUID?
    public var showingSourceSystem: OUID?
    public var property: Property?

    public init(id: Int? = nil, listingKeyNumeric: Double? = nil, showingAgentKeyNumeric: Double? = nil, showingKeyNumeric: Double? = nil, agentOriginatingSystemID: String? = nil, agentOriginatingSystemName: String? = nil, agentSourceSystemID: String? = nil, agentSourceSystemName: String? = nil, listingId: String? = nil, listingKey: String? = nil, listingOriginatingSystemID: String? = nil, listingOriginatingSystemName: String? = nil, listingSourceSystemID: Double? = nil, listingSourceSystemName: String? = nil, originatingSystemAgentKey: String? = nil, originatingSystemListingKey: String? = nil, originatingSystemShowingKey: String? = nil, showingAgentKey: String? = nil, showingAgentMlsID: String? = nil, showingId: String? = nil, showingKey: String? = nil, showingOriginatingSystemID: String? = nil, showingOriginatingSystemName: String? = nil, showingSourceSystemID: Double? = nil, showingSourceSystemName: String? = nil, sourceSystemAgentKey: String? = nil, sourceSystemListingKey: String? = nil, sourceSystemShowingKey: String? = nil, modificationTimestamp: Date? = nil, originalEntryTimestamp: Date? = nil, showingEndTimestamp: Date? = nil, showingRequestedTimestamp: Date? = nil, showingStartTimestamp: Date? = nil, showingAgent: Member? = nil, agentOriginatingSystem: OUID? = nil, agentSourceSystem: OUID? = nil, listingOriginatingSystem: OUID? = nil, listingSourceSystem: OUID? = nil, showingOriginatingSystem: OUID? = nil, showingSourceSystem: OUID? = nil, property: Property? = nil) {
        self.id = id
        self.listingKeyNumeric = listingKeyNumeric
        self.showingAgentKeyNumeric = showingAgentKeyNumeric
        self.showingKeyNumeric = showingKeyNumeric
        self.agentOriginatingSystemID = agentOriginatingSystemID
        self.agentOriginatingSystemName = agentOriginatingSystemName
        self.agentSourceSystemID = agentSourceSystemID
        self.agentSourceSystemName = agentSourceSystemName
        self.listingId = listingId
        self.listingKey = listingKey
        self.listingOriginatingSystemID = listingOriginatingSystemID
        self.listingOriginatingSystemName = listingOriginatingSystemName
        self.listingSourceSystemID = listingSourceSystemID
        self.listingSourceSystemName = listingSourceSystemName
        self.originatingSystemAgentKey = originatingSystemAgentKey
        self.originatingSystemListingKey = originatingSystemListingKey
        self.originatingSystemShowingKey = originatingSystemShowingKey
        self.showingAgentKey = showingAgentKey
        self.showingAgentMlsID = showingAgentMlsID
        self.showingId = showingId
        self.showingKey = showingKey
        self.showingOriginatingSystemID = showingOriginatingSystemID
        self.showingOriginatingSystemName = showingOriginatingSystemName
        self.showingSourceSystemID = showingSourceSystemID
        self.showingSourceSystemName = showingSourceSystemName
        self.sourceSystemAgentKey = sourceSystemAgentKey
        self.sourceSystemListingKey = sourceSystemListingKey
        self.sourceSystemShowingKey = sourceSystemShowingKey
        self.modificationTimestamp = modificationTimestamp
        self.originalEntryTimestamp = originalEntryTimestamp
        self.showingEndTimestamp = showingEndTimestamp
        self.showingRequestedTimestamp = showingRequestedTimestamp
        self.showingStartTimestamp = showingStartTimestamp
        self.showingAgent = showingAgent
        self.agentOriginatingSystem = agentOriginatingSystem
        self.agentSourceSystem = agentSourceSystem
        self.listingOriginatingSystem = listingOriginatingSystem
        self.listingSourceSystem = listingSourceSystem
        self.showingOriginatingSystem = showingOriginatingSystem
        self.showingSourceSystem = showingSourceSystem
        self.property = property
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case id
        case listingKeyNumeric = "ListingKeyNumeric"
        case showingAgentKeyNumeric = "ShowingAgentKeyNumeric"
        case showingKeyNumeric = "ShowingKeyNumeric"
        case agentOriginatingSystemID = "AgentOriginatingSystemID"
        case agentOriginatingSystemName = "AgentOriginatingSystemName"
        case agentSourceSystemID = "AgentSourceSystemID"
        case agentSourceSystemName = "AgentSourceSystemName"
        case listingId = "ListingId"
        case listingKey = "ListingKey"
        case listingOriginatingSystemID = "ListingOriginatingSystemID"
        case listingOriginatingSystemName = "ListingOriginatingSystemName"
        case listingSourceSystemID = "ListingSourceSystemID"
        case listingSourceSystemName = "ListingSourceSystemName"
        case originatingSystemAgentKey = "OriginatingSystemAgentKey"
        case originatingSystemListingKey = "OriginatingSystemListingKey"
        case originatingSystemShowingKey = "OriginatingSystemShowingKey"
        case showingAgentKey = "ShowingAgentKey"
        case showingAgentMlsID = "ShowingAgentMlsID"
        case showingId = "ShowingId"
        case showingKey = "ShowingKey"
        case showingOriginatingSystemID = "ShowingOriginatingSystemID"
        case showingOriginatingSystemName = "ShowingOriginatingSystemName"
        case showingSourceSystemID = "ShowingSourceSystemID"
        case showingSourceSystemName = "ShowingSourceSystemName"
        case sourceSystemAgentKey = "SourceSystemAgentKey"
        case sourceSystemListingKey = "SourceSystemListingKey"
        case sourceSystemShowingKey = "SourceSystemShowingKey"
        case modificationTimestamp = "ModificationTimestamp"
        case originalEntryTimestamp = "OriginalEntryTimestamp"
        case showingEndTimestamp = "ShowingEndTimestamp"
        case showingRequestedTimestamp = "ShowingRequestedTimestamp"
        case showingStartTimestamp = "ShowingStartTimestamp"
        case showingAgent = "ShowingAgent"
        case agentOriginatingSystem = "AgentOriginatingSystem"
        case agentSourceSystem = "AgentSourceSystem"
        case listingOriginatingSystem = "ListingOriginatingSystem"
        case listingSourceSystem = "ListingSourceSystem"
        case showingOriginatingSystem = "ShowingOriginatingSystem"
        case showingSourceSystem = "ShowingSourceSystem"
        case property = "Property"
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(listingKeyNumeric, forKey: .listingKeyNumeric)
        try container.encodeIfPresent(showingAgentKeyNumeric, forKey: .showingAgentKeyNumeric)
        try container.encodeIfPresent(showingKeyNumeric, forKey: .showingKeyNumeric)
        try container.encodeIfPresent(agentOriginatingSystemID, forKey: .agentOriginatingSystemID)
        try container.encodeIfPresent(agentOriginatingSystemName, forKey: .agentOriginatingSystemName)
        try container.encodeIfPresent(agentSourceSystemID, forKey: .agentSourceSystemID)
        try container.encodeIfPresent(agentSourceSystemName, forKey: .agentSourceSystemName)
        try container.encodeIfPresent(listingId, forKey: .listingId)
        try container.encodeIfPresent(listingKey, forKey: .listingKey)
        try container.encodeIfPresent(listingOriginatingSystemID, forKey: .listingOriginatingSystemID)
        try container.encodeIfPresent(listingOriginatingSystemName, forKey: .listingOriginatingSystemName)
        try container.encodeIfPresent(listingSourceSystemID, forKey: .listingSourceSystemID)
        try container.encodeIfPresent(listingSourceSystemName, forKey: .listingSourceSystemName)
        try container.encodeIfPresent(originatingSystemAgentKey, forKey: .originatingSystemAgentKey)
        try container.encodeIfPresent(originatingSystemListingKey, forKey: .originatingSystemListingKey)
        try container.encodeIfPresent(originatingSystemShowingKey, forKey: .originatingSystemShowingKey)
        try container.encodeIfPresent(showingAgentKey, forKey: .showingAgentKey)
        try container.encodeIfPresent(showingAgentMlsID, forKey: .showingAgentMlsID)
        try container.encodeIfPresent(showingId, forKey: .showingId)
        try container.encodeIfPresent(showingKey, forKey: .showingKey)
        try container.encodeIfPresent(showingOriginatingSystemID, forKey: .showingOriginatingSystemID)
        try container.encodeIfPresent(showingOriginatingSystemName, forKey: .showingOriginatingSystemName)
        try container.encodeIfPresent(showingSourceSystemID, forKey: .showingSourceSystemID)
        try container.encodeIfPresent(showingSourceSystemName, forKey: .showingSourceSystemName)
        try container.encodeIfPresent(sourceSystemAgentKey, forKey: .sourceSystemAgentKey)
        try container.encodeIfPresent(sourceSystemListingKey, forKey: .sourceSystemListingKey)
        try container.encodeIfPresent(sourceSystemShowingKey, forKey: .sourceSystemShowingKey)
        try container.encodeIfPresent(modificationTimestamp, forKey: .modificationTimestamp)
        try container.encodeIfPresent(originalEntryTimestamp, forKey: .originalEntryTimestamp)
        try container.encodeIfPresent(showingEndTimestamp, forKey: .showingEndTimestamp)
        try container.encodeIfPresent(showingRequestedTimestamp, forKey: .showingRequestedTimestamp)
        try container.encodeIfPresent(showingStartTimestamp, forKey: .showingStartTimestamp)
        try container.encodeIfPresent(showingAgent, forKey: .showingAgent)
        try container.encodeIfPresent(agentOriginatingSystem, forKey: .agentOriginatingSystem)
        try container.encodeIfPresent(agentSourceSystem, forKey: .agentSourceSystem)
        try container.encodeIfPresent(listingOriginatingSystem, forKey: .listingOriginatingSystem)
        try container.encodeIfPresent(listingSourceSystem, forKey: .listingSourceSystem)
        try container.encodeIfPresent(showingOriginatingSystem, forKey: .showingOriginatingSystem)
        try container.encodeIfPresent(showingSourceSystem, forKey: .showingSourceSystem)
        try container.encodeIfPresent(property, forKey: .property)
    }
}

