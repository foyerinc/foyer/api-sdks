//
// PropertyScoreInferenceScoresKitchen.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PropertyScoreInferenceScoresKitchen: Codable, JSONEncodable, Hashable {

    public var presentation: Score?
    public var spaciousness: Score?
    public var trendiness: Score?

    public init(presentation: Score? = nil, spaciousness: Score? = nil, trendiness: Score? = nil) {
        self.presentation = presentation
        self.spaciousness = spaciousness
        self.trendiness = trendiness
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case presentation
        case spaciousness
        case trendiness
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(presentation, forKey: .presentation)
        try container.encodeIfPresent(spaciousness, forKey: .spaciousness)
        try container.encodeIfPresent(trendiness, forKey: .trendiness)
    }
}

