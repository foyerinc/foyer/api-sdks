//
// PropertyScoreFinalCategoryScores.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

public struct PropertyScoreFinalCategoryScores: Codable, JSONEncodable, Hashable {

    public var bathroom: Double?
    public var bedroom: Double?
    public var diningRoom: Double?
    public var kitchen: Double?
    public var livingRoom: Double?
    public var outdoor: Double?

    public init(bathroom: Double? = nil, bedroom: Double? = nil, diningRoom: Double? = nil, kitchen: Double? = nil, livingRoom: Double? = nil, outdoor: Double? = nil) {
        self.bathroom = bathroom
        self.bedroom = bedroom
        self.diningRoom = diningRoom
        self.kitchen = kitchen
        self.livingRoom = livingRoom
        self.outdoor = outdoor
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case bathroom
        case bedroom
        case diningRoom = "dining_room"
        case kitchen
        case livingRoom = "living_room"
        case outdoor
    }

    // Encodable protocol methods

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(bathroom, forKey: .bathroom)
        try container.encodeIfPresent(bedroom, forKey: .bedroom)
        try container.encodeIfPresent(diningRoom, forKey: .diningRoom)
        try container.encodeIfPresent(kitchen, forKey: .kitchen)
        try container.encodeIfPresent(livingRoom, forKey: .livingRoom)
        try container.encodeIfPresent(outdoor, forKey: .outdoor)
    }
}

