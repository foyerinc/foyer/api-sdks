#!/bin/zsh
openApiFilePath=openapi.yaml

# Remove old stuff
rm -rf ./ts/src ./ts/docs ./ts/build

# TS
npx openapi-typescript-codegen@0.23 --input $openApiFilePath --output ./ts/src --client axios
cd ./ts
npm i
npx typedoc --plugin none --out ./docs ./src/index.ts --readme ./README.md
npx tsc --build
echo "Regenerated ts sdk"

# Elixir
# cd ..
# rm -rf elixir
# mkdir elixir
# npx openapi-generator-cli generate -i $openApiFilePath -g elixir -o elixir  --skip-validate-spec --additional-properties=packageName=foyer_api_sdk

# JS
#npx tsc --build
#echo "Regenerated js sdk"

# # C#
# npx -p openapi-codegen cg og:client:csharp -o cs $openApiFilePath
# echo "Regenerated c# sdk"

# # Python
# npx -p openapi-codegen cg og:client:python -o python $openApiFilePath
# echo "Regenerated python sdk"

# # Swift
# npx @openapitools/openapi-generator-cli generate -i $openApiFilePath -g swift5 -o swift
# echo "Regenerated swift sdk"

echo "Done"
