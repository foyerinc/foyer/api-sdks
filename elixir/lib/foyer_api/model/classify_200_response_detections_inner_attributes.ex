# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Model.Classify200ResponseDetectionsInnerAttributes do
  @moduledoc """
  
  """

  @derive [Poison.Encoder]
  defstruct [
    :"is-stainless"
  ]

  @type t :: %__MODULE__{
    :"is-stainless" => boolean() | nil
  }
end

defimpl Poison.Decoder, for: FoyerAPI.Model.Classify200ResponseDetectionsInnerAttributes do
  def decode(value, _options) do
    value
  end
end

