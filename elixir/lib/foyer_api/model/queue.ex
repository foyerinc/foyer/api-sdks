# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Model.Queue do
  @moduledoc """
  
  """

  @derive [Poison.Encoder]
  defstruct [
    :id,
    :QueueTransactionKeyNumeric,
    :ResourceRecordKeyNumeric,
    :OriginatingSystemID,
    :OriginatingSystemName,
    :OriginatingSystemQueueKey,
    :QueueTransactionKey,
    :ResourceRecordID,
    :ResourceRecordKey,
    :SourceSystemID,
    :SourceSystemName,
    :SourceSystemQueueKey,
    :ClassName,
    :QueueTransactionType,
    :ResourceName,
    :ModificationTimestamp,
    :OriginatingSystem,
    :SourceSystem
  ]

  @type t :: %__MODULE__{
    :id => integer() | nil,
    :QueueTransactionKeyNumeric => float() | nil,
    :ResourceRecordKeyNumeric => float() | nil,
    :OriginatingSystemID => float() | nil,
    :OriginatingSystemName => String.t | nil,
    :OriginatingSystemQueueKey => String.t | nil,
    :QueueTransactionKey => String.t | nil,
    :ResourceRecordID => String.t | nil,
    :ResourceRecordKey => String.t | nil,
    :SourceSystemID => String.t | nil,
    :SourceSystemName => String.t | nil,
    :SourceSystemQueueKey => String.t | nil,
    :ClassName => String.t | nil,
    :QueueTransactionType => String.t | nil,
    :ResourceName => String.t | nil,
    :ModificationTimestamp => Date.t | nil,
    :OriginatingSystem => FoyerAPI.Model.Ouid.t | nil,
    :SourceSystem => FoyerAPI.Model.Ouid.t | nil
  }
end

defimpl Poison.Decoder, for: FoyerAPI.Model.Queue do
  import FoyerAPI.Deserializer
  def decode(value, options) do
    value
    |> deserialize(:ModificationTimestamp, :date, nil, options)
    |> deserialize(:OriginatingSystem, :struct, FoyerAPI.Model.Ouid, options)
    |> deserialize(:SourceSystem, :struct, FoyerAPI.Model.Ouid, options)
  end
end

