# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Model.ResetPasswordStepOneRequest do
  @moduledoc """
  
  """

  @derive [Poison.Encoder]
  defstruct [
    :email
  ]

  @type t :: %__MODULE__{
    :email => String.t | nil
  }
end

defimpl Poison.Decoder, for: FoyerAPI.Model.ResetPasswordStepOneRequest do
  def decode(value, _options) do
    value
  end
end

