# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Model.PropertyScoreFinalCategoryScores do
  @moduledoc """
  
  """

  @derive [Poison.Encoder]
  defstruct [
    :bathroom,
    :bedroom,
    :dining_room,
    :kitchen,
    :living_room,
    :outdoor
  ]

  @type t :: %__MODULE__{
    :bathroom => float() | nil,
    :bedroom => float() | nil,
    :dining_room => float() | nil,
    :kitchen => float() | nil,
    :living_room => float() | nil,
    :outdoor => float() | nil
  }
end

defimpl Poison.Decoder, for: FoyerAPI.Model.PropertyScoreFinalCategoryScores do
  def decode(value, _options) do
    value
  end
end

