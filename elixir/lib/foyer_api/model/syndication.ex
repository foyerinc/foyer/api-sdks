# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Model.Syndication do
  @moduledoc """
  
  """

  @derive [Poison.Encoder]
  defstruct [
    :id,
    :memberId,
    :officeId,
    :publisherId,
    :permission,
    :createdAt,
    :updatedAt,
    :Member,
    :Office,
    :Publisher
  ]

  @type t :: %__MODULE__{
    :id => integer() | nil,
    :memberId => integer() | nil,
    :officeId => integer() | nil,
    :publisherId => integer() | nil,
    :permission => String.t | nil,
    :createdAt => Date.t | nil,
    :updatedAt => Date.t | nil,
    :Member => FoyerAPI.Model.Member.t | nil,
    :Office => FoyerAPI.Model.Office.t | nil,
    :Publisher => FoyerAPI.Model.Publisher.t | nil
  }
end

defimpl Poison.Decoder, for: FoyerAPI.Model.Syndication do
  import FoyerAPI.Deserializer
  def decode(value, options) do
    value
    |> deserialize(:createdAt, :date, nil, options)
    |> deserialize(:updatedAt, :date, nil, options)
    |> deserialize(:Member, :struct, FoyerAPI.Model.Member, options)
    |> deserialize(:Office, :struct, FoyerAPI.Model.Office, options)
    |> deserialize(:Publisher, :struct, FoyerAPI.Model.Publisher, options)
  end
end

