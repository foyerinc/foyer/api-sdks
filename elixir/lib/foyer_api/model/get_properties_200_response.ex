# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Model.GetProperties200Response do
  @moduledoc """
  
  """

  @derive [Poison.Encoder]
  defstruct [
    :count,
    :rows
  ]

  @type t :: %__MODULE__{
    :count => integer() | nil,
    :rows => [FoyerAPI.Model.Property.t] | nil
  }
end

defimpl Poison.Decoder, for: FoyerAPI.Model.GetProperties200Response do
  import FoyerAPI.Deserializer
  def decode(value, options) do
    value
    |> deserialize(:rows, :list, FoyerAPI.Model.Property, options)
  end
end

