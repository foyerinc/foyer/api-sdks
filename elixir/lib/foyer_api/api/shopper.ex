# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Api.Shopper do
  @moduledoc """
  API calls for all endpoints tagged `Shopper`.
  """

  alias FoyerAPI.Connection
  import FoyerAPI.RequestBuilder


  @doc """
  Delete Shopper By Id
  Delete specific Shopper with unique Id

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - id (integer()): 
  - opts (KeywordList): [optional] Optional parameters
  ## Returns

  {:ok, nil} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec delete_shopper_by_id(Tesla.Env.client, integer(), keyword()) :: {:ok, nil} | {:error, Tesla.Env.t}
  def delete_shopper_by_id(connection, id, _opts \\ []) do
    %{}
    |> method(:delete)
    |> url("/Shopper/#{id}")
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 204, false},
      { 401, false}
    ])
  end

  @doc """
  Find One Shopper
  Find one specific Shopper according to defined filter criteria

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - filter (String.t): 
  - opts (KeywordList): [optional] Optional parameters
  ## Returns

  {:ok, FoyerAPI.Model.Shopper.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec find_one_shopper(Tesla.Env.client, String.t, keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.Shopper.t} | {:error, Tesla.Env.t}
  def find_one_shopper(connection, filter, _opts \\ []) do
    %{}
    |> method(:get)
    |> url("/Shopper/findOne")
    |> add_param(:headers, :filter, filter)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.Shopper{}},
      { 401, false}
    ])
  end

  @doc """
  Get Shopper By Id
  Get specific Shopper with unique Id

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - id (integer()): 
  - opts (KeywordList): [optional] Optional parameters
    - :filter (String.t): 
  ## Returns

  {:ok, FoyerAPI.Model.Shopper.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec get_shopper_by_id(Tesla.Env.client, integer(), keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.Shopper.t} | {:error, Tesla.Env.t}
  def get_shopper_by_id(connection, id, opts \\ []) do
    optional_params = %{
      :filter => :headers
    }
    %{}
    |> method(:get)
    |> url("/Shopper/#{id}")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.Shopper{}},
      { 401, false}
    ])
  end

  @doc """
  Get Shoppers
  Get an array of Shoppers according to defined filter criteria

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - opts (KeywordList): [optional] Optional parameters
    - :filter (String.t): 
  ## Returns

  {:ok, [%Shopper{}, ...]} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec get_shoppers(Tesla.Env.client, keyword()) :: {:ok, nil} | {:ok, list(FoyerAPI.Model.Shopper.t)} | {:error, Tesla.Env.t}
  def get_shoppers(connection, opts \\ []) do
    optional_params = %{
      :filter => :headers
    }
    %{}
    |> method(:get)
    |> url("/Shopper")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, [%FoyerAPI.Model.Shopper{}]},
      { 401, false}
    ])
  end

  @doc """
  Patch Shopper By Id
  Patch specific Shopper with unique Id

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - id (integer()): 
  - opts (KeywordList): [optional] Optional parameters
    - :body ([FoyerAPI.Model.PatchDocument.t]): 
  ## Returns

  {:ok, FoyerAPI.Model.Shopper.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec patch_shopper_by_id(Tesla.Env.client, integer(), keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.Shopper.t} | {:error, Tesla.Env.t}
  def patch_shopper_by_id(connection, id, opts \\ []) do
    optional_params = %{
      :body => :body
    }
    %{}
    |> method(:patch)
    |> url("/Shopper/#{id}")
    |> add_optional_params(optional_params, opts)
    |> ensure_body()
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.Shopper{}},
      { 401, false}
    ])
  end

  @doc """
  Post Shoppers
  Post an array of Shopper objects

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - shopper (Shopper): 
  - opts (KeywordList): [optional] Optional parameters
  ## Returns

  {:ok, FoyerAPI.Model.Shopper.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec post_shoppers(Tesla.Env.client, FoyerAPI.Model.Shopper.t, keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.Shopper.t} | {:error, Tesla.Env.t}
  def post_shoppers(connection, shopper, _opts \\ []) do
    %{}
    |> method(:post)
    |> url("/Shopper")
    |> add_param(:body, :body, shopper)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.Shopper{}},
      { 401, false}
    ])
  end
end
