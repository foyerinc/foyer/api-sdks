# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# https://openapi-generator.tech
# Do not edit the class manually.

defmodule FoyerAPI.Api.Property do
  @moduledoc """
  API calls for all endpoints tagged `Property`.
  """

  alias FoyerAPI.Connection
  import FoyerAPI.RequestBuilder


  @doc """
  Compare property by id to local area

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - id (integer()): 
  - opts (KeywordList): [optional] Optional parameters
    - :filter (String.t): 
  ## Returns

  {:ok, nil} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec calculate_local_graph(Tesla.Env.client, integer(), keyword()) :: {:ok, nil} | {:error, Tesla.Env.t}
  def calculate_local_graph(connection, id, opts \\ []) do
    optional_params = %{
      :filter => :headers
    }
    %{}
    |> method(:get)
    |> url("/Property/#{id}/calculateLocalGraph")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, false},
      { 401, false}
    ])
  end

  @doc """
  Calculate Foyer Score for Property

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - id (integer()): 
  - opts (KeywordList): [optional] Optional parameters
    - :filter (String.t): 
  ## Returns

  {:ok, FoyerAPI.Model.CalculateScore200Response.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec calculate_score(Tesla.Env.client, integer(), keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.CalculateScore200Response.t} | {:error, Tesla.Env.t}
  def calculate_score(connection, id, opts \\ []) do
    optional_params = %{
      :filter => :headers
    }
    %{}
    |> method(:get)
    |> url("/Property/#{id}/calculateScore")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 401, false},
      { 200, %FoyerAPI.Model.CalculateScore200Response{}}
    ])
  end

  @doc """
  Find Nearest Properties
  Provide an address to find the geographically closest Properties

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - street_number (String.t): 
  - street (String.t): 
  - street_type (String.t): 
  - city_name (String.t): 
  - state (String.t): 
  - lat (float()): 
  - lng (float()): 
  - opts (KeywordList): [optional] Optional parameters
  ## Returns

  {:ok, FoyerAPI.Model.FindNearestProperties200Response.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec find_nearest_properties(Tesla.Env.client, String.t, String.t, String.t, String.t, String.t, float(), float(), keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.FindNearestProperties200Response.t} | {:error, Tesla.Env.t}
  def find_nearest_properties(connection, street_number, street, street_type, city_name, state, lat, lng, _opts \\ []) do
    %{}
    |> method(:get)
    |> url("/Property/findNearest")
    |> add_param(:query, :streetNumber, street_number)
    |> add_param(:query, :street, street)
    |> add_param(:query, :streetType, street_type)
    |> add_param(:query, :cityName, city_name)
    |> add_param(:query, :state, state)
    |> add_param(:query, :lat, lat)
    |> add_param(:query, :lng, lng)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.FindNearestProperties200Response{}},
      { 401, false}
    ])
  end

  @doc """
  Find One Property
  Find one Property according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - filter (String.t): 
  - opts (KeywordList): [optional] Optional parameters
    - :verbosity (String.t): 
  ## Returns

  {:ok, FoyerAPI.Model.Property.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec find_one_property(Tesla.Env.client, String.t, keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.Property.t} | {:error, Tesla.Env.t}
  def find_one_property(connection, filter, opts \\ []) do
    optional_params = %{
      :verbosity => :query
    }
    %{}
    |> method(:get)
    |> url("/Property/findOne")
    |> add_param(:headers, :filter, filter)
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.Property{}},
      { 401, false}
    ])
  end

  @doc """
  Get Properties
  Get an object with an array of properties and a total count according to defined filter criteria. Also allows for verbosity parameter to shortcut commonly used filters

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - opts (KeywordList): [optional] Optional parameters
    - :filter (String.t): 
    - :verbosity (String.t): 
  ## Returns

  {:ok, FoyerAPI.Model.GetProperties200Response.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec get_properties(Tesla.Env.client, keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.GetProperties200Response.t} | {:error, Tesla.Env.t}
  def get_properties(connection, opts \\ []) do
    optional_params = %{
      :filter => :headers,
      :verbosity => :query
    }
    %{}
    |> method(:get)
    |> url("/Property")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.GetProperties200Response{}},
      { 401, false}
    ])
  end

  @doc """
  Get Property by Id
  Get specific Property with unique id. Also allows for verbosity parameter to shortcut commonly used filters

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - id (integer()): 
  - opts (KeywordList): [optional] Optional parameters
    - :filter (String.t): 
    - :verbosity (String.t): 
  ## Returns

  {:ok, FoyerAPI.Model.Property.t} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec get_property_by_id(Tesla.Env.client, integer(), keyword()) :: {:ok, nil} | {:ok, FoyerAPI.Model.Property.t} | {:error, Tesla.Env.t}
  def get_property_by_id(connection, id, opts \\ []) do
    optional_params = %{
      :filter => :headers,
      :verbosity => :query
    }
    %{}
    |> method(:get)
    |> url("/Property/#{id}")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, %FoyerAPI.Model.Property{}},
      { 401, false}
    ])
  end

  @doc """
  Get Recommendations
  Return an array of Properties according to preferences set on User and Shopper Models

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - opts (KeywordList): [optional] Optional parameters
    - :limit (integer()): 
    - :buf_len (integer()): 
  ## Returns

  {:ok, [%Property{}, ...]} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec get_recommendations(Tesla.Env.client, keyword()) :: {:ok, nil} | {:ok, list(FoyerAPI.Model.Property.t)} | {:error, Tesla.Env.t}
  def get_recommendations(connection, opts \\ []) do
    optional_params = %{
      :limit => :query,
      :bufLen => :query
    }
    %{}
    |> method(:get)
    |> url("/Property/getRecommendations")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, [%FoyerAPI.Model.Property{}]},
      { 401, false}
    ])
  end

  @doc """
  Property Search
  Search for properties according to defined criteria and desired image features

  ## Parameters

  - connection (FoyerAPI.Connection): Connection to server
  - opts (KeywordList): [optional] Optional parameters
    - :verbosity (String.t): 
  ## Returns

  {:ok, [%Property{}, ...]} on success
  {:error, Tesla.Env.t} on failure
  """
  @spec property_search(Tesla.Env.client, keyword()) :: {:ok, nil} | {:ok, list(FoyerAPI.Model.Property.t)} | {:error, Tesla.Env.t}
  def property_search(connection, opts \\ []) do
    optional_params = %{
      :verbosity => :query
    }
    %{}
    |> method(:get)
    |> url("/Property/search")
    |> add_optional_params(optional_params, opts)
    |> Enum.into([])
    |> (&Connection.request(connection, &1)).()
    |> evaluate_response([
      { 200, [%FoyerAPI.Model.Property{}]},
      { 401, false}
    ])
  end
end
